@extends('mails.layouts.main')

@section('content')

    <div class="container">
    	<h1>{{ $eventName }}</h1>
      <h2>Accept</h2>
      <a class="btn btn-success" href="{{ url('event/attend-event-acceptance/'.$link.'/'.$attendeID.'/1/'.$eventId) }}">Accept</a>
      
    </div>
    <div class="container">
      <h2>Reject</h2>
      <a class="btn btn-danger" href="{{ url('event/attend-event-acceptance/'.$link.'/'.$attendeID.'/0/'.$eventId) }}">Reject</a>
    </div>
@endsection