@extends('adminlte::page')

@section('title', 'List Events')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <style>
        .form-horizontal .form-group{margin-left:0px;margin-right: 0px;}
     .form-horizontal .box-body{display:flex;flex-wrap:wrap;}
     .form-horizontal .box-body .form-group{width: 50%;
    float: left;
    padding: 0 15px;}
    .box-footer a.btn-default{margin-right:10px;}
     .form-horizontal .box-body .form-group {
    width: 25%;
    }
    </style>
@stop

@section('content')


     <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->

        <div class="col-md-12">
            <!-- Horizontal Form -->

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Events</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="" method="get" style="padding:2px;marging:5px;">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" >Name</label>

                            <div >
                                <input type="text" name= "eventName" class="form-control" id="name" placeholder="Name" value="{{ (isset($_GET['eventName']))?$_GET['eventName']:old('eventName')}}">

                            </div>
                        </div>
                        <div class="form-group">
                            <label>Start Date:</label>

                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker" name="eventStartDate" value="{{ (isset($_GET['eventStartDate']))?$_GET['eventStartDate']:old('eventStartDate') }}">
                            </div>

                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>End Date:</label>

                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker1" name="eventEndDate" value="{{ (isset($_GET['eventEndDate']))?$_GET['eventEndDate']:old('eventEndDate') }}">
                            </div>

                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label for="name" >Address</label>

                            <div >
                                <textarea type="text" name= "eventAddress" class="form-control" id="eventAddress" placeholder="Event Address">{{ (isset($_GET['eventAddress']))?($_GET['eventAddress']):old('eventAddress')}}</textarea>

                            </div>
                        </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('list.event') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>

        <!--/.col (right) -->
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
                <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Event List</h3>
                </div>
            <div class="box-body table-responsive no-padding">

                @isset($events)

                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>StartDate</th>
                            <th>EndDate</th>
                            <th>Start time</th>
                            <th>End time</th>
                            <th>Event Address</th>
                            <th>Attendents in events</th>
                            <th>Attende List</th>
                            <th>Status</th>
                            @can('write')
                                <th>Action</th>
                            @endcan
                            <th>Created At</th>
                            <th>Created By</th>
                            <th>Updated At</th>
                        </tr>
                        @php
                        $i = $events->perPage() * ($events->currentPage()-1) + 1;
                        @endphp
                        @foreach($events as $event)
                            @if($event)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ ($event['eventName'])?$event['eventName']:'No name found' }}</td>
                                    <td>{{ ($event['eventStartDate'])?$event['eventStartDate']:'No name found' }}</td>
                                    <td>{{ ($event['eventEndDate'])?$event['eventEndDate']:'No name found' }}</td>
                                    <td>{{ ($event['eventStartTime'])?$event['eventStartTime']:'No name found' }}</td>
                                    <td>{{ ($event['eventEndTime'])?$event['eventEndTime']:'No name found' }}</td>
                                    <td>{{ ($event['eventAddress'])?$event['eventAddress']:'No name found' }}</td>
                                    <td>{{count(App\Helpers\EventAttendeHelper::getEventAttende($event->id))}}</td>
                                    <td>@if(count(App\Helpers\EventAttendeHelper::getEventAttende($event->id))>0)<a class="btn btn-primary" href="{{ url('event/list-attende/'.$event['id'])}}">AttendeList</a>
                                    @else
                                        No record found
                                    @endif
                                    </td>
                                    @if ($event['eventStatus'] == 1)
                                    <td><a href="{{ url('event/status-update/'.$event['id'].'/'.$event['eventStatus'])}}" class="btn btn-danger" onclick="if(!confirm('Do You Want To Update status?')){return false;}">Inactive</a></td>
                                    @else
                                    <td><a href="{{ url('event/status-update/'.$event['id'].'/'.$event['eventStatus'])}}" class="btn btn-success" onclick="if(!confirm('Do You Want To Update status?')){return false;}">Active</a></td>
                                    @endif
                                    @can('write')
                                        <td><a href="{{ url('event/event-edit/'.$event['id']) }}"><i class="fa fa-fw fa-edit"></i></a>
                                            <a href="{{ url('event/event-delete/'.$event['id']) }}" onclick="if(!confirm('Do You Want To Delete?')){return false;}"><i class="fa fa-fw fa-trash"></i></a></td>
                                    @endcan
                                    <td>{{ $event['created_at']}}</td>
                                    <td>{{ (App\Helper\AppHelper::getUserCreateEditName($event['eventCreatedByUserID']))?(App\Helper\AppHelper::getUserCreateEditName($event['eventCreatedByUserID'])):'Deleted user'}}</td>
                                    <td>{{ $event['updated_at']}}</td>
                                </tr>
                            @endif
                            @php $i++ @endphp
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endisset
            </div>

            <div class="clearfix">
                {{ $events->links() }}
            </div>
                </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
     <script>
    $('#datepicker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd'
    })
    $('#datepicker1').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd'
    })

    </script>
@stop
