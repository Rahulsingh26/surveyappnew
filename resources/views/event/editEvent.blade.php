@extends('adminlte::page')

@section('title', 'Add User')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <style>
     .help-block{
        font-weight:bold;
        color:red;
     } 
     .ui-timepicker-container{
        z-index:99999 !important;
     } 
     .form-horizontal .form-group{margin-left:0px;margin-right: 0px;}
  </style>
@stop

@section('content')

    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Events</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{ url('event/event-update') }}" method="post" style="padding:2px;marging:5px;">
                    @csrf
                    <div class="box-body">
                        <input type="hidden" name="eventID" class="form-control" value="{{ $eventObj->id}}" >
                        <div class="form-group">
                            <label for="name" >Name</label>

                            <div >
                                <input type="text" name= "eventName" class="form-control" id="name" placeholder="Name" value="{{ ($eventObj->eventName)?$eventObj->eventName:old('eventName')}}">
                                @if($errors->first('eventName'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eventName') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Start Date:</label>

                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker" name="eventStartDate" value="{{ ($eventObj->eventStartDate)?$eventObj->eventStartDate:old('eventStartDate')}}">
                            </div>
                           @if($errors->first('eventStartDate'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eventStartDate') }}</strong>
                                </span>
                            @endif
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>End Date:</label>

                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="datepicker1" name="eventEndDate" value="{{ ($eventObj->eventEndDate)?$eventObj->eventEndDate:old('eventEndDate')}}">
                            </div>
                           @if($errors->first('eventEndDate'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eventEndDate') }}</strong>
                                </span>
                            @endif
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Start Time</label>
                            <div class="input-group date" id='datetimepicker3'>
                              <input type="text" class="form-control pull-right timepicker1"  name="eventStartTime" autocomplete="off" value="{{ ($eventObj->eventStartTime)?$eventObj->eventStartTime:old('eventStartTime')}}">
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                             </span>
                            </div>
                          @if($errors->first('eventStartTime'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eventStartTime') }}</strong>
                                </span>
                            @endif
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>End Time</label>

                            <div class="input-group date" id='datetimepicker4'>
                              
                              <input type="text" class="form-control pull-right timepicker"  name="eventEndTime" autocomplete="off" value="{{ ($eventObj->eventEndTime)?$eventObj->eventEndTime:old('eventEndTime')}}">
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                             </span>
                            </div>
                          @if($errors->first('eventStartTime'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eventEndTime') }}</strong>
                                </span>
                            @endif
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label for="name" >Address</label>

                            <div >
                                <textarea type="text" name= "eventAddress" class="form-control" id="eventAddress" placeholder="Event Address">{{($eventObj->eventAddress)?$eventObj->eventAddress:old('eventAddress')}}</textarea>
                                @if($errors->first('eventAddress'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('eventAddress') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                      
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('create.event') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right" onclick="if(!confirm('Do You Want To Update Event?')){return false;}">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <div class="col-md-2"></div>
        <!--/.col (right) -->
    </div>

@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

     <script>
    $('#datepicker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd',
        startDate: new Date()
    })
    $('#datepicker1').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd',
        startDate: new Date()
    })
    $('#datepicker').datepicker()
    .on('changeDate', function(e) {
        $('#datepicker1').datepicker('setStartDate', e.target.value);
    });
    $('#datepicker1').datepicker()
    .on('changeDate', function(e) {
        $('#datepicker').datepicker('setEndDate', e.target.value);
    });
    </script>
    <script >
    $(document).ready(function(){
        $('input.timepicker1').timepicker({});
    });
    </script>
    <script >
    $(document).ready(function(){
        $('input.timepicker').timepicker({});
    });
    </script>
@stop