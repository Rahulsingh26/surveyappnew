{{--@foreach($canvasserid as $list)
    @php
            $user = \App\Helper\AppHelper::getUserDetails($list->canvasser_id);

    @endphp
    <span class="label label-primary" title="{{$user->name}} ({{$user->email}})">{{$user->name}}</span>
@endforeach--}}
@extends('adminlte::page')

@section('title', 'Dashboard page')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
@stop

@section('content')
    <div class="row dashboard">

        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header" align="Center">
                    <h3 class="box-title">Filter by Political Geography</h3>
                    <div class="box-tools pull-right">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                       {{-- <form id="filterDashboardData" class="form-horizontal" action="{{route('dashboard-graph-data')}}" method="post">
                            @csrf--}}
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class="control-label">Precinct</label>
                                    <select class="precinct form-control" name="precinct">
                                        <option value="">Select Precinct</option>
                                        @foreach($surveyUsers as $value)
                                            <option value="{{$value->id}}">{{$value->precinct}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">City</label>
                                     <select class="cityList form-control" id="seleteCity" name="city">
                                     </select>

                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">County</label>
                                     <select class="countryList form-control" id="selectCountry" name="country">
                                     </select>

                                </div>
                                 <div class="col-lg-3">
                                    <label class="control-label">Zipcode</label>
                                     <select class="zipcodeList form-control" id="selectZipcode" name="zipcodeList">
                                     </select>

                                </div>



                                <div class="col-lg-3">
                                    <label class="control-label">State</label>
                                    <select class="StateList form-control" id="seleteState" name="state">
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">Survey List</label>
                                    <select class="surveyList form-control" id="surveyListdata" name="survey_id">
                                    </select>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-lg-3">
                                    <label class="control-label">Question List</label>
                                    <select class="ques form-control" id="response" name="question_id">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">Additional Observations</label>
                                    <div >
                                        <select class="js-multiple form-control" id="observations" name="observations[]" multiple="multiple" data-placeholder="Select Canvassers" style="width: 100%;">
                                            @foreach(["Conservative YS1","Conservative YS2","Liberal YS3"] as $value)
                                                <option value="{{$value}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 dashboard-btn">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-info" id="graphfilterbtn"> Filter graph data</button>
                                    </div>
                                </div>
                            </div>



                       {{-- </form>--}}
                        <div class="panel-body">
                            <div id="additional-observations"></div>
                            <canvas id="canvas" height="100" width="600"></canvas>
                        </div>
                        <div class="panel-body">
                            <div id="survey-question"></div>
                            <canvas id="canvasQues" height="100" width="600"></canvas>
                        </div>
                    </div>

                </div>
            </div>
            <div class="box box-info">
                <div class="box-header" align="left">
                    <h3 class="box-title">Total HH Knocked By Canvasser</h3>
                    <div class="box-tools pull-right">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                            @if(isset($canvasserid))
                            <div class="col-lg-3">
                                <strong><h7>Canvasser List</h7></strong>
                                <select class="canvas_name form-control select2" id="canvas_ID" name="canvasser_id[]" multiple="multiple">
                                    <option value="">Select Canvasser</option>
                                    @foreach($canvasserid as $list)
                                        @php
                                            $user = \App\Helper\AppHelper::getUserDetails($list->canvasser_id);
                                        @endphp
                                        <option value="{{$user->id}}">{{$user->name}} </option>
                                    @endforeach
                                </select>

                            </div>
                            @endif
                            <div class="col-lg-3">
                                <strong><h7>Get Details</h7></strong>
                               <!--  <select class="daysAccording form-control" name="label">
                                    <option value="1">Weeks</option>
                                    <option value="2">Months</option>
                                    <option value="3">Years</option>
                                </select> -->
                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span></span> <i class="fa fa-caret-down"></i>
                                </div>



                            </div>
                            <div class="col-lg-6 custom-btn">
                                <div class="btn-group">
                                    <button class="btn btn-info" id="canvasGraph">Filter graph data</button>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <!-- <canvas id="canvasKnock_1"></canvas>
                                <canvas id="canvasKnock_2"></canvas>
                                <canvas id="canvasKnock_3"></canvas> -->
                                <canvas id="canvasKnock_4"></canvas>
                            </div>
                </div>
            </div>

            <div class="box box-info">
                <div class="box-header" align="left">
                    <h3 class="box-title">Canvasser Identification</h3>
                        <div class="box-tools pull-right">

                            <!-- Collapse Button -->
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>

                </div>
                <div class="box-body">
                    <div class="col-lg-8">
                        <canvas id="identificationCanvas"></canvas>
                    </div>
                </div>

            </div>

                <div class="box box-info">
                    <div class="box-header" align="left">
                        <h3 class="box-title">Survey Result</h3>
                        <div class="box-tools pull-right">

                            <!-- Collapse Button -->
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>

                    </div>
                    <div class="box-body">
                        <div class="col-lg-8">
                            <canvas id="surverResulted" height="200" width="600"></canvas>
                        </div>
                    </div>

                </div>
        </div>

    </div>




@stop



@section('custom_js')

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<script type="text/javascript">
    $('.select2').select2()
</script>
    <script>
        var myPieChart = null;
        var myQuesPieChart = null;
        var myBarChart = null;
        $(document).ready(function() {
            $("#canvas").hide();
            $("#canvasQues").hide();
            $("#canvasKnock").hide();
            $("#canvasKnock_1").hide();
            $("#canvasKnock_2").hide();
            $("#canvasKnock_3").hide();
            $("#graphfilterbtn").prop('disabled', true);
            $('.js-multiple').select2();
            $("select.surveyList").change(function(){
                var selectedSurveyList = $(".surveyList option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('survey.question') }}",
                    data: {_token:"{{csrf_token()}}",surveylist : selectedSurveyList }
                }).done(function(data){
                    $('#response option').remove();
                    if(data.length){
                        $("#graphfilterbtn").prop('disabled', false);
                        $.each(data,function(key,value){
                            $("#response").append('<option value="'+value['id']+'">'+value['title']+'</option>');
                        });
                    }else{
                        $("#canvas").hide();
                        $("#canvasQues").hide();
                        $("#graphfilterbtn").prop('disabled', true);
                    }

                });
            });

            //render pie charts
            var ctx = document.getElementById("canvas").getContext('2d');
            var Quesctx = document.getElementById("canvasQues").getContext('2d');

            var data = {
                datasets: [{
                    data: [],
                    backgroundColor: ['#ff6384', '#36a2eb', '#ffce56', '#4bc0c0', '#9966ff'],
                }],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: []
            };
            var Quesdata = {
                datasets: [{
                    data: [],
                    backgroundColor: ['#ff6384', '#36a2eb', '#ffce56', '#4bc0c0', '#9966ff'],
                }],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: []
            };

            myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data
            });

            myQuesPieChart = new Chart(Quesctx, {
                type: 'pie',
                data: Quesdata
            });

            function getGraphData(selectedQuesList,surveyList){
                $.ajax({
                    type: "POST",
                    url: "{{ route('question.report') }}",
                    data: {_token:"{{csrf_token()}}",surveylist : surveyList , quesId: selectedQuesList }
                }).done(function(data){
                    updateQuesGraphData(myPieChart, data.labels, data.data);
                });
            }

            function updateQuesGraphData(chart, labels, data) {

                elementSum = data.reduce((a, b) => a + b, 0);
                if(!elementSum){
                    $("#canvasQues").hide();
                    alert("There is no data for survey Question")
                    return
                }
                $("#canvasQues").show();
                myQuesPieChart.data.labels = labels;
                myQuesPieChart.data.datasets[0]["data"] = data;
                myQuesPieChart.update();
               /* $( "#survey-question" ).append('<div><label for="survey-Question">survey Question</label></div>');*/
            }

            $("#graphfilterbtn").click(function(){
                var selectedQuesList = $(".ques option:selected").val();
                var surveyList = $(".surveyList option:selected").val();
                var observationsList = $("#observations").val();

                $.ajax({
                    type: "POST",
                    url: "{{ route('dashboard-graph-data') }}",
                    data: {_token:"{{csrf_token()}}",surveyid : surveyList , quesId: selectedQuesList,observations: observationsList}
                }).done(function(data){
                    $("#graphfilterbtn").prop('disabled', false);
                    updateGraphData(myPieChart, data.labels, data.data);
                    getGraphData(selectedQuesList, surveyList);
                });
            });

            function updateGraphData(chart, labels, data) {

                elementSum = data.reduce((a, b) => a + b, 0);
                if(!elementSum){
                    $("#canvas").hide();
                    alert("There is no data for additional observations")
                    return
                }
                $("#canvas").show();
                myPieChart.data.labels = labels;
                myPieChart.data.datasets[0]["data"] = data;
                myPieChart.update();
                /*$( "#additional-observations" ).append('<div><label for="additional-observations">additional observations</label></div>');*/
            }

            $("select.precinct").change(function(){
               var selectedPrecinctList = $(".precinct option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('selected.precinct') }}",
                    data: {_token:"{{csrf_token()}}",precinctlistId : selectedPrecinctList }
                }).done(function(data){
                    $('#response option').remove();
                    $('#surveyListdata option').remove();
                    $('#seleteCity option').remove();
                    $('#seleteState option').remove();

                    if(data){
                        $("#surveyListdata").append('<option value="'+'">Select Survey List</option>');
                        $("#surveyListdata").append('<option value="'+data.survey.survey.id+'">'+data.survey.survey.title+'</option>');

                        $("#seleteCity").append('<option value="'+data.surveyUser.survey_user.city+'">'+data.surveyUser.survey_user.city+'</option>');
                        $("#selectCountry").append('<option value="'+data.surveyUser.survey_user.country+'">'+data.surveyUser.survey_user.country+'</option>');
                        $("#selectZipcode").append('<option value="'+data.surveyUser.survey_user.zipcode+'">'+data.surveyUser.survey_user.zipcode+'</option>');
                        $("#seleteState").append('<option value="'+data.surveyUser.survey_user.state+'">'+data.surveyUser.survey_user.state+'</option>');
                    }
                });
            });



            $("#canvasGraph").click(function() {
                var selectedcanvasList = $(".canvas_name").val();
                console.log(selectedcanvasList);
                var selectedDMY = $(".daysAccording option:selected").val();
                var barLable = $(".canvas_name option:selected").text();
                var dateRange = $("span.drp-selected").text();

                //console.log(barLable);

                $.ajax({
                    type: "POST",
                    url: "{{ route('selected.canvasser') }}",
                    data: {_token:"{{csrf_token()}}",canvasser_id : selectedcanvasList,DMY:selectedDMY, dateRange:dateRange}
                }).done(function(data){
                    updatecanvasGraphData(data.labels, data.data,barLable,selectedDMY);
                    $("select.canvas_name").change(function(){
                        window.location.reload(true)
                    });
                });

            });

           function updatecanvasGraphData(labels, data,barLable) {
               // $("#canvasKnock_1").hide();
               // $("#canvasKnock_2").hide();
               // $("#canvasKnock_3").hide();
               // $("#canvasKnock_"+selectedDMY).show();
               $("#canvasKnock_4").show();
               // alert(selectedDMY);
               var canvasKn = document.getElementById("canvasKnock_4").getContext('2d');


               var myBarChart = new Chart(canvasKn, {
                   type: 'bar',
                   data: {
                       labels:labels,
                       datasets: [{
                           label: barLable,
                           label: 'Canvasser List',
                           data: data
                       }]

                   },
                   options: {
                       scales: {
                           yAxes: [{
                               ticks: {
                                   beginAtZero:true
                               }
                           }]
                       }
                   }
               });

               console.log('-----------', (window.bar != undefined), '--------');

               if(window.bar != undefined) {

                  location.reload();
;

                }
                window.bar = myBarChart

               //Better to construct options first and then pass it as a parameter

           }

            var url = "{{ route('canvasser.identification') }}";
            $.get(url, function(response){
                identificationcanvasGraphData(response.labels, response.data);
            });

            function identificationcanvasGraphData(labels, data) {
                $("#identificationCanvas").show();
                var identificationcanvas = document.getElementById("identificationCanvas").getContext('2d');
                var myBarChart = new Chart(identificationcanvas, {
                    type: 'bar',
                    data: {
                        labels:labels,
                        datasets: [{
                            label: "Identification Canvas",
                            data: data
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }
            var url = "{{ route('survey.result') }}";
            $.get(url, function(response){
                surverResultedGraphData(response.labels, response.data);
            });
            function surverResultedGraphData(labels, data) {
                $("#surverResulted").show();
                var surverResulted = document.getElementById("surverResulted").getContext('2d');
                var myBarChart = new Chart(surverResulted, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: "Surver Resulted",
                            backgroundColor:  ['#ff6384', '#36a2eb', '#ffce56', '#4bc0c0', '#9966ff'],
                            data: data
                        }]
                    },
                    // options: {
                    //     title: {
                    //         display: true
                    //     }
                    // }


                     options: {
                            responsive: true,
                            legend: {
                              position: 'top',
                            },
                            title: {
                              display: true,

                            },
                            animation: {
                              animateScale: true,
                              animateRotate: true
                            },
                            tooltips: {
                              callbacks: {
                                label: function(tooltipItem, data) {
                                    var dataset = data.datasets[tooltipItem.datasetIndex];
                                  var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                    return previousValue + currentValue;
                                  });
                                  var currentValue = dataset.data[tooltipItem.index];
                                  var percentage = Math.floor(((currentValue/total) * 100)+0.5);
                                  return percentage + "%";
                                }
                              }
                            }
                          }
                });
            }




        });

    </script>

<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
      /*  ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }*/
    }, cb);

    cb(start, end);
    console.log(start, end);

});
</script>

@stop
