<html>
<head>
    <title>Drawing Tools</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        #map-canvas {
            width:100%;
            height:100%;
        }
    </style>
</head>
<body>
<<h1>Google Maps - Visible Markers In Bounds</h1>

<h2 align="center"><div id="info"></div></h2>
<div id="index"></div>

<div id="map-canvas"></div>
<script>
    function initMap() {
        var map,
            markers = [];

// Our markers database (for testing)
        var locations = [
            ['Bondi Beach', -33.890542, 151.274856],
            ['Coogee Beach', -33.923036, 151.259052],
            ['Cronulla Beach', -34.028249, 151.157507],
            ['Manly Beach', -33.80010128657071, 151.28747820854187],
            ['Maroubra Beach', -33.950198, 151.259302]
        ];


        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(-33.9, 151.2),
                zoom: 9,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            // Adding our markers from our "big database"
            addMarkers();



            var polygon = new google.maps.Polygon({
                strokeColor: "#1E41AA",
                strokeOpacity: 1.0,
                strokeWeight: 3,
                map: map,
                fillColor: "#2652F2",
                fillOpacity: 0.6
            });

            var poly = polygon.getPath();

            function addPolyPoints(e) {
                console.log(markers);
                poly.push(e.latLng);
                var markerCnt = 0;
                var index =[];
                for (var i=0; i<markers.length; i++) {
                    if (google.maps.geometry.poly.containsLocation(markers[i].getPosition(), polygon)) {
                        markerCnt++;
                        index.push(i);
                    }
                }
                document.getElementById('info').innerHTML = "markers in polygon: "+markerCnt;
                document.getElementById('index').innerHTML = "markers in polygon: "+index;
            }

            google.maps.event.addListener(map, 'click', addPolyPoints);
        }

        function addMarkers() {
            for (var i = 0; i < locations.length; i++) {
                var beach = locations[i],
                    myLatLng = new google.maps.LatLng(beach[1], beach[2]),
                    marker = new google.maps.Marker({
                        position: myLatLng,
                        title: beach[0]
                    });

                marker.setMap(map);

                // Keep marker instances in a global array
                markers.push(marker);
            }
        }



        google.maps.event.addDomListener(window, 'load', initialize);

    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9KiNx3g9qZiDzgetuEHlFRpZGIcYHVio&libraries=drawing&callback=initMap"
        async defer></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>

</body>
</html>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9KiNx3g9qZiDzgetuEHlFRpZGIcYHVio&libraries=drawing&callback=initMap"
         async defer></script> -->
