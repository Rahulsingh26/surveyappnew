@extends('adminlte::page')

@section('title', 'Change Password')

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Password</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('change-password')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Current Password</label>

                            <div class="col-sm-9 @error('current_password') has-error @enderror">
                                <input type="password" name ="current_password" class="form-control" id="inputEmail3" placeholder="Current Password">
                                @error('current_password')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">New Password</label>

                            <div class="col-sm-9 @error('password') has-error @enderror">
                                <input type="password" name="password" class="form-control" id="inputPassword1" placeholder="New Password">
                                @error('password')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Confirm Password</label>
                            <div class="col-sm-9 @error('password_confirmation') has-error @enderror">
                                <input type="password" name = "password_confirmation" class="form-control" id="inputPassword2" placeholder="Confirm Password">
                                @error('password_confirmation')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('change-password') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop