@extends('adminlte::page')

@section('title', 'Change Email')


@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Email</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('change-email')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Current Email</label>

                            <div class="col-sm-9 @error('email') has-error @enderror">
                                <input type="email" name= "email" class="form-control" id="inputEmail3" value="{{old('email')}}" placeholder="Current Email">
                                @error('email')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3"  class="col-sm-3 control-label">New Email</label>

                            <div class="col-sm-9 @error('new_email') has-error @enderror">
                                <input type="email" name= "new_email" class="form-control" id="inputPassword3" placeholder="New Email">
                                @error('new_email')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('change-email') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop
