@extends('adminlte::page')

@section('title', 'Edit User')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <style>
     .help-block{
        font-weight:bold;
        color:red;
     } 
     
  </style>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Users</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('edit-user',[$user->id])}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name"  class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-9 @error('name') has-error @enderror">
                                <input type="text" name= "name" class="form-control" id="name" placeholder="Name" value="{{ $user->name }}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-9 @error('email') has-error @enderror">
                                <input type="email" name= "email" class="form-control" id="email" placeholder="Email" value="{{ old('email') ? old('email') : $user->email }}">
                                @error('email')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone-number" class="col-sm-3 control-label">Phone Number</label>

                            <div class="col-sm-9 @error('phone_number') has-error @enderror">
                                <input type="text" name= "phone_number" class="form-control" id="email" placeholder="Format should be +123456789 or 123456789" value="{{ old('phone_number') ? old('phone_number') : $user->phone_number }}">
                                @error('phone_number')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone-number" class="col-sm-3 control-label">Address</label>

                            <div class="col-sm-9">
                                <textarea class="form-control" name ="address" rows="3" placeholder="Address">{{ old('address') ? old('address') : $user->address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Type Of User</label>
                            <div class="col-sm-9">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="user_type" id="admin" value="3" onchange="accessRightDisplay()" {{ old('user_type') == "3" ? 'checked='.'checked' : $user_role[0]=="3" ? 'checked='.'checked' : '' }} >
                                        Admin
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="user_type" id="canvessar" value="2" {{ old('user_type') == "2" ? 'checked='.'checked' : old('user_type') == "3" ? '' : $user_role[0]=="2" ? 'checked='.'checked' : '' }} onchange="accessRightDisplay()">
                                        Canvasser
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group" id ="access-right" {{old('user_type') == "3" ? 'style='.'display:block'  : $user_role[0]=="3" ? 'style='.'display:block' : 'style='.'display:none'  }} >
                            <label for="email" class="col-sm-3 control-label">Access Rights</label>
                            <div class="col-sm-9">
                                <div class="col-sm-9 @error('rights') has-error @enderror">
                                @foreach($permissions as $permission)
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name = "rights[]" value="{{$permission->id}}" {{in_array($permission->id,$user_permissions) ? 'checked='.'checked' : '' }}>
                                        {{  ucfirst($permission->name) }}
                                    </label>
                                </div>
                                @endforeach
                                    @error('rights')
                                    <span class="help-block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">User Status</label>
                            <div class="col-sm-9">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" value = 1 {{ $user->status =="1" ? 'checked='.'checked' : '' }}>
                                        Active
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" value = 0 {{ $user->status =="0" ? 'checked='.'checked' : '' }} >
                                        Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop