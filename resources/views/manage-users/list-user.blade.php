@extends('adminlte::page')

@section('title', 'List Users')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
                <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">User List</h3>
                </div>
            <div class="box-body table-responsive no-padding">
            
                @isset($users)
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Permission</th>
                            <th>Status</th>
                            @can('write')
                            <th>Edit</th>
                            @endcan
                        </tr>
                            @foreach ($users as $key => $user)
                 
                                <tr>
                                    <td>{{ (($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ ucwords(isset($user->getRoleNames()[0]))?$user->getRoleNames()[0]:''}}</td>
                                    <td>
                                        {{  \App\Helper\AppHelper::getPermissionsName($user->getAllPermissions()) }}
                                    </td>
                                    <td>
                                        <span class="label {{ $user->status ==1 ? 'label-success' : 'label-danger'}}">
                                        @if($user->status ==1)
                                            Active
                                        @else
                                            Inactive
                                        @endif
                                        </span>
                                    </td>
                                    <td>
                                    @can('write')
                                        <a href="{{route('edit-user',[$user->id])}}" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                    @endcan
                                    </td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endisset
            </div>

            <div class="clearfix">
                {{ $users->links() }}
            </div>
                </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
