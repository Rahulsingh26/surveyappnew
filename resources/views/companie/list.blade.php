@extends('adminlte::page')

@section('title', 'Company List')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Company List</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    @isset($company)
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Sub domain</th>
                                <th>Subscription</th>
                                <th>view</th>

                            </tr>
                            @php
                                $i = $company->perPage() * ($company->currentPage()-1) + 1;
                            @endphp
                            @foreach ($company as $key => $companie)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $companie->name }}</td>
                                    <td>{{ $companie->email }}</td>
                                    <td>{{$companie->companie[0]->subdomain}}</td>
                                    @if ($companie->status == 1)
                                        <td>
                                            <a href="{{ url('companies/status-update/'.$companie->id.'/'.$companie->status)}}" class="label-success" onclick="if(!confirm('Do You Want To Update status?')){return false;}">Active</a>
                                        </td>
                                    @else
                                        <td><a href="{{ url('companies/status-update/'.$companie->id.'/'.$companie->status)}}" class="label-danger" onclick="if(!confirm('Do You Want To Update status?')){return false;}">Inactive</a></td>
                                    @endif
                                    <td>
                                        <a href="{{route('view-companie',$companie->companie_id)}}" title="View"><i class="fa fa-fw fa-eye"></i></a>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div>No Record Found</div>
                    @endisset
                </div>

                <div class="clearfix">
                    {{ $company->links() }}
                </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('public/js/custom.js') }}"></script>
@stop
