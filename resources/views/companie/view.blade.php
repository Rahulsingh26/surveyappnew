@extends('adminlte::page')
@section('title', 'Company details')
@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3></h3>
                    <p></p>
                </div>
                <div class="box-body">
                    <h2 class="page-header">Company Details</h2>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="customer_details_box">
                                        <div class="customer_image">
                                            <img src= "{{asset($company[0]->image)}}" style="width: 80%;">
                                        </div>
                                        <div class="customer_final_details">
                                            <h3 style="text-transform: uppercase;">{{$company[0]->name}}</h3>
                                            <p><strong>Address:</strong> {{$company[0]->address}}</p>
                                            <p><strong>Email:</strong> {{$company[0]->email}} </p>
                                            <p><strong>Subdomain:</strong> {{$company[0]->companie[0]->subdomain}}</p>
                                        </div>
                                    </div>
                                    @isset($users)
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Permission</th>
                                            <th>Status</th>

                                        </tr>
                                        @foreach ($users as $key => $user)
                                            <tr>
                                               <td>{{ (($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration}}</td>
                                               <td>{{ $user->email }}</td>
                                               <td>{{ ucwords(isset($user->getRoleNames()[0]))?$user->getRoleNames()[0]:''}}</td>
                                               <td> {{  \App\Helper\AppHelper::getPermissionsName($user->getAllPermissions()) }}</td>
                                               <td>
                                                   <span class="label {{ $user->status ==1 ? 'label-success' : 'label-danger'}}">
                                                        @if($user->status ==1)
                                                           Active
                                                         @else
                                                           Inactive
                                                         @endif
                                                    </span>
                                               </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        <div>No Record Found</div>
                                    @endisset
                                </div>
                                <!-- /.box-body -->
                                <div class="clearfix">
                                    {{ $users->links() }}
                                </div>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('custom_js')
    <script src="{{ asset('public/js/custom.js') }}"></script>
@stop
