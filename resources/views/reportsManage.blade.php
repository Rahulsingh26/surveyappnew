@extends('adminlte::page')

@section('title', 'Reports Manage')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header" align="Center">
                    <h3 class="box-title">Report Management</h3>
                    <div class="box-tools pull-right">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-3">
                        <strong><h7>Survey List</h7></strong>
                        <select class="surveyList form-control">
                            <option value="">Select Survey</option>
                            @foreach($surveyList as $list)
                                <option value="{{$list->id}}">{{$list->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <strong><h7>Survey Question List</h7></strong>

                        <select class="ques form-control" id="response">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-lg-6 custom-btn">
                        <div class="btn-group">
                            <button class="btn btn-info" onclick="getGraphData()" id="graphfilterbtn"> Filter graph data</button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <canvas id="canvas" height="100" width="600"></canvas>
                    </div>

                </div>
            </div>
            <div class="box box-info">
                    <div class="box-header" align="Center">
                        <h3 class="box-title">Download Reports</h3>
                        <div class="box-tools pull-right">
                            <!-- Collapse Button -->
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-3">
                            <strong><h7>Walkbook list</h7></strong>
                            <select class="walkbookList form-control">
                                <option value=""> Select Walkbook</option>
                                @foreach($walkbookList as $list)
                                    <option value="{{$list->id}}">{{$list->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-6 custom-btn">
                            <div class="btn-group">
                                <button class="btn btn-info" onclick="downloadWalkbookReport()"> Download Walkbook Individual's List</button>
                            </div>
                        </div>
                        <div class="panel-body">

                        </div>

                    </div>
            </div>

        </div>
    </div>

@stop

@section('custom_js')
    <script>
        var myPieChart = null;
        $(document).ready(function(){
            $("#canvas").hide();
            $("#graphfilterbtn").prop('disabled', true);
            $("select.surveyList").change(function(){
                var selectedSurveyList = $(".surveyList option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('survey.question') }}",
                    data: {_token:"{{csrf_token()}}",surveylist : selectedSurveyList }
                }).done(function(data){
                    $('#response option').remove();
                    if(data.length){
                        $("#graphfilterbtn").prop('disabled', false);
                        $.each(data,function(key,value){
                            $("#response").append('<option value="'+value['id']+'">'+value['title']+'</option>');
                        });
                    }else{
                        $("#canvas").hide();
                        $("#graphfilterbtn").prop('disabled', true);
                    }

                });
            });

            //render pie charts
            var ctx = document.getElementById("canvas").getContext('2d');
            var data = {
                datasets: [{
                    data: [],
                    backgroundColor: ['#ff6384', '#36a2eb', '#ffce56', '#4bc0c0', '#9966ff'],
                }],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: []
            };

            myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data
            });
        });

        function getGraphData(){
            var selectedQuesList = $(".ques option:selected").val();
            var surveyList = $(".surveyList option:selected").val();

            $.ajax({
                type: "POST",
                url: "{{ route('question.report') }}",
                data: {_token:"{{csrf_token()}}",surveylist : surveyList , quesId: selectedQuesList }
            }).done(function(data){
                updateGraphData(myPieChart, data.labels, data.data);
            });
        }

        function updateGraphData(chart, labels, data) {
            elementSum = data.reduce((a, b) => a + b, 0);
            if(!elementSum){
                $("#canvas").hide();
                alert("There is no data")
                return
            }
            $("#canvas").show();
            myPieChart.data.labels = labels;
            myPieChart.data.datasets[0]["data"] = data;
            myPieChart.update();
        }

        //download walkbook reports
        function downloadWalkbookReport() {
            var walkbookId = $(".walkbookList option:selected").val();
            if(walkbookId !=''){
                var url = "{{URL::to('/manage-reports/download-walkbook')}}/" + walkbookId
                window.location.href = url;
            }else{
                alert("Please select walkbook from list")
            }
        }
    </script>

@stop
