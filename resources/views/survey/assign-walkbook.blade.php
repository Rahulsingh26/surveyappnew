@extends('adminlte::page')

@section('title', 'List Of Surveys')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row" id="result">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
               @if(isset($data[0]))
                <div class="box-header" align="center">
                    <h3 class="box-title">{{$data[0]->survey->title}}</h3>
                </div>
                @endif
                @if(isset($data[0]))
                   <div class="box-header" align="right">
                       <a href="{{route('add.walkbook',[$data[0]->survey_id])}}" class="box-title">Add Walkbook</a>
                   </div>
                @else
                       <div class="box-header" align="right">
                           <a href="{{ route('add.NewWalkbook',$id)}}" class="box-title">Add Walkbook</a>
                       </div>
                 @endif
                <div class="box-body table-responsive no-padding">
                    @if(count($data))
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>Title</th>
                                <th>Action</th>


                            </tr>
                            @foreach ($data as $key => $survey)
                                <tr>
                                    <td>{{ (($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                    <td>@if(isset($survey->walkbook)){{$survey->walkbook->title}}@else @endif</td>
                                    <td><a href="{{route('walkbook.delete',[$survey->id])}}" title="Delete"><i class="fa fa-fw fa-trash"></i></a></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div>No Record Found</div>
                    @endif
                </div>

                <div class="clearfix">
                    {{ $data->appends(['type' => app('request')->input('type'),'value'=>app('request')->input('value')])->links() }}
                </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>

@stop
