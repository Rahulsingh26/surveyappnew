@extends('adminlte::page')

@section('title', 'List Of Surveys')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row" id="result">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Surveys List</h3>
                </div>
            <div class="box-body table-responsive no-padding">
                @if(count($surveys))
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned Walkbook List</th>
                            <th>Video</th>
                            @if(auth()->user()->can('write'))
                                <th>Edit</th>
                            @else
                                <th>View</th>
                            @endif
                            <th>Created At</th>
                            <th>User created or updated</th>
                            <th>Updated At</th>

                        </tr>
                            @foreach ($surveys as $key => $survey)
                                <tr>
                                    <td>{{ (($surveys->currentPage() - 1 ) * $surveys->perPage() ) + $loop->iteration}}</td>
                                    <td>{{ $survey->title }}</td>
                                    <td>{{ $survey->description }}</td>
                                    <td class="text text-success">
                                            <a href="{{route('walkbook.survey',[$survey->id])}}">List Walkbook </a>
                                    </td>
                                    <td> {!!  isset($survey->video->file_name) ? "<a href='".url('/uploads/videos/'.$survey->video->file_name)."' target='_blank'>Click to see video</a>" : "N/A" !!}</td>
                                    <td>
                                    @if(auth()->user()->can('write'))
                                            <a href="{{route('detail.survey',[$survey->id])}}" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                    @else
                                            <a href="{{route('view.survey',[$survey->id])}}" title="View"><i class="fa fa-fw fa-eye"></i></a>
                                    @endif
                                    </td>
                                    <td>{{ $survey->created_at }}</td>
                                    <td>{{ (App\Helper\AppHelper::getUserCreateEditName($survey->userCreatedSurveyID))?(App\Helper\AppHelper::getUserCreateEditName($survey->userCreatedSurveyID)):'Deleted user' }}</td>
                                    <td>{{ $survey->created_at }}</td>

                                </tr>
                            @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endif
            </div>

            <div class="clearfix">
                {{ $surveys->appends(['type' => app('request')->input('type'),'value'=>app('request')->input('value')])->links() }}
            </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>

    <script>
        $(document).ready(function () {
            $(document).on("click", ".deactivated_survey", function () {
                id = $(this).attr('data-type');
                url="{{ route('deactivated-survey') }}";
                data={_token:"{{csrf_token()}}",'id':id};

                $.post(url,data,function(rdata){
                    location.reload(true);
                    console.log(rdata);
                })
            })
        })
    </script>
@stop
