@extends('adminlte::page')
@section('title', 'Survey Details')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3>{{ $survey->title }}</h3>
                    <p>{{ $survey->description }}</p>
                </div>
                <div class="box-body">
                    <a href="{{$survey->id}}/edit">Edit Survey</a> |  <a href="#" data-toggle="modal" data-target="#survey-delete" class="">Delete Survey</a>
                    <div class="modal fade" id="survey-delete">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form method="get" action ="{{route('delete.survey',[$survey->id])}}">
                                    @csrf
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Are you sure you want to delete this survey ?</h4>
                                </div>
                                <div class="modal-body">
                                    <p> Survey Title: {{ $survey->title }}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" id="" data-survey-id = "{{ $survey->id }}">Delete</button>
                                </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <h2 class="page-header">Questions</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-group" id="accordion">
                                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                        @forelse ($survey->questions as $key => $question)
                                        <div class="panel box box-primary">
                                            <div class="box-header with-border">
                                                    <span data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$key}}" style="cursor: pointer">
                                                        {{ $question->title }}
                                                    </span>
                                                <a href="#" style="float:right;margin-left:10px;" data-toggle="modal" data-target="#modal-default">Delete</a>
                                                <a href="question/{{ $question->id }}/edit" style="float:right;">Edit</a>
                                                <form method="get" action ="{{route('delete.question',[$question->id])}}">
                                                    @csrf
                                                <div class="modal fade" id="modal-default">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Are you sure you want to delete this question ?</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p> Question Title: {{ $question->title }}</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary" id="delete_question" data-question-id = "{{ $question->id }}">Delete</button>
                                                            </div>

                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                </form>
                                            </div>
                                            <div id="collapse-{{$key}}" class="panel-collapse collapse">
                                                <div class="box-body">
                                                    @if($question->question_type === 1)
                                                        <label>Type</label> : Text
                                                    @elseif($question->question_type === 2)
                                                        <label> Type</label> : TextArea
                                                    @elseif($question->question_type === 3)
                                                        @if($question->option_name!=null && !in_array('null',$question->option_name) )
                                                            @foreach($question->option_name as $key=>$value)
                                                                <p style="margin:0px; padding:0px;display:flex;">
                                                                    <input type="radio" id="{{ $key }}"  disabled/>
                                                                    <label for="{{ $key }}" style="margin-left:5px;">{{ $value }}</label>
                                                                </p>
                                                            @endforeach
                                                         @else
                                                            <label>Type</label> : Radio
                                                            <p>No options Added</p>
                                                        @endif
                                                    @elseif($question->question_type === 4)
                                                        @if($question->option_name!=null && !in_array('null',$question->option_name) )
                                                            @foreach($question->option_name as $key=>$value)
                                                                <p style="margin:0px; padding:0px;display:flex;">
                                                                    <input type="checkbox" id="{{ $key }}" disabled />
                                                                    <label for="{{$key}}" style="margin-left:5px;">{{ $value }}</label>
                                                                </p>
                                                            @endforeach
                                                        @else
                                                            <label>Type</label> : Checkbox
                                                            <p>No options Added</p>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @empty
                                            <div class="panel box box-primary" style="padding:10px;">Nothing to show. Add questions below.</span>
                                        @endforelse
                                                <h2 class="flow-text">Add Question</h2>
                                                <form method="POST" action="{{ route('store.question',[$survey->id])  }}" id="boolean">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="form-group">
                                                        <div class="row">
                                                        <label for="title" class="col-sm-3">Question Type</label>

                                                        <div class="col-sm-6 @error('question_type') has-error @enderror">
                                                            <select class="form-control" name="question_type" id="question_type">
                                                                <option value="" disabled selected>Choose your option</option>
                                                                <option value=1 {{old('question_type') == 1 ? 'selected' : ''}}>Text</option>
                                                                <option value=2 {{old('question_type') == 2 ? 'selected' : ''}}>Textarea</option>
                                                                <option value=3 {{old('question_type') == 3 ? 'selected' : ''}}>Radio Buttons</option>
                                                                <option value=4 {{old('question_type') == 4 ? 'selected' : ''}}>Checkbox</option>
                                                            </select>
                                                            @error('question_type')
                                                            <span class="help-block">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                        <label for="title"  class="col-sm-3 control-label">Question</label>

                                                        <div class="col-sm-6 @error('title') has-error @enderror">
                                                            <input type="text" name= "title" class="form-control" id="title" placeholder="Title" value="{{old('title')}}">
                                                            @error('title')
                                                            <span class="help-block">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-g">
                                                        @if(old('question_type') ==4 || old('question_type') ==3)
                                                            @if($errors->first('option_name.0') || $errors->first('title'))
                                                            <div class="form-group options"><div class="row">
                                                                    <label for="option_name" class="col-sm-3">Option Name</label>
                                                                    <div class="col-sm-3 option_input @error('option_name.0') has-error @enderror">
                                                                        <input name="option_name[]" type="text" class="form-control option_name" value="{{old('option_name.0')}}">
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('option_name.0') }}</strong>
                                                                        </span>
                                                                    </div>
                                                                    <span class="add-option" style="cursor:pointer;margin-right:20px;">Add Another</span>
                                                                    </div>

                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-3"></div>
                                                        <div class="col-sm-6">
                                                        <button type="submit" class="btn btn-info">Submit</button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </form>

                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('custom_js')
  <script src="{{ asset('js/custom.js') }}"></script>
@stop
