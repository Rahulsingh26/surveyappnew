@extends('adminlte::page')

@section('title', 'List Of Surveys')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row" id="result">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header" align="Center">
                    <h3 class="box-title">Surveys List</h3>



                    <div>
                        <a href="{{route('surveyHistoryDetails.Download')}}" class="btn btn-primary" style="float:right;">Export all Status</a>
                        <form action="" method="get">
                            @csrf
                            <div class="row">
                               <!--  <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Voter ID</label>
                                        <input type="text" value="{{ Request::get('voter_ID') }} " class="form-control"
                                               name="voter_ID">

                                    </div>
                                </div> -->

                                   <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Canvaser Name</label>
                                        @php
                                            $user = \App\Helper\AppHelper::getUserDetails(Request::get('canvasser_id'));
                                        @endphp
                                        <input type="text" value="" placeholder="Search Prospect using canvaser Name" class="form-control"
                                               name="canvasser_name">

                                    </div>
                                </div>

                                 <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Date</label>
                                        @php
                                            $user = \App\Helper\AppHelper::getUserDetails(Request::get('canvasser_id'));
                                        @endphp
                                        <input type="date" value="" class="form-control"
                                               name="date">

                                    </div>
                                </div>
                                   <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Impediments</label>
                                        @php
                                            $user = \App\Helper\AppHelper::getUserDetails(Request::get('canvasser_id'));
                                        @endphp
                                        <input type="text" value="" class="form-control"
                                               name="impediments">

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        <select name="status" title="" style="width: 100%; height: 34px;">
                                            <option value="">Select</option>
                                            <option value="1">Complete</option>
                                            <option value="2">Pending</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div align="left">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Search</button>
                                <a class="btn btn-default" href="{{ route ('histroy.survey') }}">Reset</a>
                            </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="box-body table-responsive no-padding">
                    @if(count($data))
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>Prospect Name</th>
                                <th>Voter's ID</th>
                                <th>Date</th>
                                <th>Observations</th>
                                <th>Impediments</th>
                                <th>Status</th>
                                <th>Views</th>



                            </tr>
                            @forelse ($data as $key => $survey)



                                <tr>
                                    <td>{{ (($data->currentPage() - 1 ) * $data->perPage() ) + $loop->iteration}}</td>
                                    <td>
                                        {{$survey->prospect}}
                                    </td>
                                    <td>
                                        {{$survey->voter_id}}
                                    </td>
                                    <td>
                                        {{$survey->CreateOn_date}}
                                    </td>
                                      <td>                       
                                        @if ( !empty( $survey->additional_observation ) )
                                            @php $impediments = implode(',',  $survey->additional_observation) @endphp
                                            {{$impediments}}
                                          @else
                                          {{'N/A'}}
                                          @endif
                                    </td>
                                      <td>
                                        {{!empty($survey->other_impediments)?$survey->other_impediments : 'N/A'}}
                                    </td>
                                    <td>
                                        @if(isset($survey->status) && $survey->status == 1)
                                            <span class="label label-primary" title="">Completed</span>
                                            @else
                                                <span class="label label-primary" title="">Pending</span>
                                        @endIf
                                    </td>
                                    <td>
                                        <a href="{{route('histroyDetails.survey',[$survey->id])}}" title="View"><i class="fa fa-fw fa-eye"></i></a>
                                    </td>

                                </tr>
                                @empty
                                    <tr>
                                        <th colspan="7" class="text-center text-danger"> list is empty</th>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    @else
                        <div class="text-center text-danger">No Record Found</div>
                    @endif
                </div>

                <div class="clearfix">
                    {{ $data->appends(['type' => app('request')->input('type'),'value'=>app('request')->input('value')])->links() }}
                </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>


@stop
