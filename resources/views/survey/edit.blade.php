@extends('adminlte::page')

@section('title', 'Edit Survey')
@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Survey</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('update.survey',[$survey->id])}}" method="post">

                    @csrf
                    
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title"  class="col-sm-3 control-label">Title</label>

                            <div class="col-sm-9 @error('title') has-error @enderror">
                                <input type="text" name= "title" class="form-control" id="title" placeholder="Title" value="{{$survey->title}}">
                                @error('title')
                                <span class="help-block">
                    <strong>{{ $message }}</strong>
                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-9">
                                <textarea class="form-control" name ="description" rows="3" placeholder="Description">{{$survey->description}}</textarea>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="description" class="col-sm-3 control-label">Video</label>

                            <div class="col-sm-9">
                                <select name="video_id" class="form-control assign_video">
                                    <option value =''>Select</option>
                                    @foreach($videos as $video)
                                        <option value ='{{$video->id}}' data-video="{{$video->file_name ? url('/uploads/videos/'.$video->file_name) : '' }}" {{isset($survey->video_id) && $survey->video_id == $video->id   ? 'selected' : ''}}>{{$video->title}}</option>
                                    @endforeach
                                </select>
                                <span class="video_link">
                                    {!!  isset($survey->video->file_name) ? "<a href='".url('/uploads/videos/'.$survey->video->file_name)."' target='_blank'>".url('/uploads/videos/'.$survey->video->file_name)."</a>" : "" !!}
                                </span>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('edit.survey',[$survey->id]) }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>


                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop
@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
