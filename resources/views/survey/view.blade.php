@extends('adminlte::page')
@section('title', 'View Survey')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3>{{ $survey->title }}</h3>
                    <p>{{ $survey->description }}</p>
                </div>
                <div class="box-body">
                    <h2 class="page-header">Questions</h2>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-group" id="accordion">
                                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                        @forelse ($survey->questions as $key => $question)
                                            <div class="panel box box-primary">
                                                <div class="box-header with-border">
                                                    <span data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$key}}">
                                                        {{ $question->title }}
                                                    </span>
                                                    </div>
                                                </div>
                                                <div id="collapse-{{$key}}" class="panel-collapse collapse">
                                                    <div class="box-body">
                                                        @if($question->question_type === 1)
                                                            <label>Type</label> : Text
                                                        @elseif($question->question_type === 2)
                                                            <label> Type</label> : TextArea
                                                        @elseif($question->question_type === 3)
                                                            @foreach($question->option_name as $key=>$value)
                                                                <p style="margin:0px; padding:0px;">
                                                                    <input type="radio" id="{{ $key }}"  disabled/>
                                                                    <label for="{{ $key }}">{{ $value }}</label>
                                                                </p>
                                                            @endforeach
                                                        @elseif($question->question_type === 4)
                                                            @foreach($question->option_name as $key=>$value)
                                                                <p style="margin:0px; padding:0px;">
                                                                    <input type="checkbox" id="{{ $key }}" disabled />
                                                                    <label for="{{$key}}">{{ $value }}</label>
                                                                </p>
                                                            @endforeach

                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="panel box box-primary" style="padding:10px;">Nothing to show. Add questions below.</span>
                                                @endforelse

                                            </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
