@extends('adminlte::page')

@section('title', 'List Of Surveys')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row" id="result">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">History Details</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    @if(count($arr))
                            @php $i=1; @endphp
                            @foreach ($arr as $key => $survey)
                                <div class="col-sm-12">
                                    Question {{$i}}: <strong>{{isset($survey->title) ? $survey->title : "N/A"}}</strong>
                                    <br>
                                    @isset($survey->question_type)
                                    @if($survey->question_type === 1)
                                    <label>Answer</label> : {{isset($survey->answer) ? $survey->answer : "N/A"}}
                                    @elseif($survey->question_type === 2)
                                        <label> Answer</label> : {{isset($survey->answer) ? $survey->answer : "N/A"}}
                                    @elseif($survey->question_type === 3)
                                        <label> Answer</label> : {{isset($survey->answer) ? $survey->answer : "N/A"}}
                                    @elseif($survey->question_type === 4)
                                        @if($survey->answer!=null && !in_array('null',$survey->answer) )
                                            <label>Answer:</label>
                                            @foreach($survey->answer as $key=>$value)
                                                <p style="margin:0px; padding:0px;display:flex;">
                                                    <input type="checkbox" id="{{ $key }}" disabled checked/>
                                                    <label for="{{$key}}" style="margin-left:5px;">{{ $value }}</label>
                                                </p>
                                            @endforeach
                                        @else
                                            <label>Type</label> : Checkbox
                                            <p>N/A</p>
                                        @endif
                                    @endif

                                    @endisset

                                </div>
                            @php $i++; @endphp
                            @endforeach


                    @else
                        <div>No Record Found</div>
                    @endif
                </div>


            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>


@stop
