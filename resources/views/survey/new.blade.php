@extends('adminlte::page')
@section('title', 'Add Survey')
@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{width:100% !important;}
        /*.select2-container--default .select2-selection--multiple .select2-selection__rendered {*/
        /*position: absolute;*/
        /*bottom: -28px;*/
        /*}*/
        .select2-container--default .select2-selection--multiple .select2-selection__rendered{padding-top:30px;margin-top:10px;}
        .select2-container--default .select2-selection--multiple{border:none}
        li.select2-search.select2-search--inline{position: absolute;
            top: 0px;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            /* background: red; */
            border: 1px solid #d2d6de;}
        .select2-container--default.select2-container--focus .select2-selection--multiple{border:none;}
        /*.select2-container--default{min-height:65px;}*/
        /*.select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search{position:absolute;left:0;bottom:32px;z-index:999;}*/

    </style>
@stop
@section('content')
  <div class="row">
    <!-- left column -->
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">
      <!-- Horizontal Form -->
      @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
      @endif
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Add Survey</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="{{route('create.survey')}}" method="post" id="servey_assign">
          @csrf
          <div class="box-body">
            <div class="form-group">
              <label for="title"  class="col-sm-3 control-label">Title</label>

              <div class="col-sm-9 @error('title') has-error @enderror">
                <input type="text" name= "title" class="form-control" id="title" placeholder="Title" value="{{old('title')}}">
                @error('title')
                <span class="help-block">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <label for="description" class="col-sm-3 control-label">Description</label>

              <div class="col-sm-9">
                <textarea class="form-control" name ="description" rows="3" placeholder="Description">{{old('description')}}</textarea>
              </div>
            </div>

           {{-- <div class="form-group">
              <label for="description" class="col-sm-3 control-label">Walkbook</label>

              <div class="col-sm-9 @error('walkbook_id') has-error @enderror">
                <select name="walkbook_id" class="form-control assign_walkbook">
                  <option value =''>Select</option>
                  @foreach($walbooks as $walkbook)
                    <option value ='{{$walkbook['id']}}'>{{$walkbook['title']}}</option>
                  @endforeach
                </select>
                @error('walkbook_id')
                <span class="help-block">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>--}}

              <div class="input_fields_wrap" id="input_tag" onload="load()">
                  <div class="form-group" style="margin-bottom:50px;margin-top:35px">
                      <label for="tags"  class="col-sm-3 control-label">Walkbook</label>
                      <div class="col-sm-9 @error('walkbook_id') has-error @enderror">
                          <select class="js-example-tokenizer" name="walkbook_id[]" id="walkbook_id" multiple="multiple" style="display:none">
                              <option value =''>Select</option>
                              @foreach($walbooks as $walkbook)
                                  <option value ='{{$walkbook['id']}}'>{{$walkbook['title']}}</option>
                              @endforeach
                          </select>
                          <span class="text-danger">{{ $errors->first("walkbook_id") }}</span>


                      </div>


                  </div>
              </div>

            <div class="form-group">
              <label for="description" class="col-sm-3 control-label">Video</label>

              <div class="col-sm-9">
                <select name="video_id" class="form-control assign_video">
                  <option value =''>Select</option>
                  @foreach($videos as $video)
                    <option value ='{{$video->id}}' data-video="{{ $video->file_name ? url('/uploads/videos/'.$video->file_name) : '' }}">{{$video->title}}</option>
                  @endforeach
                </select>
              </div>
            </div>



          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a class="btn btn-default" href="{{ route ('new.survey') }}">Reset</a>
            <button type="submit" class="btn btn-info pull-right">Submit</button>
          </div>
            <input type="hidden" name="update[]" value="" id="rr">
          <!-- /.box-footer -->
        </form>
      </div>
      <!-- /.box -->
      <!-- /.box -->
    </div>
    <!--/.col (right) -->
  </div>
@stop

@section('custom_js')
  <script src="{{ asset('js/custom.js') }}"></script>
  <script>


      $(".js-example-tokenizer").select2({
          tags: true,
          tokenSeparators: [',', ' ']
      })
      var arr=[];
      $('.js-example-tokenizer').on('select2:select', function (e) {
          var data = e.params.data;
        //  console.log(data.id);
          option = data.id;
          url="{{ route('deactivated-survey') }}";
          data={_token:"{{csrf_token()}}",'id':option};

          $.post(url,data,function(rdata){
              console.log(rdata);

              if (rdata.id) {
                  if (confirm("survery has already assigned to 'walkbook' If you want to continue then new survey will override in existing survey") == true) {
                      arr.push(rdata.id)
                      document.getElementById('rr').value = arr;
                   } else {
                      var $select = $('.js-example-tokenizer');
                      var idToRemove = option;
                      var values = $select.val();
                      if (values) {
                          var i = values.indexOf(idToRemove);
                          console.log(idToRemove);
                          if (i >= 0) {
                              values.splice(i, 1);
                              $select.val(values).change();

                          }
                      }
                  }
              }
              console.log(arr);


          })
      });



      //add dynamic field for notes with timestamp

      function load(){
          document.getElementById('walkbook_id').style.display="block";
      }


  </script>
@stop
