
@extends('adminlte::page')

@section('title', 'Edit House Hold Individual')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{width:100% !important;}
        /*.select2-container--default .select2-selection--multiple .select2-selection__rendered {*/
        /*position: absolute;*/
        /*bottom: -28px;*/
        /*}*/
        .select2-container--default .select2-selection--multiple .select2-selection__rendered{padding-top:30px;margin-top:10px;}
        .select2-container--default .select2-selection--multiple{border:none}
        li.select2-search.select2-search--inline{position: absolute;
            top: 0px;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            /* background: red; */
            border: 1px solid #d2d6de;}
        .select2-container--default.select2-container--focus .select2-selection--multiple{border:none;}
        /*.select2-container--default{min-height:65px;}*/
        /*.select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search{position:absolute;left:0;bottom:32px;z-index:999;}*/

    </style>
@stop

@section('content')

    <div class="row">
{{--@php dd($surveyUser,$tagsRecord)@endphp--}}
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">@can('write')Edit @endcan House Hold Individual</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('surveyUser.edit',[$surveyUser])}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name"  class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9 @error('name') has-error @enderror">
                                    <input type="text" name= "name" class="form-control" id="name" placeholder="First Name" value="{{ $surveyUser->name }}">
                                    @error('name')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name"  class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9 @error('name') has-error @enderror">
                                    <input type="text" name= "name" class="form-control" id="last_name" placeholder="Last Name" value="{{ $surveyUser->last_name }}">
                                    @error('name')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voter_id"  class="col-sm-3 control-label">Voter ID</label>
                                <div class="col-sm-9 @error('voter_id') has-error @enderror">
                                    <input type="text" name= "voter_id" class="form-control" id="voter_id" placeholder="Voter ID" value="{{$surveyUser->voter_id}}">
                                    @error('voter_id')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone"  class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9 @error('phone') has-error @enderror">
                                    <input type="text" name= "phone" class="form-control" id="phone" placeholder="Phone" value="{{old('phone', $surveyUser->phone)}}">
                                    @error('phone')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone-number" class="col-sm-3 control-label">Precinct</label>
                                <div class="col-sm-9 @error('precinct') has-error @enderror">
                                    <textarea class="form-control" name ="precinct" rows="3" placeholder="Precinct">{{old('precinct', $surveyUser->precinct)}}</textarea>
                                        @error('precinct')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country"  class="col-sm-3 control-label">Street</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "street" class="form-control" id="street" placeholder="Street" value="{{ $surveyUser->street }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country"  class="col-sm-3 control-label">City</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "city" class="form-control" id="city" placeholder="City" value="{{$surveyUser->city}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country"  class="col-sm-3 control-label">State</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "state" class="form-control" id="state" placeholder="State" value="{{old('state', $surveyUser->state)}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="zipcode" class="col-sm-3 control-label">Zipcode</label>
                                <div class="col-sm-9 @error('zipcode') has-error @enderror">
                                    <input type="text" name= "zipcode" class="form-control" id="zipcode" placeholder="zipcode" value="{{old('zipcode', $surveyUser->zipcode)}}">
                                    @error('zipcode')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country"  class="col-sm-3 control-label">County</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "country" class="form-control" id="country" placeholder="Country" value="{{old('country', $surveyUser->country)}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_1"  class="col-sm-3 control-label">Vote History 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_1" class="form-control" id="vote_history_1" placeholder="Vote History 1" value="{{old('vote_history_1', $surveyUser->vote_history_1)}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_2"  class="col-sm-3 control-label">Vote History 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_2" class="form-control" id="vote_history_2" placeholder="Vote History 2" value="{{ $surveyUser->vote_history_2 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_3"  class="col-sm-3 control-label">Vote History 3</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_3" class="form-control" id="vote_history_3" placeholder="Vote History 3" value="{{ $surveyUser->vote_history_3 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_4"  class="col-sm-3 control-label">Vote History 4</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_4" class="form-control" id="vote_history_4" placeholder="Vote History 4" value="{{ $surveyUser->vote_history_4 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voted"  class="col-sm-3 control-label">Voted</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "voted" class="form-control" id="voted" placeholder="Voted" value="{{ $surveyUser->voted }}">
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="addnotesdata">
                                <label for="note" class="col-sm-3 control-label">Note</label>

                                <div class="col-sm-9" id="addnote">
                                    <textarea class="form-control" name ="notes[]" multiple="multiple" rows="3" placeholder="Note" id="notes"></textarea>&nbsp;
                                    @if($titleRrecord)
                                        @php $i = 1; @endphp
                                        @foreach($titleRrecord as $t)

                                    <textarea class="form-control" name ="notes[]" multiple="multiple" rows="3" placeholder="Note" id="{{$t->id}}">{{ $t->text }}</textarea>&nbsp;
                                            <a id="r_{{$t->id}}" onclick="remove({{$t->id}})" >Remove</a>
                                            @php $i++ @endphp
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-md-12 text-right" style="margin-top:12px">
                                    <button class="add_field_button btn btn-info" >Add Notes</button>
                                </div>
                            </div>
                            <div class="input_fields_wrap" id="input_tag" onload="load()">
                                <div class="form-group" style="margin-bottom:50px;margin-top:35px">
                                    <label for="tags"  class="col-sm-3 control-label">Tag</label>
                                    <div class="col-sm-9 @error('tags') has-error @enderror">

                                        @if($tagsRecord)
                                        <select class="js-example-tokenizer" name="tags[]" id="tags" value="" multiple="multiple" style="display:none">
                                            @php
                                                $tag = [];@endphp
                                                @foreach($surveyUser->tags as $tags)
                                                @php $tag[] = $tags['title'];@endphp
                                                @endforeach

                                            @foreach($tagsRecord as $tags)
                                                <option value="{{ $tags['title'] }}" {{ in_array($tags->title,$tag)?'selected':'' }}>{{ $tags['title'] }}</option>
                                            @endforeach
                                        </select>
                                        @endif
                                        @error('tags')
                                        <span class="help-block" style="position: absolute;top: -28px;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        {{--<input type="text" name= "tags[]" class="form-control" id="tag"placeholder="Tag">--}}
                                    </div>
                                    {{--<div class="col-sm-3">
                                        <button class="add_field_button btn btn-info" >Add More Tags</button>
                                    </div>--}}
                                </div>
                            </div>
                            {{--<div class="input_fields_wrap">
                                <div class="form-group">
                                    <label for="tags"  class="col-sm-3 control-label">Tag</label>
                                    <div class="col-sm-6">
                                        <input type="text" name= "tags[]" class="form-control" placeholder="Tag" value="{{ count($surveyUser->tags) ? $surveyUser->tags[0]['title'] : '' }}">
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="add_field_button btn btn-info">Add More Tags</button>
                                    </div>
                                </div>
                                @if(count($surveyUser->tags) > 1)
                                    @for($i = 1 ; $i<count($surveyUser->tags) ;$i++ )
                                        <div class="form-group">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-6"><input type="text" name= "tags[]"  class="form-control tags" placeholder="Tag" value = {{$surveyUser->tags[$i]['title']}}></div>
                                            <div><a href="#" class="remove_field">Remove</a></div>
                                        </div>
                                    @endfor
                                @endif
                            </div>--}}
                            <div class="form-group">
                                <label for="financial_contributor"  class="col-sm-3 control-label">Financial Contributor</label>
                                <div class="col-sm-9">
                                    <select name="financial_contributor" id="financialcontributor" value="{{old('financial_contributor')}}">
                                        <option value="">Select</option>
                                        <option value="Yes"  @if (old('financial_contributor') == "Yes") {{ 'selected' }} @endif>Yes</option>
                                        <option value="No"  @if (old('financial_contributor') == "No") {{ 'selected' }} @endif>No</option>
                                    </select>
                                    {{--<input type="text" name= "financial contributor" class="form-control" id="financialcontributor" placeholder="Financial Contributor" value="{{old('financialcontributor')}}">--}}
                                </div>
                                {{--<div class="col-sm-9">
                                    <input type="text" name= "financial_contributor" class="form-control" id="financial_contributor" placeholder="Financial Contributor" value="{{ $surveyUser->financial_contributor }}">
                                </div>--}}
                            </div>

                            <div class="form-group">
                                <label for="Identification"  class="col-sm-3 control-label">Identification</label>
                                <div class="col-sm-9 @error('identification') has-error @enderror">
                                    <select name="identification" id="identification">
                                            <option value="Opposed">Opposed</option>
                                            <option value="Undecided">Undecided</option>
                                            <option value="Support">Support</option>
                                            <option value="Unidentified">Unidentified</option>
                                    </select>
                                    @error('identification')
                                    <span class="help-block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="male" value="M" checked="">
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="female" value="F" {{old('gender')=="F" ? 'checked='.'checked' : '' }}>
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="age"  class="col-sm-3 control-label">Age</label>

                                <div class="col-sm-9 @error('age') has-error @enderror">
                                    <input type="text" name= "age" class="form-control" id="age" placeholder="Age" value="{{ $surveyUser->age }}">
                                    @error('age')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="contact_history_1"  class="col-sm-3 control-label">Contact History 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_1" class="form-control" id="contact_history_1" placeholder="Contact History 1" value="{{ $surveyUser->contact_history_1 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_2"  class="col-sm-3 control-label">Contact History 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_2" class="form-control" id="contact_history_2" placeholder="Contact History 2" value="{{ $surveyUser->contact_history_2}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_3"  class="col-sm-3 control-label">Contact History 3</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_3" class="form-control" id="contact_history_3" placeholder="Contact History 3" value="{{ $surveyUser->contact_history_3}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voting_location"  class="col-sm-3 control-label">Early Voting Location</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "early_voting_location" class="form-control" id="early_voting_location" placeholder="Voting Location" value="{{ $surveyUser->early_voting_location }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="election_day_location"  class="col-sm-3 control-label">Election Day Location</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "election_day_location" class="form-control" id="election_day_location" placeholder="Election Day Location" value="{{ $surveyUser->election_day_location }}">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="append_1"  class="col-sm-3 control-label">Append 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "extra_1" class="form-control" id="extra_1" placeholder="Append 1"  value="{{ $surveyUser->extra_1 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="append_2"  class="col-sm-3 control-label">Append 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "extra_2" class="form-control" id="extra_2" placeholder="Append 2"  value="{{ $surveyUser->extra_2 }}">
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.box-body -->
                    @can('write')
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('surveyUser.add') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    @endcan
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        var data = "{{ $surveyUser->Identification }}";
        var data1 = "{{ $surveyUser->financial_contributor }}";



        $(".js-example-tokenizer").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })
        $("#identification").val(data);
        $('#financialcontributor').val(data1);


        function load(){
            document.getElementById('tags').style.display="block";
        }
    function remove(i){

           // console.log(i);
        $.ajax({

            url: "{{ url('manage-households/deletenotes') }}",
            type: "post",
            data: {_token: "{{ csrf_token() }}", id : i },
            success: function(data){
               // console.log(data);
                if(data == "success") {
                  //  console.log(i);
                    document.getElementById(i).remove();
                    document.getElementById('r_'+i).remove();
                    /*document.getElementById(i).style.display = "none";
                    document.getElementById('r_'+i).style.display = "none";*/
                } else {
                    document.getElementById(i).style.display = "block";
                    document.getElementById('r_'+i).style.display = "block";
                }


            }
        });
    }


    $('#tags').on('select2:unselect', function (e) {
            var tagTitle = e.params.data.id;
            var surveyUserID = "{{ $surveyUser->id }}";
            console.log(tagTitle,surveyUserID);

                $.ajax({
                    url: "{{ url('manage-households/delete-tag') }}",
                    method: 'POST',
                    data: {
                        "tagTitle": tagTitle,
                        "surveyUserID":surveyUserID,
                        "_token": "{{csrf_token()}}",
                    },
                    success: function (data){
                        console.log(data)
                        if (data == "success") {
                            alert("Tag deleted successfully")
                        } if (data == "error") {
                            alert("Tag deleted successfully")
                        }
                    }

            });

        });

    </script>
@stop
