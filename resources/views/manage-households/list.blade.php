
@extends('adminlte::page')

@section('title', 'List House Holds')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12 list_body">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title col-sm-2" style="margin-left: -15px;">Individual List</h3>
                <div class="col-sm-2">
                    @can('upload/download')
                        <form method="get" action="{{route('surveyUser.export')}}">
                            <input type="hidden" name="type" value="{{ app('request')->input('type') ? app('request')->input('type') : '' }}"/>
                            <input type="hidden" name="value" value="{{ app('request')->input('value') ? app('request')->input('value') : '' }}"/>
                            <input type="hidden" name="csv_data_id" value="{{ app('request')->csvData ? app('request')->csvData->id : '' }}"/>
                            <input type="submit" class="btn btn-info" value="Download CSV"/>
                        </form>

                    @endcan
                </div>
                <div class="col-sm-4 add-walk-book">
                </div>
                <div class="col-sm-4" style="text-align: right">
                    @if(app('request')->csvData)
                        <form method="get" action="{{url('/manage-households/list',$csvData->id)}}">
                    @else
                         <form method="get" action="{{route('surveyUser.list')}}">
                    @endif
                        <select name="type" id="selectf" class="selectfc">
                            <option value="">Select</option>
                            <option value="name" {{ app('request')->input('type')=='name' ? 'selected' : '' }}>First Name</option>
                            <option value="last_name" {{ app('request')->input('type')=='last_name' ? 'selected' : '' }}>Last Name</option>
                            <option value="voter_id" {{ app('request')->input('type')=='voter_id' ? 'selected' : '' }}>Voter ID</option>
                            <option value="house_hold" {{ app('request')->input('type')=='house_hold' ? 'selected' : '' }}>House Hold</option>
                            <option value="precinct" {{ app('request')->input('type')=='precinct' ? 'selected' : '' }}>Precinct</option>
                            <option value="appartmentno" {{ app('request')->input('type')=='appartmentno' ? 'selected' : '' }}>Apartment No</option>
                            <option value="ethencity" {{ app('request')->input('type')=='ethencity' ? 'selected' : '' }}>Ethencity</option>
                            <option value="street" {{ app('request')->input('type')=='street' ? 'selected' : '' }}>Street</option>
                            <option value="city" {{ app('request')->input('type')=='city' ? 'selected' : '' }}>City</option>
                            <option value="state" {{ app('request')->input('type')=='state' ? 'selected' : '' }}>State</option>
                            <option value="zipcode" {{ app('request')->input('type')=='zipcode' ? 'selected' : '' }}>Zipcode</option>
                            <option value="identification" {{ app('request')->input('type')=='identification' ? 'selected' : '' }}>Identification</option>
                            <option value="Financial Contributor" {{ app('request')->input('type')=='financial_contributor' ? 'selected' : '' }}>financial contributor</option>
                            <option value="Tag" {{ app('request')->input('type')=='tag' ? 'selected' : '' }}>Tag</option>

                            {{--<option value="Tags" {{ app('request')->input('type')=='tag' ? 'selected' : '' }}>Tags</option>--}}

                        </select>
                        <input type="text" name="value" id="textserach" value="{{ app('request')->input('value') ? app('request')->input('value') : '' }}"/>
                        <input type="submit" id="subbb" class="btn btn-info" />
                    </form>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                @if(count($surveyUsers))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            @if(auth()->user()->can('write'))
                            <th style="width:95px;"><input type="checkbox" name="select_all"  id ="select_all" style="margin-right:5px;float: left;"/>Select all</th>
                            @else
                                <th>S.no</th>
                            @endif
                                {{--<th>S.no</th>--}}
                            <th>House Hold</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Voter ID</th>
                            <th>Precinct</th>
                            <th>Apartment No</th>
                            <th>Ethencity</th>
                            <th>Street</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zipcode</th>
                            <th>Financial Contributor</th>
                                <th>Support</th>
                                <th>Identification</th>
                                <th>Tag</th>
                                <th>Edit Time</th>





                            @if(auth()->user()->can('write'))
                                <th>Edit</th>
                            @else
                                <th>View</th>
                            @endif
                            <th>Edit by User</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody id="changedatatable">
                            @php
                            $i=0;$j=0;$k=0
                            @endphp
                            @foreach ($surveyUsers as $key => $surveyUser)

                                <tr>
                                    @if(auth()->user()->can('write'))
                                    <td><input type="checkbox" name="surveyUserIds[]" class="checkbox" value="{{$surveyUser->id}}"/></td>
                                    @else
                                        <td>{{ (($surveyUsers->currentPage() - 1 ) * $surveyUsers->perPage() ) + $loop->iteration}}</td>
                                    @endif
                                        {{--<td>{{ (($surveyUsers->currentPage() - 1 ) * $surveyUsers->perPage() ) + $loop->iteration}}</td>--}}
                                    <td>{{ isset($surveyUser->houseHold->name) ? $surveyUser->houseHold->name : '' }}</td>
                                    <td>{{ $surveyUser->name}}</td>
                                    <td>{{ $surveyUser->last_name}}</td>
                                    <td>{{ $surveyUser->voter_id }}</td>
                                    <td>{{ $surveyUser->precinct }}</td>
                                    <td>{{ $surveyUser->appartmentno }}</td>
                                    <td>{{ $surveyUser->ethencity }}</td>
                                    <td>{{ $surveyUser->street }}</td>
                                    <td>{{ $surveyUser->city }}</td>
                                    <td>{{ $surveyUser->state }}</td>
                                    <td>{{ $surveyUser->zipcode }}</td>
                                        <td><label class="radio-inline">
                                                <input type="checkbox"  name="optradio-@php echo ++$i;@endphp" class="check"  data-type="{{$surveyUser->id}}" id="checkData{{$surveyUser->id}}" <?= ($surveyUser->financial_contributor=="Yes")?'checked':''; ?> ></label> </td>
                                        <td><label class="radio-inline">
                                                <input type="checkbox" name="optradio-@php echo ++$j;@endphp" class="checksupport" data-type="{{$surveyUser->id}}" id="supportData{{$surveyUser->id}}" <?= ($surveyUser->Identification=="Support")?'checked':''; ?>>
                                            </label></td>
                                        <td id="userIdentification{{$surveyUser->id}}">{{ $surveyUser->Identification == "" ? 'NA': $surveyUser->Identification}}</td>

                                        <td>
                                            @if($surveyUser->tags)
                                                @foreach($surveyUser->tags->take(4) as $tags)
                                                    <span class="label label-primary">{{ $tags->title }}</span>


                                                @endforeach
                                             @else
                                                No record
                                             @endif
                                        </td>


                                        <td>{{ $surveyUser->updated_at }}</td>
                                    <td>

                                    @if(auth()->user()->can('write'))
                                            <a href="{{route('surveyUser.edit',[$surveyUser->id])}}" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                    @else
                                            <a href="{{route('surveyUser.show',[$surveyUser->id])}}" title="View"><i class="fa fa-fw fa-eye"></i></a>
                                    @endif
                                    </td>
                                    <td>{{ (App\Helper\AppHelper::getUserCreateEditName($surveyUser->userCreatedSurveyUserID))?(App\Helper\AppHelper::getUserCreateEditName($surveyUser->userCreatedSurveyUserID)):'Deleted user'}}</td>
                                    <td>{{ $surveyUser->created_at }}</td>
                                </tr>
                            @endforeach

                    </tbody>
                </table>
                @else

                    <div>No Record Found</div>
                @endif
            </div>

            <div class="clearfix">
                {{ $surveyUsers->appends(['type' => app('request')->input('type'),'value'=>app('request')->input('value')])->links() }}
            </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        $(document).ready(function(){


            var getallindividuals = localStorage.getItem("allindividuals");
            if (getallindividuals) {
                if(getallindividuals == 1){
                    $('.checkbox, #select_all').each(function(){
                        this.checked = true;
                    });
                if($('.checkbox:checked').length){
                                    if(!$('#walkbook').text().length) {
                                        addWalkbookButton();
                                    }
                                }


                    
                }else{
                    $('.checkbox,  #select_all').each(function(){
                        this.checked = false;
                    });
                }
                
            }else{
                $('.checkbox,  #select_all').each(function(){
                    this.checked = false;
                });
            }

            $('#select_all').on('click',function(){
                if(this.checked){
                    $('.checkbox').each(function(){
                        this.checked = true;
                         localStorage.setItem("allindividuals", 1);
                    });
                }else{
                    $('.checkbox').each(function(){
                        this.checked = false;
                        localStorage.setItem("allindividuals", 0);
                    });
                    $('.walkBook-container').remove();
                }
                if($('.checkbox:checked').length){
                    if(!$('#walkbook').text().length) {
                        addWalkbookButton();
                    }
                }
            });

            $('.checkbox').on('click',function(){
                if($('.checkbox:checked').length == $('.checkbox').length){
                    $('#select_all').prop('checked',true);
                }else{
                    $('#select_all').prop('checked',false);
                }
                if($('.checkbox:checked').length){
                    if(!$('#walkbook').text().length) {
                        addWalkbookButton();
                    }
                }else{
                    $('.walkBook-container').remove();
                }
            });

        $(document).on('change','#walkbook',function(){
            if($(this).val() != 0) {
                $('#walkbook_name').hide();
            }else{
                console.log('dsf');
                $('#walkbook_name').show();
            }
        })

         $(document).on('click','#add',function(){
             addsurveyUserToWalkbook()
          })
     });

        function addWalkbookButton(){
            $.ajax({
                type:'GET',
                url: "{{ route('ajax.walkbook') }}",
                success: function(result){
                    $('.add-walk-book').append(result);
                }
            });
        }

        function addsurveyUserToWalkbook(){
            var checkCheckbox = $('.checkbox:checked');
            var surveyUsersId = [];
            if(checkCheckbox.length){
                checkCheckbox.each(function(){
                    surveyUsersId.push($(this).val())
                })
            }
            var id = $('#walkbook').val();
            var title = $('#walkbook_name').val();
            var select_all = $('#select_all').val();
            var textserach = $('#textserach').val();
            var selectf = $('#selectf').val();
            $.ajax({
                type:'POST',
                url: "{{ route('addSurveyUser.walkbook') }}",
                data:{'_token':'{{csrf_token()}}','suveyUsersId':surveyUsersId,'title':title,'id':id, 'select_all':select_all, 'selectf':selectf, 'textserach':textserach},
                success: function(result){
                    var status = result.status;
                    var message = result.message;
                    var alertClass = 'alert-danger';
                    if(status == 200){
                        alertClass  ='alert-success';
                    }

                    var messageDiv = '<div class="alert '+alertClass+ ' alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+message+'</div>';
                    $(".list_body .alert").remove();
                    $(".list_body").prepend(messageDiv);
                }
            });
        }
        $(document).ready(function () {
            $(document).on("click", ".check", function () {
                self = $(this);
                var id = self.attr('data-type');
                if($('#checkData'+id).is(':checked')){
                    self = $(this);
                id = self.attr('data-type');
                data = 'Yes'
                url = "{{ route('surveyUser.finacial') }}";
                data = {_token: "{{csrf_token()}}", 'id': id, 'data': data};

                    $.post(url, data, function (rdata) {


                })


            }
            else{
                   id= self.attr('data-type');
                   data = 'No'
                    url = "{{route('surveyUser.delete')}}"
                    data = {_token: "{{csrf_token()}}", 'id': id, 'data': data};

                    $.post(url, data, function (rdata) {


                    })

                }

            })
        })



            $(document).on("click", ".checksupport", function () {
                self = $(this);
                var id = self.attr('data-type');
                if($('#supportData'+id).is(':checked')){

                    self = $(this);
                    id = self.attr('data-type');
                    data = 'Support'
                    url = "{{ route('surveyUser.support') }}";
                    data = {_token: "{{csrf_token()}}", 'id': id, 'data': data};
                    localStorage.setItem("supportData"+id, "checked");
                    if(localStorage.getItem("supportData"+id) =="checked") {
                        //alert("jhkj");
                        $('#supportData'+id).prop('chekbox',true);
                        $('#userIdentification'+id).text("Support");
                        //localStorage.setItem("supportData"+id, "true"+id);

                    }


                    $.post(url, data, function (rdata) {


                    })
                }
                else{
                    id= self.attr('data-type');
                    url = "{{route('surveyUser.deletesupport')}}"
                    data = {_token: "{{csrf_token()}}", 'id': id, 'data': "no"};

                    $.post(url, data, function (rdata) {
                        $('#userIdentification'+id).text("NA");

                    })

                }

            })


        $(document).on("change", ".selectfc", function () {
            var selecData = (this.value);
            if(selecData=="Financial Contributor") {
                var txtdata = $("#textserach").val()
                url = "{{ route('surveyUser.finance') }}";
                data = {_token: "{{csrf_token()}}", 'type': selecData, textserach: txtdata};

                $.post(url, data, function (nayncy) {
                    $("#changedatatable").html(nayncy.data)
                })
            }else{
                if(selecData==""){
                    var txtdata = $("#textserach").val()
                    url = "{{ route('surveyUser.finance') }}";
                    data = {_token: "{{csrf_token()}}", 'type': selecData, textserach: txtdata};

                    $.post(url, data, function (nayncy) {
                        $("#changedatatable").html(nayncy.data)
                    })
                }
            }
        })


    </script>


@stop
