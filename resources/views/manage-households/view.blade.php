@extends('adminlte::page')

@section('title', 'Edit House Hold Individual')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit House Hold Individual</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('surveyUser.edit',[$surveyUser])}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name"  class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name= "name" class="form-control" id="name" placeholder="Name" value="{{ $surveyUser->name }}">
                                    @error('name')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voter_id"  class="col-sm-3 control-label">Voter ID</label>
                                <div class="col-sm-9 @error('voter_id') has-error @enderror">
                                    <input type="text" name= "voter_id" class="form-control" id="voter_id" placeholder="Voter ID" value="{{$surveyUser->voter_id}}">
                                    @error('voter_id')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone"  class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9 @error('phone') has-error @enderror">
                                    <input type="text" name= "phone" class="form-control" id="phone" placeholder="Phone" value="{{ $surveyUser->phone }}">
                                    @error('phone')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone-number" class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-9">
                                    <textarea class="form-control" name ="address" rows="3" placeholder="Address">{{ $surveyUser->address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_1"  class="col-sm-3 control-label">Vote History 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_1" class="form-control" id="vote_history_1" placeholder="Vote History 1" value="{{ $surveyUser->vote_history_1 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_2"  class="col-sm-3 control-label">Vote History 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_2" class="form-control" id="vote_history_2" placeholder="Vote History 2" value="{{ $surveyUser->vote_history_2 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_3"  class="col-sm-3 control-label">Vote History 3</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_3" class="form-control" id="vote_history_3" placeholder="Vote History 3" value="{{ $surveyUser->vote_history_3 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_4"  class="col-sm-3 control-label">Vote History 4</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_4" class="form-control" id="vote_history_4" placeholder="Vote History 4" value="{{ $surveyUser->vote_history_4 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voted"  class="col-sm-3 control-label">Voted</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "voted" class="form-control" id="voted" placeholder="Voted" value="{{ $surveyUser->voted }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="age"  class="col-sm-3 control-label">Age</label>

                                <div class="col-sm-9 @error('age') has-error @enderror">
                                    <input type="text" name= "age" class="form-control" id="age" placeholder="Age" value="{{ $surveyUser->age }}">
                                    @error('age')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="male" value="M" {{ $surveyUser->gender == 'M'? 'checked' : ''  }}>
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="female" value="F" {{ $surveyUser->gender == 'F'? 'checked' : ''  }}>
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_1"  class="col-sm-3 control-label">contact History 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_1" class="form-control" id="contact_history_1" placeholder="contact History 1" value="{{ $surveyUser->contact_history_1 }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_2"  class="col-sm-3 control-label">Contact History 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_2" class="form-control" id="contact_history_2" placeholder="Contact History 2" value="{{ $surveyUser->contact_history_2}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_3"  class="col-sm-3 control-label">Contact History 3</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_3" class="form-control" id="contact_history_3" placeholder="Contact History 3" value="{{ $surveyUser->contact_history_3}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voting_location"  class="col-sm-3 control-label">Early Voting Location</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "early_voting_location" class="form-control" id="early_voting_location" placeholder="Voting Location" value="{{ $surveyUser->early_voting_location }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="election_day_location"  class="col-sm-3 control-label">Election Day Location</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "election_day_location" class="form-control" id="election_day_location" placeholder="Election Day Location" value="{{ $surveyUser->election_day_location }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="financial_contributor"  class="col-sm-3 control-label">Financial Contributor</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "financial_contributor" class="form-control" id="financial_contributor" placeholder="Financial Contributor" value="{{ $surveyUser->financial_contributor }}">
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.box-body -->
                    @can('write')
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('surveyUser.add') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    @endcan
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop