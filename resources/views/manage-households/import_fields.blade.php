@extends('adminlte::page')

@section('title', 'Map Fields')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">CSV Import</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="csv-form" method="POST" action="{{ route('surveyUser.import_process') }}">
                    {{ csrf_field() }}
                    <div class="box-body">
                    <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file->id }}" />

                    <table class="table">
                        @if (isset($csv_header_fields))
                            {{--@php dd($csv_header_fields) @endphp--}}
                            <tr>
                                <th>Fields</th>
                                <th>Mapping Fields From CSV</th>

                            </tr>

                            {{--@php dd(config('app.db_fields')) @endphp--}}
                            @foreach (config('app.db_fields') as $key =>$db_field)
                                <tr>
                                    <td>{{ $key }} <?php if($key == 'Voter ID' || $key == 'Street'){ echo "*"; } ?></td>
                                    <td>
                                        <select name="fields[{{ $db_field }}]" id ="{{$db_field}}">
                                            {{--<option value="">Select</option>--}}
                                            <option value="">Select</option>

                                            @foreach ($csv_header_fields as $row)
                                                <option value="{{ $row }}">{{ $row }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>

                    <button type="button" id="submit-form" class="btn btn-primary">
                        Import Data
                    </button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('public/js/custom.js') }}"></script>
@stop

