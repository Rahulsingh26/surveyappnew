


<form class="form-horizontal" action="{{route('nextStep')}}" method="post">
 @csrf 

<div class="row">
    <!-- left column --> 

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">List</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>Waklbook Name</th>
                            <th>Number of households</th>
                            <th>Number of individuals</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                         @foreach($excludeSave as $data)
                        <tr>                           
                            <td>
                                <h5 id="addTitle{{ $data->id }}">{{ $data->title }}</h5>
                                <p hidden>{{ $data->id }}</p>
                            </td>
                            <td>{{ $data->household }}</td>
                            <td>{{ $data->individual }}</td>  
                            <td>{{ $data->created_at }}</td>  
                            <td><a href="#" class="addTitle" title="Show data" data-value="{{ $data->id }}">Title Create</a></td> 

                        </tr>
                         @endforeach
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
    <div id="MyPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color: #3c8dbc; color: white;">
                    <button type="button" class="close" data-dismiss="modal">
                    &times;</button>
                    <h4>Create walkbook name</h4>
                </div>
                @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              {{ Session::get('message') }}</div>
              @endif
                <div class="modal-body">
                    <div class="">
                        <label for="name" >Walkbook Name</label>
                        <div >
                            <input type="text" name= "walkbookName" class="" id="walkbookName" placeholder="Name" value="">
                            <p id="rawId" hidden></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="sub">
                    Create</button>
                </div>
            </div>
        </div>
    </div>

    @foreach($CsvData as $value)
    <input type="hidden" name="csvId[]" value="{{$value}}">
    @endforeach
</div>

<div class="bottom_btn_section">
    <button type="button" class="btn btn-primary save_item" onclick='refreshPage()'>back</button>
    <button type="submit" class="btn btn-primary save_item">next</button>
   <script type="text/javascript">
            function refreshPage(){
                if(confirm("Are you sure, want to back?")){
                    location.reload();
                }               
            }
            $('.addTitle').on('click', function () {
                id = $(this).attr('data-value');
                $("#rawId").html(id);
                $("#MyPopup").modal("show");
            });
            $('#sub').on('click', function () {
                var walkbookName = $('#walkbookName').val();
                var rawId = $('#rawId').html();

                url = "{{ route('saveStep') }}";
                data = {_token: "{{csrf_token()}}", 'walkbookName': walkbookName,'rawId': rawId};
                  $.post(url, data, function (rdata) {  
                    if ($.trim(rdata) === "Error") {
                        alert('Error! Duplicate walkbookName');
                    }
                    else{
                        $('#MyPopup').modal('toggle');  
                        $("#addTitle"+rawId).html(rdata);
                    }      
                     
                });
                
                console.log(rawId);
            });
    </script>
</div>
</form>
