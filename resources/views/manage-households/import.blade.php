@extends('adminlte::page')

@section('title', 'Import CSV')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">

                    <h3 class="box-title">CSV Import</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="{{ route('surveyUser.import') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="csv_name" class="col-md-4 control-label">Name for the import</label>

                            <div class="col-md-8 @error('csv_name') has-error @enderror">

                                    <select class="csvSelectList form-control" name="csvSelectList">
                                        <option value="new" {{ old('csvSelectList') == 'new' ? 'selected' : '' }}>New</option>
                                        @foreach ($csvList as $key => $row)
                                            <option value="{{$row->id}}" {{ old('csvSelectList') == $row->id ? 'selected' : '' }}> {{$row->csv_filename}} </option>
                                        @endforeach
                                    </select>
                                    <input type="input" class="form-control csv_name" name="csv_name" placeholder="Name for the import" value="{{old('csv_name')}}">

                                    @error('csv_name')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror

                            </div>
                        </div>
                    <div class="form-group">
                        <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

                        <div class="col-md-8 @error('csv_file') has-error @enderror">
                            <input id="csv_file" type="file" class="form-control" name="csv_file" accept=".csv, .xls, .xlsx" onchange="ValidateSize(this)">
                            <input id="header" type="hidden" class="form-control" name="header" vlaue="1" >
                            @error('csv_file')
                            <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Parse CSV
                            </button>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('public/js/custom.js') }}"></script>

    <script>

        $(document).ready(function(){
            if($(".csvSelectList").val() ==="new"){
                $(".csv_name").show();
            }else{
                $(".csv_name").hide();
            }
        });

        $(".csvSelectList").on("change", function(){

            if(this.value==="new"){
                $(".csv_name").show();
                $(".csv_name").val("");
                $(".csvSelectList").parent(1).find('.help-block').show();
            }else{
                // $(".csv_name").val($(".csvSelectList option:selected").text());
                $(".csvSelectList").parent(1).find('.help-block').hide();
                $(".csv_name").hide();
            }
        });
    </script>
@stop

