@extends('adminlte::page')
@section('title', 'List Uploaded Csv')
@section('custom_css')
<link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css"/>
<style>
    .panel-collapse{display:none;}
    .opendiv{display:block;}
</style>
@stop
@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12 list_body">
        @if(Session::has('message'))
        <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('message') }}
        </div>
        @endif
        <div class="box box-info">

            <div class="box-body table-responsive no-padding">
                <div class="new_name_field">
                </br>
                <form>
                    <input type="text" name="newlistname" placeholder="Name Your List...">
                </form>
            </br>
        </div>


        <div class="col-md-6 col-sm-6">

            <div class="new_name_field"><h3 class="list_title"><span>Select Universe</span></h3></div>

            @if(count($csvData))
            <div class="panel-group raw_list_data">
                @foreach ($csvData as $key => $row)
                <div class="panel panel-default" id="{{$row->id}}">

                    <div class="panel-heading" id={{$row->id}}>
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent=""
                            href="#collapse{{$row->id}}">{{ $row->csv_filename}}

                        </a>
                    </h4>
                </div>
                <div id="collapse{{$row->id}}" class="panel-collapse collapse csv-data-get" data-value="{{$row->id}}">
                    <div class="panel-body">
                        <table style="width:100%;">
                            <tbody id="changedatatable">
                                <tr>
                                    <td>Name</td>
                                    <td><a href="{{ url('/manage-households/list',$row->id)}}"
                                     title="Show data">{{ $row->csv_filename}}</a></td>
                                 </tr>
                                 <tr class="number_household">
                                    <td>Number of households</td>
                                    <td><span>{{ $row->house_holds_number }}</span></td>
                                </tr>
                                <tr class="number_indiv">
                                    <td>Number of individuals</td>
                                    <td><span>{{ $row->individuals_number }}</span></td>
                                </tr>
                                <tr>
                                    <td>Created Date</td>
                                    <td>{{ $row->created_at }}</td>
                                </tr>
                                <tr>
                                    <td>Download</td>
                                    <td>
                                        @if($row->csv_saved_filename)
                                        <a href="{{ url( "csv-upload/".$row->csv_saved_filename) }}"
                                         download><i class="fa fa-fw fa-download "></i></a>
                                         @else
                                         NA
                                         @endif
                                     </td>
                                 </tr>
                             </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
             <span class="add_item_multi" style="display: none"><input class="styled-checkbox chk{{$row->id}}" id="cb1"
                type="checkbox" name="rowIds[]"
                value="{{$row->id}}"><label
                for="styled-checkbox-1">add</label></span>
            </div>
            @endforeach
        </div>
        @else
        <div class="text-center">No Record Found</div>
        @endif

        <div class="new_name_field">
            <h3 class="list_title"><span>Exclude</span></h3>
            <p id="csv_id" hidden></p>
        </div>


        <div class="panel-group raw_list_data" id="accordion1">
            <h4>Geography</h4>
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse14">ZIP
                        CODE</a>
                    </h4>
                </div>
                <div id="collapse14" class="panel-collapse type collapse in" data-value="zipcode">
                    <div class="panel-body">
                        <select class="ful_widthselect form-control" id="csvzipcode" name="zipcode"
                        multiple>

                    </select>
                </div>
            </div>
            <div id="zipcodeRemove" class="geographyRemove">
                <span class="exclude_item"><i class="fa fa-trash"
                  aria-hidden="true"></i>REMOVE</span>
              </div>

          </div>
          <div class="panel panel-default">

            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse141">CITY</a>
                </h4>
            </div>
            <div id="collapse141" class="panel-collapse type collapse" data-value="city">
                <div class="panel-body">
                    <select class="ful_widthselect form-control" id="csvcity" name="city" multiple>
                    </select>
                </div>
            </div>
            <div id="cityRemove" class="geographyRemove">
                <span class="exclude_item"><i class="fa fa-trash"
                  aria-hidden="true"></i>REMOVE</span>
              </div>

          </div>
          <div class="panel panel-default">

            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion1"
                    href="#collapse142">COUNTRY</a>
                </h4>
            </div>
            <div id="collapse142" class="panel-collapse collapse">
                <div class="panel-body">
                    <select class="ful_widthselect form-control" id="csvcountry" name="csvcountry"
                    multiple>
                </select>
            </div>
        </div>
        <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
    </div>
    <h4>Demographics</h4>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion1"
                href="#collapse12">GENDER</a>
            </h4>
        </div>
        <div id="collapse12" class="panel-collapse collapse">
            <div class="panel-body">
                <select class="ful_widthselect form-control" id="csvgender" name="gender"
                multiple>
            </select>
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse121">AGE</a>
        </h4>
    </div>
    <div id="collapse121" class="panel-collapse collapse">
        <div class="panel-body">
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse122">ETHNICITY</a>
        </h4>
    </div>
    <div id="collapse122" class="panel-collapse collapse">
        <div class="panel-body">
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>

<h4>Political Geography</h4>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion1"
            href="#collapse13">PRECINCTS</a>
        </h4>
    </div>
    <div id="collapse13" class="panel-collapse collapse">
        <div class="panel-body">
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse131">HOUSE
            DISTRICTS</a>
        </h4>
    </div>
    <div id="collapse131" class="panel-collapse collapse">
        <div class="panel-body">
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse132">CONGRESSIONAL
            DISTRICTS</a>
        </h4>
    </div>
    <div id="collapse132" class="panel-collapse collapse">
        <div class="panel-body">
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>
<h4>Tag</h4>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse11">TAG</a>
        </h4>
    </div>
    <div id="collapse11" class="panel-collapse collapse">
        <div class="panel-body">
        </div>
    </div>
    <span class="exclude_item"><i class="fa fa-trash" aria-hidden="true"></i>REMOVE</span>
</div>
</div>
</div>

<div class="col-md-6 col-sm-6 live_count">
    <div class="panel-group raw_list_data" id="accordion2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapsee">Live
                    Count</a>
                </h4>
            </div>
            <div id="collapsee" class="panel-collapse collapse in">
                <div class="panel-body">
                    <table style="width:100%;">
                        <tbody id="changedatatable">
                            <tr class="number_household">
                                <td><i class="fa fa-home"></i> Number of households</td>
                                <td><span id="livehousehold">-</span></td>
                            </tr>
                            <tr class="number_household">
                                <td><i class="fa fa-user"></i> Number of individuals</td>
                                <td><span id="liveindividual">-</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bottom_btn_section">
    <button type="button" class="btn btn-primary save_item">save</button>
    <button type="button" class="btn btn-info next_item">next</button>
</div>
</div>
<div class="clearfix">
    {{ $csvData->appends(['type' => app('request')->input('type'),'value'=>app('request')->input('value')])->links() }}
</div>
</div>
</div>
</div>
<!--/.col (right) -->
@stop
@section('custom_js')
<script src="{{ asset('public/js/custom.js') }}"></script>
<script>
    $(document).on('click', '#cb1', function () {
        var households = [];
        var individuals = [];
        var householdstotal = 0;
        var individualstotal = 0;

        $.each($("input[name='rowIds[]']:checked"), function (i) {
            var checkbox_id = $(this).val();
            households[i] = $('#collapse' + checkbox_id + ' tr td:last-child').eq(1).text();
            individuals[i] = $('#collapse' + checkbox_id + ' tr td:last-child').eq(2).text();
            
        });
        for (var i = 0; i < households.length; i++) {
            householdstotal += households[i] << 0;
        }
        for (var i = 0; i < individuals.length; i++) {
            individualstotal += individuals[i] << 0;
        }
        $("#livehousehold").html(householdstotal);
        $("#liveindividual").html(individualstotal);

            // Data get to display drop down box
            var csvID = [];

            $(':checkbox:checked').each(function (i) {
                csvID[i] = $(this).val();
            });
            url = "{{ route('exclude-data-get') }}";
            data = {_token: "{{csrf_token()}}", 'csvID': csvID};

            $.post(url, data, function (rdata) {
                $('#csvzipcode').empty();
                $('#csvgender').empty();
                $('#csvcity').empty();
                $('#csvcountry').empty();
                $.each(rdata.zipcode, function (j) {
                    $('#csvzipcode').append('<option value="' + rdata.zipcode[j].zipcode + '"> ' + rdata.zipcode[j].zipcode + ' </option>');

                });
                $.each(rdata.gender, function (j) {

                    $('#csvgender').append('<option value="' + rdata.gender[j].gender + '"> ' + rdata.gender[j].gender + ' </option>');
                });
                $.each(rdata.city, function (j) {
                    $('#csvcity').append('<option value="' + rdata.city[j].city + '"> ' + rdata.city[j].city + ' </option>');

                });
                $.each(rdata.country, function (j) {
                    $('#csvcountry').append('<option value="' + rdata.country[j].country + '"> ' + rdata.country[j].country + ' </option>');
                });
            });
            // CSV id append a hidden input value
            csvID.push();
            document.getElementById("csv_id").innerHTML = csvID;

            
        });

    //exlude the data

            $('.geographyRemove').on('click', function () {
                var zipcodeRemove = $('#csvzipcode').val();
                var type = document.getElementsByClassName('panel-collapse type collapse in')[0].getAttribute('data-value');
            
                for (var j = 0; j < zipcodeRemove.length; j++) {
                    $('#csvzipcode option[value=' + zipcodeRemove[j] + ']').remove();
                }
                var datazip = [];
                var zipcodepending = $("#csvzipcode option").attr("selected", false);

                for (var i = 0; i < zipcodepending.length; i++) {

                    if (datazip.indexOf(zipcodepending[i].value) < 0) {

                        datazip.push(zipcodepending[i].value);
                    }
                }
                var cityRemove = $('#csvcity').val();
                for (var j = 0; j < cityRemove.length; j++) {
                    $('#csvcity option[value=' + cityRemove[j] + ']').remove();
                }
                var datacity = [];
                var citycodepending = $("#csvcity option").attr("selected", false);

                for (var i = 0; i < citycodepending.length; i++) {

                    if (datacity.indexOf(citycodepending[i].value) < 0) {

                        datacity.push(citycodepending[i].value);
                    }
                }


                var genderRemove = $('#csvgender').val();
               /* for (var j = 0; j < genderRemove.length; j++) {
                    $('#csvcity option[value=' + genderRemove[j] + ']').remove();
                }
                var datagender = [];
                var genderpending = $("#csvcity option").attr("selected", false);

                for (var i = 0; i < genderpending.length; i++) {

                    if (datagender.indexOf(genderpending[i].value) < 0) {

                        datagender.push(genderpending[i].value);
                    }
                }*/

                var csv_id = $('#csv_id').html();

                url = "{{ route('exclude-data') }}";
                data = {_token: "{{csrf_token()}}", 'type': type,'zipcodeRemove': datazip, 'csv_id': csv_id,'city': datacity};

                $.post(url, data, function (edata) {
                    $("#livehousehold").html(edata.households);
                    $("#liveindividual").html(edata.individual);
                });

            });
    </script>
    <script>
        $(document).ready(function(){
            $( ".panel-heading" ).each(function(index) {
                $(this).on("click", function(){
                    var abc=$(this).siblings().attr('data-value');
                    $('.chk'+abc)[0].click();
                    $(this).siblings().toggleClass('opendiv');                  

                });
            });


            jQuery(function($)
             {
              $(window).scroll(function fix_element() {
                $('.live_count').css(
                  $(window).scrollTop() > 100
                    ? { 'position': 'fixed', 'top': '10px','width': '500px', 'max-width': '93%','right' : '10px'}
                    : { 'position': 'relative', 'top': 'auto' }
                );
                return fix_element;
              }());
            });
        });

    </script>


    @stop
