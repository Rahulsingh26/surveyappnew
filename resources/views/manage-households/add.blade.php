
@extends('adminlte::page')

@section('title', 'Add House Hold Individual')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .select2-container{width:100% !important;}
        /*.select2-container--default .select2-selection--multiple .select2-selection__rendered {*/
            /*position: absolute;*/
            /*bottom: -28px;*/
        /*}*/
        .select2-container--default .select2-selection--multiple .select2-selection__rendered{padding-top:30px;margin-top:10px;}
        .select2-container--default .select2-selection--multiple{border:none}
        li.select2-search.select2-search--inline{position: absolute;
            top: 0px;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            /* background: red; */
            border: 1px solid #d2d6de;}
        .select2-container--default.select2-container--focus .select2-selection--multiple{border:none;}
        /*.select2-container--default{min-height:65px;}*/
        /*.select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search{position:absolute;left:0;bottom:32px;z-index:999;}*/

    </style>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Individual Profile</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('surveyUser.add')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="name"  class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9 @error('name') has-error @enderror">
                                <input type="text" name= "name" class="form-control" id="name" placeholder="First Name" value="{{old('name')}}">
                                @error('name')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="last_name"  class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9 @error('last_name') has-error @enderror">
                                <input type="text" name= "last name" class="form-control" id="last_name" placeholder="Last Name" value="{{old('last_name')}}">
                                @error('last_name')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="voter_id"  class="col-sm-3 control-label">Voter ID</label>
                                <div class="col-sm-9 @error('voter_id') has-error @enderror">
                                    <input type="text" name= "voter_id" class="form-control" id="voter_id" placeholder="Voter ID" value="{{old('voter_id')}}">
                                    @error('voter_id')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="phone"  class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9 @error('phone') has-error @enderror">
                                <input type="text" name= "phone" class="form-control" id="phone" placeholder="Phone" value="{{old('phone')}}">
                                @error('phone')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="precinct" class="col-sm-3 control-label">Precinct</label>
                            <div class="col-sm-9 @error('precinct') has-error @enderror">
                                <textarea class="form-control" name ="precinct" rows="3" placeholder="Precinct">{{old('precinct')}}</textarea>
                                @error('precinct')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                          <div class="form-group">
                                <label for="appartmentno"  class="col-sm-3 control-label">Apartment No</label>

                                <div class="col-sm-9 @error('street') has-error @enderror">
                                    <input type="text" name= "appartmentno" class="form-control" id="appartment" placeholder="Apartment" value="{{old('appartmentno')}}">
                                    @error('appartment')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                              <div class="form-group">
                                <label for="ethencity"  class="col-sm-3 control-label">Ethencity</label>

                                <div class="col-sm-9 @error('street') has-error @enderror">
                                    <input type="text" name= "ethencity" class="form-control" id="ethencity" placeholder="Ethencity" value="{{old('ethencity')}}">
                                    @error('ethencity')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="street"  class="col-sm-3 control-label">Street</label>

                                <div class="col-sm-9 @error('street') has-error @enderror">
                                    <input type="text" name= "street" class="form-control" id="street" placeholder="Street" value="{{old('street')}}">
                                    @error('street')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="street"  class="col-sm-3 control-label">City</label>

                                <div class="col-sm-9 @error('city') has-error @enderror">
                                    <input type="text" name= "city" class="form-control" id="city" placeholder="City" value="{{old('city')}}">
                                    @error('city')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="state"  class="col-sm-3 control-label">State</label>

                                <div class="col-sm-9 @error('state') has-error @enderror">
                                    <input type="text" name= "state" class="form-control" id="state" placeholder="State" value="{{old('state')}}">
                                    @error('state')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pincode" class="col-sm-3 control-label">Zipcode</label>
                                <div class="col-sm-9 @error('zipcode') has-error @enderror">
                                    <input type="text" name= "zipcode" class="form-control" id="zipcode" placeholder="zipcode" value="{{old('zipcode')}}">
                                    @error('zipcode')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country"  class="col-sm-3 control-label">County</label>

                                <div class="col-sm-9 @error('country') has-error @enderror">
                                    <input type="text" name= "country" class="form-control" id="country" placeholder="County" value="{{old('country')}}">
                                    @error('country')
                                    <span class="help-block">
                                    <strong>The county field is required.</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_1"  class="col-sm-3 control-label">Vote History 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_1" class="form-control" id="vote_history_1" placeholder="Vote History 1" value="{{old('vote_history_1')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_2"  class="col-sm-3 control-label">Vote History 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_2" class="form-control" id="vote_history_2" placeholder="Vote History 2" value="{{old('vote_history_2')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_3"  class="col-sm-3 control-label">Vote History 3</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_3" class="form-control" id="vote_history_3" placeholder="Vote History 3" value="{{old('vote_history_3')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vote_history_4"  class="col-sm-3 control-label">Vote History 4</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "vote_history_4" class="form-control" id="vote_history_4" placeholder="Vote History 4" value="{{old('vote_history_4')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="voted"  class="col-sm-3 control-label">Voted</label>
                                <div class="col-sm-9">
                                    <input type="text" name= "voted" class="form-control" id="voted" placeholder="Voted" value="{{old('voted')}}">
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6">
                           <div class="form-group" id="addnotesdata">
                                <label for="note" class="col-sm-3 control-label">Note</label>
                                <div class="col-sm-9" id="addnote">
                                    <textarea class="form-control" name ="notes[]" multiple="multiple" rows="3" placeholder="Note" id="notes">{{old('note')}}</textarea>
                                </div>

                                <div class="col-md-12 text-right" style="margin-top:12px">
                                    <button class="add_field_button btn btn-info" >Add Notes</button>
                                </div>
                            </div>
                            <div class="input_fields_wrap" id="input_tag" onload="load()">
                                <div class="form-group" style="margin-bottom:50px;margin-top:35px">
                                    <label for="tags"  class="col-sm-3 control-label">Tag</label>
                                    <div class="col-sm-9 @error('tags') has-error @enderror">
                                        <select class="js-example-tokenizer" name="tags[]" id="tags" multiple="multiple" style="display:none">
                                        </select>
                                        @error('tags')
                                        <span class="help-block" style="position: absolute;top: -28px;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        {{--<input type="text" name= "tags[]" class="form-control" id="tag"placeholder="Tag">--}}
                                    </div>

                                    {{--<div class="col-sm-3">
                                        <button class="add_field_button btn btn-info" >Add More Tags</button>
                                    </div>--}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="financial contributor"  class="col-sm-3 control-label">Financial Contributor</label>

                                <div class="col-sm-9">
                                    <select name="financial_contributor" id="financialcontributor" value="{{old('financial_contributor')}}">
                                        <option value="">Select</option>
                                        <option value="Yes"  @if (old('financial_contributor') == "Yes") {{ 'selected' }} @endif>Yes</option>
                                        <option value="No"  @if (old('financial_contributor') == "No") {{ 'selected' }} @endif>No</option>
                                    </select>
                                    {{--<input type="text" name= "financial contributor" class="form-control" id="financialcontributor" placeholder="Financial Contributor" value="{{old('financialcontributor')}}">--}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Identification"  class="col-sm-3 control-label">Identification</label>
                                <div class="col-sm-9 @error('identification') has-error @enderror">

                                    <select name="identification" value="{{old('identification')}}">
                                        <option value="">Select</option>

                                        <option value="Support"  @if (old('identification') == "Support") {{ 'selected' }} @endif>Support</option>
                                        <option value="Undecided" @if (old('identification') == "Undecided") {{ 'selected' }} @endif>Undecided</option>
                                        <option value="Opposed" @if (old('identification') == "Opposed") {{ 'selected' }} @endif>Opposed</option>
                                        <option value="Unidentified" @if (old('identification') == "Unidentified") {{ 'selected' }} @endif>Unidentified</option>
                                    </select>



                                    @error('identification')
                                    <span class="help-block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                         @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="age"  class="col-sm-3 control-label">Age</label>
                                <div class="col-sm-9 @error('age') has-error @enderror">
                                    <input type="number"  min="1" name= "age" class="form-control" id="age" placeholder="Age" value="{{old('age')}}">
                                    @error('age')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="male" value="M" checked="">
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="female" value="F" {{old('gender')=="F" ? 'checked='.'checked' : '' }}>
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email"  class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9 @error('email') has-error @enderror">
                                    <input type="text" name= "email" class="form-control" id="email" placeholder="email" value="{{old('email')}}">
                                    @error('email')
                                    <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                         </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="contact_history_1"  class="col-sm-3 control-label">Contact History 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_1" class="form-control" id="contact_history_1" placeholder="Contact History 1" value="{{old('contact_history_1')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_2"  class="col-sm-3 control-label">Contact History 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_2" class="form-control" id="contact_history_2" placeholder="Contact History 2" value="{{old('contact_history_2')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_history_3"  class="col-sm-3 control-label">Contact History 3</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "contact_history_3" class="form-control" id="contact_history_3" placeholder="Contact History 3" value="{{old('contact_history_3')}}">
                                </div>
                            </div>

                        <div class="form-group">
                            <label for="early_voting_location"  class="col-sm-3 control-label">Early Voting Location</label>

                            <div class="col-sm-9">
                                <input type="text" name= "early_voting_location" class="form-control" id="early_voting_location" placeholder="Voting Location" value="{{old('early_voting_location')}}">
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="election_day_location"  class="col-sm-3 control-label">Election Day Location</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "election_day_location" class="form-control" id="election_day_location" placeholder="Election Day Location" value="{{old('election_day_location')}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="append_1"  class="col-sm-3 control-label">Append 1</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "extra_1" class="form-control" id="extra_1" placeholder="Append 1" value="{{old('extra_1')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="append_2"  class="col-sm-3 control-label">Append 2</label>

                                <div class="col-sm-9">
                                    <input type="text" name= "extra_2" class="form-control" id="extra_2" placeholder="Append 2" value="{{old('extra_2')}}">
                                </div>
                            </div>


                    </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('surveyUser.add') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        $(".js-example-tokenizer").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })

        //add dynamic field for notes with timestamp

        function load(){
            document.getElementById('tags').style.display="block";
        }


    </script>

@stop
