
@extends('adminlte::page')

@section('title', 'List Uploaded Csv')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12 list_body">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Csv Data</h3>
                </div>

            <div class="box-body table-responsive no-padding">
                @if(count($csvData))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Upload CSV file</th>
                            <th>Number of households</th>
                            <th>Number of individuals</th>
                            <th>Created Date</th>
                            <th>Download</th>

                        </tr>
                    </thead>
                    <tbody id="changedatatable">

                            @foreach ($csvData as $key => $row)

                                <tr>
                                    <td><a href="{{ url('/manage-households/list',$row->id)}}" title="Show data">{{ $row->csv_filename}}</a></td>
                                    <td><a href="{{ url('/manage-households/createcsv-list',$row->id)}}" title="Show data">View</a></td>
                                    <td>{{ $row->house_holds_number }}</td>
                                    <td>{{ $row->individuals_number }}</td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>
                                        @if($row->csv_saved_filename)
                                            <a href="{{ url( "public/csv-upload/".$row->csv_saved_filename) }}" download><i class="fa fa-fw fa-download "></i></a>
                                        @else
                                            NA
                                        @endif

                                </tr>
                            @endforeach

                    </tbody>
                </table>
                @else

                    <div class="text-center">No Record Found</div>
                @endif
            </div>

            <div class="clearfix">
                {{ $csvData->appends(['type' => app('request')->input('type'),'value'=>app('request')->input('value')])->links() }}
            </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('public/js/custom.js') }}"></script>
@stop
