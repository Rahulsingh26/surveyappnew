@extends('adminlte::page')

@section('title', 'Fraud Detection')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header" align="Center">
                    <h3 class="box-title">Assign Canvasser List</h3>
                    <div style="display:flex;flex-direction:row;justify-content:space-between;">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Canvasser Name</label>
                                        <select name="canvessar_id" title="" style="width: 100%; height: 34px;">
                                            <option value="">Select</option>
                                             @foreach($canvessa as $key => $canvessar)
                                                @foreach($canvessar->CanvessarUser as $cdata)                                             
                                                <option value="{{$cdata->id}}">{{$cdata->name}} ( {{$cdata->email}} )</option>
                                            @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div align="left">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                    <a class="btn btn-default" href="{{ route ('fraud.walkbook') }}">Reset</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                 @if(isset($data)) 
                <div class="box-body table-responsive no-padding">               
             
                        @php $count= 1; @endphp
                        <table class="table table-hover" id="networkStatus">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>name</th>
                                <th>Address</th>
                              

                            </tr>
                            @foreach ($data as $key => $SurveUser)

                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$SurveUser['name']}}</td>
                                    <td>{{$SurveUser['precinct']}},{{$SurveUser['street']}} {{$SurveUser['city']}} {{$SurveUser['state']}} {{$SurveUser['country']}}</td>
                                </tr>
                              @php $count++; @endphp
                             @endforeach
                            </tbody>
                        </table>
                   
                </div>

                <!--  -->
                
                @else
                   <div>No Record Found</div>
                @endif
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
