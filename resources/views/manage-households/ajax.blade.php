
@php
    $i=0;$j=0;$k=0
@endphp
@foreach ($surveyUsers as $key => $surveyUser)

    <tr>
        @if(auth()->user()->can('write'))
            <td><input type="checkbox" name="surveyUserIds[]" class="checkbox" value="{{$surveyUser->id}}"/></td>
        @else
            <td>{{ (($surveyUsers->currentPage() - 1 ) * $surveyUsers->perPage() ) + $loop->iteration}}</td>
        @endif
        {{--<td>{{ (($surveyUsers->currentPage() - 1 ) * $surveyUsers->perPage() ) + $loop->iteration}}</td>--}}
        <td>{{ isset($surveyUser->houseHold->name) ? $surveyUser->houseHold->name : '' }}</td>
        <td>{{ $surveyUser->name}}</td>
        <td>{{ $surveyUser->voter_id }}</td>
        <td>{{ $surveyUser->precinct }}</td>
        <td>{{ $surveyUser->street }}</td>
        <td>{{ $surveyUser->city }}</td>
        <td>{{ $surveyUser->state }}</td>
        <td>{{ $surveyUser->zipcode }}</td>
        <td><label class="radio-inline">
                <input type="checkbox"  name="optradio-@php echo ++$i;@endphp" class="check"  data-type="{{$surveyUser->id}}" id="checkData{{$surveyUser->id}}" <?= ($surveyUser->financial_contributor=="Yes")?'checked':''; ?> ></label> </td>
        <td><label class="radio-inline">
                <input type="checkbox" name="optradio-@php echo ++$j;@endphp" class="checksupport" data-type="{{$surveyUser->id}}" id="supportData{{$surveyUser->id}}" <?= ($surveyUser->Identification=="Support")?'checked':''; ?>>
            </label></td>
        <td>{{ $surveyUser->Identification }}</td>
        <td>{{ $surveyUser->tag }}</td>
        <td>{{ $surveyUser->updated_at }}</td>
        <td>

            @if(auth()->user()->can('write'))
                <a href="{{route('surveyUser.edit',[$surveyUser->id])}}" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
            @else
                <a href="{{route('surveyUser.show',[$surveyUser->id])}}" title="View"><i class="fa fa-fw fa-eye"></i></a>
            @endif
        </td>
    </tr>
@endforeach