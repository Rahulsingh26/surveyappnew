@extends('adminlte::page')

@section('title', 'Fraud Detection')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header" align="Center">
                    <h3 class="box-title">Fraud Detection</h3>
                    <div style="display:flex;flex-direction:row;justify-content:space-between;">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Canvasser Name</label>
                                        <select name="canvessar_id" title="" style="width: 100%; height: 34px;">
                                            <option value="">Select</option>
                                            @foreach($canvassers as $key => $canvessar)
                                                <option value="{{$canvessar->id}}"  {{  old('user_id.0') && in_array($canvessar->id,old('user_id')) ? 'selected':''}}>{{$canvessar->name}} ( {{$canvessar->email}} )</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div align="left">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                    <a class="btn btn-default" href="{{ route ('fraud.walkbook') }}">Reset</a>
                                </div>
                            </div>
                        </form>
                           <form action="" method="get">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Walkbook Name</label>
                                    <select name="walkbooks_id" title="" style="width: 100%; height: 34px;">
                                        <option value="">Select</option>
                                        @foreach ($walkbookData as $key => $walkbook)
                                               <option value="{{$walkbook->id}}" >{{isset($walkbook->title) ? $walkbook->title : "N/A"}}</option>
                                               @endforeach
                                           </select>
                                       </div>
                                   </div>
                               </div>
                               <div align="left">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                    <a class="btn btn-default" href="{{ route ('fraud.walkbook') }}">Reset</a>
                                </div>
                            </div>
                        </form>


                            <form action="" method="get">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Enter the Fraud distance (miles)</label>
                                        <input type="text" name="fraud" style="padding: 5px;">
                                         <button style="vertical-align: top;" type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                          
                            </div>
                         
                        </form>



                    </div>
                </div>

 

                <div class="box-body table-responsive no-padding">
                    @if(count($data))
                        @php $count= 1; @endphp
                        <table class="table table-hover" id="networkStatus">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>Canvasser Name</th>
                                <th>households Prospect Name</th>
                                <th>TotalHouseHold/Pending</th>
                                <th>Walkbook Name</th>
                                <th>Fraud Detection Status</th>
                                <th>Fraud Average Distance(miles)</th>

                            </tr>
                            @foreach ($data as $key => $walkbook)
                                @foreach($records as $distance)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>
                                        <span class="label label-primary" title="{{$walkbook->users->name}} ({{$walkbook->users->email}})">{{$walkbook->users->name}}</span>
                                    </td>
                                    <td>{{$walkbook->prospect}}</td>

                                   <td>
                                    
                                    {{$getWalkList[$walkbook->walkbook['title']]['totalwalkbook']}} / 
                                    {{$getWalkList[$walkbook->walkbook['title']]['Household_left']}}
                                           
                   
                                </td>
                                    <td>{{isset($walkbook->walkbook->title) ? $walkbook->walkbook->title : "N/A"}}</td>
                                   <!--  <td>@if(isset($distance) && $distance[$key] == 0)
                                           <strong class="text-info">Completed</strong>
                                        @else <b><div class="text-danger">Fraud</div></b> @endif
                                    </td> -->
                                    <td>
                                        @if (round($distance[$key]) <= round(Request::get('fraud')))
                                        <strong class="text-info">{{'Completed'}}</strong>
                                    @else
                                        <div class="text-danger">{{'Fraud' }}</div>
                                    @endif

                                    </td>
                                    
                                    <td> {{$distance[$key]}}</td>

                                </tr>
                                @php $count++; @endphp
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div>No Record Found</div>
                    @endif
                </div>

                <div class="clearfix">
                    {{ $data->links() }}
                </div>


            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataMode').on('change', function() {
                option =  $('#dataMode').val();

                url="{{ route('dataMode.walkbook') }}";
                data={_token:"{{csrf_token()}}",'data_mode':option};

                $.post(url,data,function(rdata){
                    $("#networkStatus").html(rdata);
                });
            });
        });


    </script>

@stop
