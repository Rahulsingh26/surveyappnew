@extends('adminlte::page')

@section('title', 'Polygon Section List')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12" id="polygonlist">
            <div id="polygonlistFlash">
                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}</div>
                @endif
            </div>

            <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Polygon Section List</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                @if(count($PolygonSection))
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Shape Name</th>                           
                            <th>View</th>
                            <th>Delete</th>
                        </tr>
                        @php
                        $i = $PolygonSection->perPage() * ($PolygonSection->currentPage()-1) + 1;
                        @endphp
                            @foreach ($PolygonSection as $key => $Polygon)
                             @if($Polygon)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $Polygon->shape_name }}</td>
                                    <td> <a href="{{route('view-polygon',$Polygon->id)}}" title="View"><i class="fa fa-fw fa-eye"></i></a></td>
                                    <td> <a href="{{route('polygon.delete',$Polygon->id)}}" title="Delete"><i class="fa fa-fw fa-trash"></i></a></td>
                                </tr>
                             @endif
                            @php $i++ @endphp
                            @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endif
            </div>

            <div class="clearfix">
                {{ $PolygonSection->links() }}
            </div>

        </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
