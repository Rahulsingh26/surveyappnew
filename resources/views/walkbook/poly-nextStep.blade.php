@extends('adminlte::page')

@section('title', 'Assign Canvasser')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
       <form class="form-horizontal" action="{{route('polygonfinalStep')}}" method="post">
        @csrf 
        <div class="box-header " align="Center">
            <h3 class="box-title">Assign Canvasser List</h3>
                <div class="row">
                     <div class="col-md-12 canvessar">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Canvassers List</h3>
                            </div>
                            <div class="box-body table-responsive no-padding">
                                <div class="col-md-8" style="margin-bottom: 13px;">
                                    <select class="js-multiple form-control" name="canvessar_id[]" multiple="multiple" data-placeholder="Select Canvassers" required="true">
                                        @foreach($canvassers as $key => $canvessar)
                                        <option value="{{$canvessar->id}}">{{$canvessar->name}} ( {{$canvessar->email}} )</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        
        <div class="col-md-12">
            <div class="box box-info">   
                       
                <div class="box-body table-responsive no-padding">                            
                    <table class="table table-hover" id="networkStatus">
                        <tbody>
                            <tr>
                                <th style="width:95px;"><input type="checkbox" name="select_all" id ="checkAll" style="margin-right:5px;float: left;"/>Select all</th>
                                <th>Walkbook name</th>
                                <th>Number of households</th>
                                <th>Assigned Canvasser</th>
                                <th>Date</th>
                                <th>View</th>
                            </tr>

                            @foreach ($data as $key => $surveyUser)
                            <tr>
                                <td>
                                    <input type="checkbox" name="surveyUserIds[]" class="checkbox" value="{{$surveyUser->id}}"/>
                                </td>
                                <td>{{$surveyUser->shape_name}}</td>
                                <td>{{$surveyUser->household}}</td>
                                <td>
                                    @foreach($surveyUser->AssignPolygon as $AssignPolygon)
                                    <span class="label label-primary" title="">{{$AssignPolygon->CanvessarUser[0]['name']}}</span>
                                    @endforeach
                                </td>
                                <td>{{$surveyUser->created_at}}</td>
                                <td> <a href="{{route('view-polygon',$surveyUser->id)}}" title="View"><i class="fa fa-fw fa-eye"></i></a></td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
           
            </div>
        </div>
        <!--/.col (right) -->
        <div class="bottom_btn_section">
             <button type="button" class="btn btn-primary" onclick='refreshPage()'>back</button>   
             <button type="submit" class="btn btn-primary save_item" id="frmsave">Save</button>   
        </div>
    </form>
    </div>
@stop

@section('custom_js')
    <script>
        function refreshPage(){
            if(confirm("Are you sure, want to back?")){
                var url = "{{ env('SUB_DOMAIN_URL')}}";
                window.location.href = "http://surveyappnew.n1.iworklab.com/polygons/polygon";
            }               
        }
        $(document).ready(function(){
          

            $('.js-multiple').select2();
            $('.canvessar').hide();

            $("#checkAll").click(function(){
                $('.canvessar').show();
                $('.select2-container').width('627px');
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
            $("input[type='checkbox']").click(function(){
                $('.canvessar').show();
             });

            $("#frmsave").click(function(){
                var checked = $("input:checked").length > 0;
                console.log(checked);
                if (!checked){
                    alert("Please check at least one checkbox");
                    return false;
                }
            });

        });
    </script>
     
@stop
