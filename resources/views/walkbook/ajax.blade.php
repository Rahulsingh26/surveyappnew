<div class="box-body table-responsive no-padding">
    @if(count($data))

        @php $count= 1; @endphp

        <table class="table table-hover">
            <tbody>
            <tr>
                <th>S.no</th>
                <th>Walkbook Title</th>
                <th>Canverser Name</th>
                <th>Number of households traverse</th>
                <th>Household View</th>

            </tr>

            @foreach ($data as $key => $walkbook)
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$walkbook['walkbook']['title']}}</td>
                    <td>
                        @php
                            $user = \App\Helper\AppHelper::getUserDetails($walkbook['canvasser_id']);
                        @endphp
                        @if(isset($user))
                            <span class="label label-primary" title="{{$user->name}} ({{$user->email}})">{{$user->name}}</span>
                        @endif
                    </td>
                    <td>{{$walkbook['completed']}}</td>
                    <td>
                        <form action="{{ url('walkbook/house-hold-view') }}" method="Post" >
                            @csrf
                            <input type="hidden" value="{{$walkbook['survey_user_ids']}}" name="suvalue">
                            <input type="hidden" value="{{ $walkbook['walkbook_id']}}" name="id">
                            <button type="submit" class="houseHoldView btn btn-xs" title="View this">View </button>
                            {{--<button type="button" class="houseHoldView btn btn-xs" title="View this" data-type = "{{ $walkbook[0]['walkbook_id']}}">View </button>--}}
                        </form>

                    </td>

                </tr>
                @php $count++; @endphp

            @endforeach
            </tbody>
        </table>
    @else
        <div>No Record Found</div>
    @endif
</div>


