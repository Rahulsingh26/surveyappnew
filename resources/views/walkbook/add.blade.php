@extends('adminlte::page')

@section('title', 'Add Walkbook')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Walkbook</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{route('store.walkbook')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name"  class="col-sm-3 control-label">Title</label>

                            <div class="col-sm-9 @error('title') has-error @enderror">
                                <input type="text" name= "title" class="form-control" id="title" placeholder="Title" value="{{old('title')}}">
                                @error('title')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name"  class="col-sm-3 control-label">Canvasser</label>
                        <div class="col-sm-9 @error('user_id') has-error @enderror">
                            <select class="js-multiple form-control" name="user_id[]" multiple="multiple" data-placeholder="Select Canvassers">
                            @foreach($canvassers as $key => $canvessar)
                                    <option value="{{$canvessar->id}}"  {{  old('user_id.0') && in_array($canvessar->id,old('user_id')) ? 'selected':''}}>{{$canvessar->name}} ( {{$canvessar->email}} )</option>
                            @endforeach
                        </select>
                            @error('user_id')
                            <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" value = 1 {{old('status')=="1" ? 'checked='.'checked' : '' }} checked="">
                                        Active
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" value = 0 {{old('status')=="0" ? 'checked='.'checked' : '' }} >
                                        Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('create.walkbook') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.js-multiple').select2();
        });

    </script>
@stop
