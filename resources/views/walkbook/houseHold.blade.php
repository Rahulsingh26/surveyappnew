@extends('adminlte::page')

@section('title', 'HouseHold Details')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">HouseHold Details</h3>
                </div>

                <div class="box-body table-responsive no-padding">
                    @if(count($record))
                        @php $count= 1; @endphp
                        <table class="table table-hover" id="trackingData">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>HouseHold Name</th>
                                <th>Voter</th>
                                <th>Address</th>
                            </tr>

                            @foreach ($record as $key => $surveyUser)
                                @if(isset($surveyUser))
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$surveyUser->surveyUser->name}}</td>
                                    <td>{{$surveyUser->surveyUser->voter_id}}</td>
                                    <td>{{$surveyUser->surveyUser->precinct . ''. $surveyUser->surveyUser->street . ''. $surveyUser->surveyUser->city . ''. $surveyUser->surveyUser->state . ''. $surveyUser->surveyUser->zipcode . ''. $surveyUser->surveyUser->country}}</td>
                                </tr>
                                @php $count++; @endphp
                                @endif

                            @endforeach

                            </tbody>
                        </table>
                    @else
                        <div>No Record Found</div>
                    @endif
                </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
