@extends('adminlte::page')

@section('title', 'Walkbook List')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12" id="walkbooklist">
            <div id="walkbooklistFlash">
                @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('message') }}</div>
                @endif
            </div>

            <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Walkbook List</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                @if(count($walkbooks))
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>No of Individuals</th>
                            <th>No of Household</th>
                            <th>Assigned Canvasser</th>
                            @if(auth()->user()->can('write'))
                                <th>Edit</th>
                            @else
                                <th>View</th>
                            @endif
                            <th>Survey</th>
                            <th>Download</th>
                            @if(auth()->user()->can('write'))
                                <th>Delete</th>
                            @endif
                            <th>Created At</th>
                            <th>Created By</th>
                            <th>Updated At</th>
                        </tr>
                            @foreach ($walkbooks as $key => $walkbook)
                                <tr>
                                    <td>{{ (($walkbooks->currentPage() - 1 ) * $walkbooks->perPage() ) + $loop->iteration}}</td>
                                    <td>{{ $walkbook->title }}</td>
                                    <td>
                                        <span class="label {{ $walkbook->status ==1 ? 'label-success' : 'label-danger'}}">
                                        @if($walkbook->status ==1)
                                            Active
                                        @else
                                            Inactive
                                        @endif
                                        </span>
                                    </td>
                                    <td>{{count($walkbook->surveyUser)}}</td>
                                    <td>
                                        @php
                                            $totalHouseHold = \App\Helper\AppHelper::getHouseHold($walkbook->surveyUser);
                                        @endphp
                                        {{count($totalHouseHold)}}
                                    </td>
                                    <td>
                                        @if(isset($walkbook->user_id) && $walkbook->user_id!='null')
                                        @foreach($walkbook->user_id as  $user_id)
                                            @php
                                                $user = \App\Helper\AppHelper::getUserDetails($user_id);
                                            @endphp
                                            <span class="label label-primary" title="{{$user->name}} ({{$user->email}})">{{$user->name}}</span>

                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if(auth()->user()->can('write'))
                                            @php
                                                $icon = 'edit';
                                                $title = 'edit';
                                            @endphp
                                        @else
                                            @php
                                                $icon = 'eye';
                                                $title = 'view';
                                            @endphp
                                        @endif
                                        <a href="{{route('edit.walkbook',[$walkbook->id])}}" title="{{ucfirst($title)}}"><i class="fa fa-fw fa-{{$icon}}"></i></a>
                                    </td>
                                    <td>
                                        <select name="survey" class="form-control" onchange="changeSurvey(this.value, {{$walkbook->id}})">
                                            <option>Select Survey</option>
                                            @foreach($surveys as  $survey)
                                                <option value="{{$survey->id}}" {{ ($walkbook->assignWalkbookDetails && $walkbook->assignWalkbookDetails->survey_id == $survey->id) ? "selected": "" }}>{{$survey->title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <a href="#" onclick="downloadWalkbookReport({{$walkbook->id}})"><i class="fa fa-fw fa-download "></i></a>
                                    </td>
                                    @if(auth()->user()->can('write'))
                                    <td>
                                        <a href="javascript:void(0)" title="Delete" data-toggle="modal" data-target="#modal-default-{{$walkbook->id}}"><i class="fa fa-fw fa-trash"></i></a>
                                        <form method="get" action ="{{route('delete.walkbook',[$walkbook->id])}}">
                                            @csrf
                                            <div class="modal fade" id="modal-default-{{$walkbook->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Are you sure you want to delete this Walkbook ?</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p> Walkbook Name: {{ $walkbook->title }}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary" id="delete_question">Delete</button>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </form>
                                    </td>
                                    @endif
                                    <td>{{ $walkbook->created_at }}</td>
                                    <td>{{ (App\Helper\AppHelper::getUserCreateEditName($walkbook->userCreatedWorkbookID))?(App\Helper\AppHelper::getUserCreateEditName($walkbook->userCreatedWorkbookID)):'Deleted user' }}</td>
                                    <td>{{ $walkbook->updated_at }}</td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endif
            </div>

            <div class="clearfix">
                {{ $walkbooks->links() }}
            </div>

        </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script type="application/javascript">
        /**
         * change survey
         * @param survey
         * @param walkBook
         */
        function changeSurvey(survey, walkBook){
            $.ajax({
                type:'POST',
                url: "{{ route('change.survey') }}",
                data:{'_token':'{{ csrf_token() }}', 'survey_id': survey, 'walkbook': walkBook},
                success: function(result){
                    if(result.status == 202){
                        $("#walkbooklistFlash").html('<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Survey added to Walkbook Successfully</div>')
                    }else{
                        $("#walkbooklistFlash").html('<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Something bad happened.</div>')
                    }

                }
            });
        }

        //download walkbook reports
        function downloadWalkbookReport(walkbookId) {
            var url = "{{URL::to('/manage-reports/download-walkbook')}}/" + walkbookId
            window.location.href = url;
        }

    </script>
@stop
