@extends('adminlte::page')

@section('title', 'Polygon Section')

@section('content_header')
    <h1>Polygon Section</h1>
@stop

@section('content')
    @parent

    <!-- Modal content-->
    <div class="modal-content ">

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <div id="map" style="width:100%; height:600px"></div>
                </div>

            </div>
        </div>
    </div>
    <br>
    <div class="bottom_btn_section" align="center">
        <button type="button" style=" margin: 22px;" class="btn btn-primary" onclick='refreshPage()'>Back</button>
    </div>

    @foreach($data as $value)
        <input type="hidden" class="lat" value="{{$value['lat']}}">
        <input type="hidden" class="lng" value="{{$value['lng']}}">
    @endforeach



@stop

@section("js")
    @parent


    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9KiNx3g9qZiDzgetuEHlFRpZGIcYHVio&libraries&callback=initMap">
    </script>
    <script>

        // This example creates a simple polygon representing the Bermuda Triangle.
        // When the user clicks on the polygon an info window opens, showing
        // information about the polygon's coordinates.

        function refreshPage() {
            if (confirm("Are you sure, want to back?")) {
                var url = "{{ env('SUB_DOMAIN_URL')}}";
                window.location.href = "http://surveyappnew.n1.iworklab.com/polygons/polygon";
            }

        }

        var map;
        var infoWindow;

        function initMap() {
            var latitude= [];
            var longitude = [];
            $(".lat").each(function () {
                latitude.push($(this).val());
            });
            $(".lng").each(function () {
                longitude.push($(this).val());
            });

            var locations =[];
            for (var i = 0; i < latitude.length; i++) {
                var trianglesubCoords = [];
                trianglesubCoords.push(latitude[i]);
                trianglesubCoords.push(longitude[i]);
                locations.push(trianglesubCoords);
            }
           console.log(locations[0]);
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: new google.maps.LatLng(locations[0][0], locations[0][1]),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            console.log(locations[0][1]);
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][0], locations[i][1]),
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }

        }
    </script>
@stop
