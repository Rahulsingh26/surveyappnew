@extends('adminlte::page')

@section('title', 'Density Tracking for walkbook')

@section('custom_css')
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">

        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Density Tracking for walkbooks</h3>
                    <div align="right">
                        <strong><h7>Tracking per square mile</h7></strong>
                        <select id="miles" title="">
                            <option value=""></option>
                            <option value="1">1 miles</option>
                            <option value="10">10 miles</option>
                            <option value="30">30 miles</option>
                        </select>
                    </div>

                </div>

                <div class="box-body table-responsive no-padding">
                    @if(count($data))
                        @php $count= 1; @endphp
                        <table class="table table-hover" id="trackingData">
                            <tbody>
                            <tr>
                                <th>S.no</th>
                                <th>Walkbook Title</th>
                                <th>Canverser Name</th>
                                <th>Number of households traverse</th>
                                <th>Household View</th>

                            </tr>

                            @foreach ($data as $key => $walkbook)
                                <tr>
                                        <td>{{$count}}</td>
                                        <td>{{$walkbook['walkbook']['title']}}</td>
                                        <td>
                                            @php
                                              $user = \App\Helper\AppHelper::getUserDetails($walkbook['canvasser_id']);
                                            @endphp
                                            @if(isset($user))
                                                <span class="label label-primary" title="{{$user->name}} ({{$user->email}})">{{$user->name}}</span>
                                            @endif
                                        </td>
                                        <td>{{$walkbook['completed']}}</td>

                                        <td>
                                            <form action="{{ url('walkbook/house-hold-view') }}" method="Post" >
                                                @csrf
                                                <input type="hidden" value="{{$walkbook['survey_user_ids']}}" name="suvalue">
                                                <input type="hidden" value="{{ $walkbook['walkbook_id']}}" name="id">
                                                <button type="submit" class="houseHoldView btn btn-xs" title="View this">View </button>
                                                {{--<button type="button" class="houseHoldView btn btn-xs" title="View this" data-type = "{{ $walkbook[0]['walkbook_id']}}">View </button>--}}
                                            </form>

                                        </td>
                                </tr>
                                @php $count++; @endphp

                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div>No Record Found</div>
                    @endif
                </div>



            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('public/js/custom.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#miles').on('change', function() {
                option =  $('#miles').val();

                url="{{ route('miles.walkbook') }}";
                data={_token:"{{csrf_token()}}",'id':option};

             $.post(url,data,function(rdata){
                     $("#trackingData").html(rdata);

                });
            });
        });


    </script>

@stop
