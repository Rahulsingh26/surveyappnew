@extends('adminlte::page')

@section('title', 'Video List')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Video List</h3>
                </div>
            <div class="box-body table-responsive no-padding">
                @if(count($videos))
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Title</th>
                            <th>Video Link</th>
                            <th>Status</th>
                            @can('write')
                            <th>Edit</th>
                            <th>Delete</th>
                            @endcan
                            <th>Created At</th>
                            <th>Created By</th>
                            <th>Updated At</th>
                        </tr>
                            @foreach ($videos as $key => $video)
                                <tr>
                                    <td>{{ (($videos->currentPage() - 1 ) * $videos->perPage() ) + $loop->iteration}}</td>
                                    <td>{{ $video->title }}</td>
                                    <td><a href = "{{ url('/uploads/videos/'.$video->file_name) }}" target ="_blank">Click here to see video</a></td>
                                    <td>
                                        <span class="label {{ $video->status ==1 ? 'label-success' : 'label-danger'}}">
                                        @if($video->status ==1)
                                            Active
                                        @else
                                            Inactive
                                        @endif
                                        </span>
                                    </td>
                                    @can('write')
                                        <td>
                                            <a href="{{route('edit.video',[$video->id])}}" title="Edit"><i class="fa fa-fw fa-edit"></i></a>
                                        </td>
                                    <td>
                                        <a href="javascript:void(0)" title="Delete" data-toggle="modal" data-target="#modal-default-{{$video->id}}"><i class="fa fa-fw fa-trash"></i></a>
                                        <form method="get" action ="{{route('delete.video',[$video->id])}}">
                                            @csrf
                                            <div class="modal fade" id="modal-default-{{$video->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Are you sure you want to delete this video ?</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p> Video Title: {{ $video->title }}</p>
                                                            <p> Video Link: <a href="{{ $video->file_name ? url('/uploads/videos/'.$video->file_name): '' }}" target="_blank">{{ url('/uploads/videos/'.$video->file_name) }}</a></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary" id="delete_question">Delete</button>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </form>

                                    </td>
                                    @endcan
                                    <td>{{ $video->created_at }}</td>
                                    <td>{{ (App\Helper\AppHelper::getUserCreateEditName($video->userCreatedVideoID))?(App\Helper\AppHelper::getUserCreateEditName($video->userCreatedVideoID)):'Deleted user' }}</td>
                                    <td>{{ $video->updated_at }}</td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endif
            </div>

            <div class="clearfix">
                {{ $videos->links() }}
            </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop
