@extends('adminlte::page')

@section('title', 'Add User')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <style>
     .help-block{
        font-weight:bold;
        color:red;
     } 
     .form-horizontal .form-group{margin-left:0px;margin-right: 0px;}
  </style>
@stop

@section('content')

    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Attendee</h3>
                </div>

                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{ url('attende/attende-add') }}" method="post" style="padding:2px;marging:5px;">
                    @csrf
                    <div class="box-body">
                        @if($eventObjs)
                        <div class="form-group">
                            <label for="name"  class="control-label">Events</label>
                        <div>
                            <select class="js-multiple form-control" name="eventID[]" multiple="multiple" data-placeholder="Select Event">
                            @foreach($eventObjs as $eventObj)
                                    <option value="{{ $eventObj->id}}">{{ $eventObj->eventName}}</option>
                            @endforeach
                        </select>
                        </div>
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="name" >Name</label>

                            <div>
                                <input type="text" name="attendeName" class="form-control" id="name" placeholder="Name" value="{{old('attendeName')}}">
                                @if($errors->first('attendeName'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('attendeName') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" >Email</label>

                            <div>
                                <input type="text" name="attendeEmailAddress" class="form-control" id="name" placeholder="Email address" value="{{old('attendeEmailAddress')}}">
                                @if($errors->first('attendeEmailAddress'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('attendeEmailAddress') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" >Attendee Address</label>

                            <div >
                                <textarea type="text" name="attendeAddress" class="form-control" id="eventAddress" placeholder="Address">{{old('attendeAddress')}}</textarea>
                                @if($errors->first('attendeAddress'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('attendeAddress') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" >Phone number</label>

                            <div>
                                <input type="text" name="attendePhoneNo" class="form-control" id="name" placeholder="Phone number" value="{{old('attendePhoneNo')}}">
                                @if($errors->first('attendePhoneNo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('attendePhoneNo') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                      
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('create.attende') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <div class="col-md-2"></div>
        <!--/.col (right) -->
    </div>

@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.js-multiple').select2();
        });

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script>
    $('#datepicker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd',
        startDate: new Date()
    })
    $('#datepicker1').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd',
        startDate: new Date()
    })
    $('#datepicker').datepicker()
    .on('changeDate', function(e) {
        $('#datepicker1').datepicker('setStartDate', e.target.value);
    });
    $('#datepicker1').datepicker()
    .on('changeDate', function(e) {
        $('#datepicker').datepicker('setEndDate', e.target.value);
    });
    </script>
@stop
