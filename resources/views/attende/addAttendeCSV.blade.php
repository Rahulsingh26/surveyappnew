@extends('adminlte::page')

@section('title', 'Add User')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
   <style>
     .help-block{
        font-weight:bold;
        color:red;
     }
     .form-horizontal .form-group{margin-left:0px;margin-right: 0px;}
     .csv{position:absolute;right:10px;top:8px;}
  </style>
@stop

@section('content')

    <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <!-- Horizontal Form -->
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-success', 'alert-danger') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title" >Add Attendee CSV</h3><a class="csv" href="{{asset('attendecsv/AttendeeRecord.csv')}}">Download reference</a>

                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{ url('attende/attende-csv-add') }}" method="post" enctype="multipart/form-data" style="padding:2px;marging:5px;">
                    @csrf
                    <div class="box-body">


                        <div class="form-group">
                            <label for="name" >File</label>
                            <div>
                                <input type="file" name="file" class="form-control" id="name" placeholder="file" >
                                @if($errors->first('file'))
                                    {{ $errors->first('file') }}
                                @endif
                            </div>
                        </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a class="btn btn-default" href="{{ route ('csv-form.attende') }}">Reset</a>
                        <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>
        <div class="col-md-2"></div>
        <!--/.col (right) -->
    </div>

@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.js-multiple').select2();
        });

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script>
    $('#datepicker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd',
        startDate: new Date()
    })
    $('#datepicker1').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd',
        startDate: new Date()
    })
    $('#datepicker').datepicker()
    .on('changeDate', function(e) {
        $('#datepicker1').datepicker('setStartDate', e.target.value);
    });
    $('#datepicker1').datepicker()
    .on('changeDate', function(e) {
        $('#datepicker').datepicker('setEndDate', e.target.value);
    });
    </script>
@stop
