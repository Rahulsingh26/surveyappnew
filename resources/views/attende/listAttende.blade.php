@extends('adminlte::page')

@section('title', 'List Events')

@section('custom_css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <style>
     .help-block{
        font-weight:bold;
        color:red;
     }
     .form-horizontal .form-group{margin-left:0px;margin-right: 0px;}
     .form-horizontal .box-body{display:flex;flex-wrap:wrap;}
     .form-horizontal .box-body .form-group{width: 50%;
    float: left;
    padding: 0 15px;}
    .box-footer a.btn-default{margin-right:10px;}
    .form-horizontal .box-body .form-group {
    width: 25%;
    }
  </style>
@stop

@section('content')


     <div class="row">
        <!-- left column -->
        <!--/.col (left) -->
        <!-- right column -->

        <div class="col-md-12">
            <!-- Horizontal Form -->

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Attendee</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <form class="form-horizontal" action="" method="get" style="padding:2px;marging:5px;">

                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" >Name</label>

                            <div>
                            <input type="text" name="attendeName" class="form-control" id="name" placeholder="Name" value="{{(isset($_GET['attendeName']))?$_GET['attendeName']:old('attendeName') }}">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" >Email</label>

                            <div >
                            <input type="text" name="attendeEmailAddress" class="form-control" id="name" placeholder="Name" value="{{ (isset($_GET['attendeEmailAddress']))?$_GET['attendeEmailAddress']:old('attendeEmailAddress')}}">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" >Address</label>

                            <div >
                                <textarea type="text" name="attendeAddress" class="form-control" id="eventAddress" placeholder="Event Address">{{ (isset($_GET['attendeAddress']))?($_GET['attendeAddress']):old('attendeAddress')}}</textarea>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" >Phone</label>

                            <div >
                                <input type="text" name="attendePhoneNo" class="form-control" id="name" placeholder="Phone" value="{{(isset($_GET['attendePhoneNo']))?$_GET['attendePhoneNo']:old('attendePhoneNo')}}">

                            </div>
                        </div>


                    <!-- /.box-body -->
                        <div class="box-footer">
                            <a class="btn btn-default" href="{{ route ('create.event') }}">Reset</a>
                            <button type="submit" class="btn btn-info pull-right">Submit</button>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /.box -->
        </div>

        <!--/.col (right) -->
    </div>

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert {{ Session::get('alert-success', 'alert-danger') }} alert-dismissible" >
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('message') }}</div>
            @endif
                <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Attendee List</h3>
                </div>
            <div class="box-body table-responsive no-padding">

                @isset($attendes)

                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone no</th>
                            <th>Email</th>
                            <th>Status</th>
                            @can('write')
                                <th>Action</th>
                            @endcan
                            <th>Created At</th>
                            <th>Created By</th>
                            <th>Updated At</th>
                        </tr>
                        @php
                        $i = $attendes->perPage() * ($attendes->currentPage()-1) + 1;
                        @endphp
                        @foreach($attendes as $attende)

                            @if($attende)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ ($attende->attendeName)?$attende->attendeName:'No name found' }}</td>
                                    <td>{{ ($attende->attendeAddress)?$attende->attendeAddress:'No name found' }}</td>
                                    <td>{{ ($attende->attendePhoneNo)?$attende->attendePhoneNo:'No name found' }}</td>
                                    <td>{{ ($attende->attendeEmailAddress)?$attende->attendeEmailAddress:'No name found' }}</td>
                                    @if ($attende->attendeStatus == 1)
                                    <td><a href="{{ url('attende/status-update/'.$attende->id.'/'.$attende->attendeStatus)}}" class="btn btn-danger" onclick="if(!confirm('Do You Want To Update status?')){return false;}">Inactive</a></td>
                                    @else
                                    <td><a href="{{ url('attende/status-update/'.$attende->id.'/'.$attende->attendeStatus)}}" class="btn btn-success" onclick="if(!confirm('Do You Want To Update status?')){return false;}">Active</a></td>
                                    @endif
                                    @can('write')
                                        <td><a class="btn btn-danger" href="{{ url('attende/attende-delete/'.$attende->id) }}" onclick="if(!confirm('Do You Want To Delete?')){return false;}">Delete</a> &nbsp;<a class="btn btn-success" href="{{ url('attende/attende-edit/'.$attende->id) }}">Edit</a></td>
                                    @endcan
                                    <td>{{ $attende->created_at }}</td>
                                    <td>{{ (App\Helper\AppHelper::getUserCreateEditNameForEventAttendee($attende->attendeCreatedByUserID))?(App\Helper\AppHelper::getUserCreateEditNameForEventAttendee($attende->attendeCreatedByUserID)):'Deleted user'}}</td>
                                    <td>{{ $attende->updated_at }}</td>
                                </tr>

                            @endif
                            @php $i++ @endphp
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div>No Record Found</div>
                @endisset
            </div>

            <div class="clearfix">
                {{ $attendes->links() }}
            </div>
                </div>
        </div>
        <!--/.col (right) -->
    </div>
@stop

@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
     <script>
    $('#datepicker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd'
    })
    $('#datepicker1').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd'
    })

    </script>
@stop
