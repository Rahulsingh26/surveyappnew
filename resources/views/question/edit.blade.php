@extends('adminlte::page')
@section('title', 'Edit Question')
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Question</h3>
                </div>
                <div class="box-body">
                    <form method="POST" action="{{route('update.question',[$question->id])}}">
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="row">
                                <label for="title" class="col-sm-3">Question Type</label>

                                <div class="col-sm-9 @error('question_type') has-error @enderror">
                                    <select class="form-control" name="question_type" id="question_type">
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value=1 {{ old('question_type') == 1 ? 'selected' : $question->question_type ==1 ? 'selected' : '' }}>Text</option>
                                        <option value=2 {{ old('question_type') == 2 ? 'selected' : $question->question_type ==2 ? 'selected' : '' }}>Textarea</option>
                                        <option value=3 {{ old('question_type') == 3 ? 'selected' : $question->question_type ==3 ? 'selected' : '' }}>Radio Buttons</option>
                                        <option value=4 {{ old('question_type') == 4 ? 'selected' : $question->question_type ==4 ? 'selected' : '' }}>Checkbox</option>
                                    </select>
                                    @error('question_type')
                                    <span class="help-block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                        <label for="title"  class="col-sm-3 control-label">Question</label>

                        <div class="col-sm-9 @error('title') has-error @enderror">
                            <input type="text" name= "title" class="form-control" id="title" placeholder="Title" value="{{ $question->title }}">
                            @error('title')
                            <span class="help-block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                            </div>
                    </div>

                        <div class="form-g">
                            @if($question->option_name)
                                @foreach($question->option_name as $key => $single_option)
                                    @if($single_option !=null)
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="option_name" class="col-sm-3">Option Name</label>
                                            <div class="col-sm-3 @error('option_name.0') has-error @enderror">
                                                <input name="option_name[]"  type="text" class="option_name form-control" value="{{$single_option}}">
                                                @error('option_name.0')
                                                <span class="help-block">
                                                        <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                                <span class="add-option" style="cursor:pointer;">Add Another</span>
                                            @if($key > 0)
                                                <span style="cursor:pointer;margin-left: 20px"class="delete-option">Delete</span>
                                            @endif
                                            </div>
                                    </div>
                                    @endif
                                @endforeach
                            @endif

                                @if((old('question_type') ==4  || (old('question_type') ==3 )) && count($question->option_name) == 0)
                                    @error('option_name.0')
                                    <div class="form-group options"><div class="row">
                                            <label for="option_name" class="col-sm-3">Option Name</label>
                                            <div class="col-sm-3 option_input @error('option_name.0') has-error @enderror">
                                                <input name="option_name[]" type="text" class="form-control option_name">
                                                <span class="help-block">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                            </div>
                                            <span class="add-option" style="cursor:pointer;margin-right:20px;">Add Another</span>
                                        </div>

                                    </div>
                                    @enderror
                                @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-3">
                                    <button class="btn waves-effect waves-light">Update</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom_js')
    <script src="{{ asset('js/custom.js') }}"></script>
@stop