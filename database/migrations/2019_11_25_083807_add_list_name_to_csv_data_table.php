<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddListNameToCsvDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('csv_data', function (Blueprint $table) {
            $table->string('csv_original_filename')->nullable();
            $table->string('csv_saved_filename')->nullable();
            $table->bigInteger('house_holds_number')->default(0);
            $table->bigInteger('individuals_number')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('csv_data', function (Blueprint $table) {
            $table->removeColumns(['csv_original_filename', 'csv_saved_filename', 'house_holds_number', 'individuals_number']);
        });
    }
}
