<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanvasserIdToSurveyUserWalkbookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_user_walkbook', function (Blueprint $table) {
            $table->integer('canvasser_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_user_walkbook', function (Blueprint $table) {
            $table->removeColumn('canvasser_id');
        });
    }
}
