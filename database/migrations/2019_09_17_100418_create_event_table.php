<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('eventName');
            $table->date('eventStartDate');
            $table->date('eventEndDate');
            $table->text('eventAddress');
            $table->tinyInteger('eventStatus')->default(1)->comment('1:- active,0:- inactive');
            $table->bigInteger('attendeCreatedByUserID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
