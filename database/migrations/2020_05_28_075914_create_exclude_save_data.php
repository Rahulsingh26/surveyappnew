<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExcludeSaveData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exclude_save_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable()->unique();
            $table->longText('surveyUser_id')->nullable();
            $table->string('surveyUser_id_secure',757)->nullable();
            $table->integer('household')->nullable();
            $table->integer('individual')->nullable();
            $table->integer('Csv_id')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE exclude_save_data ADD CONSTRAINT exclude_save_data UNIQUE(surveyUser_id_secure,Csv_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exclude_save_data');
    }

    /*ALTER TABLE exclude_save_data
  ADD CONSTRAINT exclude_save_data UNIQUE(surveyUser_id,Csv_id);*/
}
