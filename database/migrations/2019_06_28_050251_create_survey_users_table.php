<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('voter_id')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('identification')->nullable();
            $table->string('street')->nullable();
            $table->string('precinct')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('vote_history_1')->nullable();
            $table->string('vote_history_2')->nullable();
            $table->string('vote_history_3')->nullable();
            $table->string('vote_history_4')->nullable();
            $table->string('contact_history_1')->nullable();
            $table->string('contact_history_2')->nullable();
            $table->string('contact_history_3')->nullable();
            $table->string('early_voting_location')->nullable();
            $table->string('election_day_location')->nullable();
            $table->string('financial_contributor')->nullable();
            $table->string('voted')->nullable();
            $table->string('extra_1')->nullable();
            $table->string('extra_2')->nullable();
            $table->integer('house_hold_id')->nullable();
            $table->integer('action_by')->nullable();
            $table->bigInteger('userCreatedSurveyUserID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_users');
    }
}
