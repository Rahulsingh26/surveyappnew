<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Preferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('canv_id');
            $table->enum('additional_HH_members',['1','0'])->default('0');
            $table->enum('contact_history',['1','0'])->default('0');
            $table->enum('vote_history',['1','0'])->default('0');
            $table->enum('make_me_discoverable',['1','0'])->default('0');
            $table->enum('phone_number',['1','0'])->default('0');
            $table->enum('early_voting_location',['1','0'])->default('0');
            $table->enum('election_day_location',['1','0'])->default('0');
            $table->enum('voted',['1','0'])->default('0');
            $table->enum('previous_contact_history',['1','0'])->default('0');
            $table->enum('financial_contributor',['1','0'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferences');
    }
}
