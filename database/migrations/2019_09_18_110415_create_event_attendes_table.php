<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAttendesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_attendes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('eventID')->index();
            $table->foreign('eventID')->references('id')->on('event')->onDelete('cascade');
            $table->unsignedBigInteger('attendeID')->index();
            $table->foreign('attendeID')->references('id')->on('attendes')->onDelete('cascade');
            $table->string('attendeToken')->nullable();
            $table->tinyInteger('attendeEventAcceptRejectStatus')->default(2)->comment('1 :- accept,0 :- reject,2:- not send');
            $table->tinyInteger('attendeEventCheckedInStatus')->default(0)->comment('1 :- checkedIn,0 :- notCheckedIn');
            $table->string('attendeEventSupportStatus')->default(2)->comment('1 :- yardSignInRequest,0 :- suport,2 :- not accept anything');
            $table->dateTime('attendeEventAccepetedTiming')->nullable();
            $table->tinyInteger('attendeEventEntryPoint')->default(1)->comment('1 : admin,0: api');
            $table->bigInteger('eventAttendeCreatedByUserID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_attendes');
    }
}
