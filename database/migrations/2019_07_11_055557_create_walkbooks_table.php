<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalkbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('walkbooks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('status')->default(1)->comment('0-inactive,1-active');
            $table->string('user_id')->nullable();
            $table->bigInteger('userCreatedWorkbookID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('walkbooks');
    }
}
