<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SurveyPolygonSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_polygon_section', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable()->unique();
            $table->longText('surveyUser_id')->nullable();
            $table->string('surveyUser_id_secure',757)->nullable();
            $table->integer('household')->nullable();
            $table->integer('Csv_id')->nullable();
            $table->timestamps();
        });

        DB::statement('ALTER TABLE survey_polygon_section ADD CONSTRAINT survey_polygon_section UNIQUE(surveyUser_id_secure,Csv_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_polygon_section');
    }
}
