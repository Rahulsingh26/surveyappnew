<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('attendeName');
            $table->text('attendeAddress');
            $table->string('attendePhoneNo');
            $table->string('attendeEmailAddress');
            //$table->string('attendeToken')->nullable();
            //$table->tinyInteger('attendeEventAcceptRejectStatus')->default(2)->comment('1 :- accept,0 :- reject,2:- not send');
            $table->tinyInteger('attendeStatus')->default(1)->comment('1 :- active,0 :- inactive');
            $table->tinyInteger('attendeAssiginedEventStatus')->default(0)->comment('1:-assigned,0:- not assigned');
            $table->bigInteger('attendeCreatedByUserID')->nullable();
            //$table->tinyInteger('attendeEventCheckedInStatus')->default(0)->comment('1 :- checkedIn,0 :- notCheckedIn');
            //$table->tinyInteger('attendeEventSupportStatus')->default(2)->comment('1 :- yardSignInRequest,0 :- suport,2 :- not accept anything');
            //$table->dateTime('attendeEventAccepetedTiming')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendes');
    }
}
