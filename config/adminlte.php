<?php

return [
//die("hii");
    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Survey App',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>Survey</b> App',

    'logo_mini' => '<b>S</b> A',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'dashboard',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => null,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        [
            'text' => 'Dashboard',
            'route'  => 'dasboard',
            'icon' => 'dashboard',
            'submenu' => [
                [
                    'text' => 'Reporting',
                    'icon' => ' fa-area-chart',
                    'route'=>'report-user',
                ],
                [
                    'text' => 'Density Tracking',
                    'icon' => 'book',
                    'route' => 'track.walkbook',
                ],
                [
                    'text' =>'Create Companie',
                    'icon' => 'book',
                    'active' => ['companies/*'],
                    'role' => 'super-admin',
                    'submenu' => [
                        [
                            'text' => 'Add',
                            'route'  => 'companie',
                            'icon' => 'plus',
                            'permission' => 'write'
                        ],
                        [
                            'text' => 'List',
                            'route'  => 'list-companie',
                            'icon' => 'list',
                        ],

                    ],
                ],
               /* [
                    'text' => 'Create Companie',
                    'icon' => 'book',
                    'route'=>'companie',
                    'role' => 'super-admin'
                ],*/
            ],
        ],
//        [
//            'text' => 'Report management',
//            'icon' => ' fa-area-chart',
//            'route'=>'report-user'
//        ],
        /*[
            'text' => 'Account Settings',
            'icon' => 'gear',
            'active' => ['account-settings/*'],
            'submenu' => [
                [
                    'text' => 'Change Email',
                    'route'  => 'change-email',
                    'icon' => 'envelope',
                ],
                [
                    'text' => 'Change Password',
                    'route'  => 'change-password',
                    'icon' => 'lock',

                ],
            ],
        ],*/
       /* [
            'text' => 'Manage Users',
            'icon' => 'user',
            'active' => ['manage-users/*'],
            'submenu' => [
                [
                    'text' => 'Add User',
                    'route'  => 'add-user',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'User List',
                    'route'  => 'list-user',
                    'icon' => 'list',
                ],
            ],
        ],*/
        [
            'text' => 'Data Upload',
            'icon' => 'user',
            'active' => ['manage-households/*'],
            'submenu' => [
                 [
                    'text' => 'Upload',
                    'route'  => 'surveyUser.import',
                    'icon' => 'upload',
                    'permission' => 'upload/download'
                ],
                [
                    'text' => 'Add Individual Profile ',
                    'route'  => 'surveyUser.add',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'Raw Lists',
                    'route'  => 'csv.list',
                    'icon' => 'list',
                ],
                [
                    'text' => 'Added Profile Manually',
                    'route'  => 'surveyUser.list',
                    'icon' => 'list',
                ],

            ],
        ],
        [
            'text' => 'Walkbook Builder',
            'icon' => 'book',
            'active' => ['walkbook/*'],
            'submenu' => [
                [
                    'text' => 'Select Uploaded List',
                    'route'  => 'csv.listnew',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'Create Walk Book',
                    // 'route'  => 'surveyUser.list',
                    'route'  => 'createcsv.list',
                    'icon' => 'plus',
                   
                    'submenu' => [
                        [
                            'text' => 'Create Walk Book List',
                            'route'  => 'createcsv.list',
                            'icon' => 'plus',
                            'permission' => 'write'
                        ],
                        [
                            'text' => 'Assign Canvessar',
                            'route'  => 'assign-canvessa',
                            'icon' => 'list',
                        ],

                    ],
                ],
                [
                    'text' =>'Create Polygon Selection',
                    'icon' => 'map-marker',
                    'active' => ['polygons/*'],
                    //'role' => 'super-admin',
                    'submenu' => [
                        [
                            'text' => 'Add',
                            'route'  => 'polygon',
                            'icon' => 'plus',
                            'permission' => 'write'
                        ],
                        [
                            'text' => 'List',
                            'route'  => 'polygon-list',
                            'icon' => 'list',
                        ],

                    ],
                ],
                [
                    'text' => 'Build Survey',
                    'route'  => 'create.walkbook',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'Assign',
                    'route'  => 'list.walkbook',
                    'icon' => 'list',
                ],
             
            ],
        ],
        [
            'text' => 'Manage Walkbooks',
            'icon' => 'bullhorn',
            'active' => ['survey/*'],
            'submenu' => [
                [
                    'text' => 'Build Survey',
                    'route'  => 'new.survey',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'Available Surveys',
                    'route'  => 'list.survey',
                    'icon' => 'list',
                ],
                [
                    'text' => 'Survey History',
                    'route'  => 'histroy.survey',
                    'icon' => 'list',
                ],
                [
                    'text' =>'Manage Videos',
                    'icon' => 'video-camera',
                    'active' => ['manage-videos/*'],
                    'submenu' => [
                        [
                            'text' => 'Add',
                            'route'  => 'create.video',
                            'icon' => 'plus',
                            'permission' => 'write'
                        ],
                        [
                            'text' => 'List',
                            'route'  => 'list.video',
                            'icon' => 'list',
                        ],

                    ],
                ]
            ],

        ],
        [
            'text' => 'Fraud Detection',
            'icon' => 'book',
            'active' => ['walkbook/*'],
            'submenu' => [
                [
                    'text' => 'Online Data Entry Tracking',
                    'route'  => 'fraud.walkbook',
                    'icon' => 'list',
                ],
                [
                    'text' => 'Offline Data Entry Tracking',
                    'route'  => 'dataMode.walkbook',
                    'icon' => 'list',
                ],
            ],
        ],

        /*[
            'text' => 'Manage Videos',
            'icon' => 'video-camera',
            'active' => ['manage-videos/*'],
            'submenu' => [
                [
                    'text' => 'Add',
                    'route'  => 'create.video',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'List',
                    'route'  => 'list.video',
                    'icon' => 'list',
                ],
            ],
        ],*/


        [
            'text' => 'Events',
            'icon' => 'book',
            'active' => ['event/*'],
            'submenu' => [
                [
                    'text' => 'Add Event',
                    'route'  => 'create.event',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'Event List',
                    'route'  => 'list.event',
                    'icon' => 'list',
                ],
                [
                    'text' => 'Add Attendee',
                    'route'  => 'create.attende',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
                [
                    'text' => 'Attendee List',
                    'route'  => 'list.attende',
                    'icon' => 'list',
                ],
                [
                    'text' => 'Upload Attendee',
                    'route'  => 'csv-form.attende',
                    'icon' => 'plus',
                    'permission' => 'write'
                ],
            ],
        ],

//        [
//            'text' => 'Attendee',
//            'icon' => 'book',
//            'active' => ['attende/*'],
//            'submenu' => [
//                [
//                    'text' => 'Add',
//                    'route'  => 'create.attende',
//                    'icon' => 'plus',
//                    'permission' => 'write'
//                ],
//                [
//                    'text' => 'List',
//                    'route'  => 'list.attende',
//                    'icon' => 'list',
//                ],
//                [
//                    'text' => 'Add csv',
//                    'route'  => 'csv-form.attende',
//                    'icon' => 'plus',
//                    'permission' => 'write'
//                ],
//            ],
//        ],

//        [
//            'text' => 'Density Tracking for walkbooks',
//            'icon' => 'book',
//            'active' => ['walkbook/*'],
//            'route'=>'track.walkbook'
//        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        App\CustomFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
