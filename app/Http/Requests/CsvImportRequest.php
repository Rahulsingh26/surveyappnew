<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CsvImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv_file' => "required|file",
            'csv_name' => "sometimes|required_if:csvSelectList,new|unique:csv_data,csv_filename"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'csv_file.max' => 'Maximum file size to upload is 8MB.',
            'csv_name.required_if' => 'The csv name field is required.'
        ];
    }
}
