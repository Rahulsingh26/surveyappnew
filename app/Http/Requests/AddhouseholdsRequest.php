<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddhouseholdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            //
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'plz fill',
            'phone.regex' => 'Format should be +123456789 or 123456789'
            //
        ];
    }
}
