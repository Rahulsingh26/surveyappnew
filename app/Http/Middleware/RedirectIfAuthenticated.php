<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Companie;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $subdomains=Companie::get()->toArray();
        foreach ($subdomains as  $subdomain) {
            if (request()->getHttpHost() == $subdomain['subdomain']) {
                if (Auth::guard($guard)->check()) {
                    return redirect('/dashboard');
                }

                return $next($request);
            }
        }
        if (request()->getHttpHost() == 'surveyappnew.n1.iworklab.com'){
            if (Auth::guard($guard)->check()) {
                return redirect('/dashboard');
            }
            return $next($request);
        }
        abort(404);
    }
}
