<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (Auth::check())
       {
           $role = Auth::user()->getRoleNames()[0];
           $role_id = Role::where(['name'=>$role])->pluck('id');
            if($role_id[0] == 1 || $role_id[0] == 3 || $role_id[0] == 4)
            {
                return $next($request);
            }
        }

       Auth::logout();
       return redirect()->route('login')->withErrors( ['email' => 'Authentication failure' ] );
    }
}
