<?php

namespace App\Http\Controllers;

use App\Models\Attende;
use App\models\User;
use App\Notifications\CompanyCreated;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Models\Companie;
use Illuminate\Support\Facades\Auth;
use Validator;

class CompanieController extends Controller
{
     use Notifiable;
    public function index(){
        return view('companie.add-user');
    }
    public function addCompanie(Request $request){
        $password = randomPassword(8);
        $rules = ['phone_number.regex'=>'Format should be +123456789 or 123456789'];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'subdomain'=> 'required|unique:companies,subdomain',
            'phone_number' => 'regex:/^\+?\d+$/',
        ],$rules)->validate();
        if ($validator){
            $companie= new Companie();
            $companie->subdomain = $request->subdomain.'.'.env('SUB_DOMAIN_URL');
            if($companie->save()){
                $user= new User();
                $user->companie_id = $companie->id;
                $user->name = $request->name;
                $user->phone_number = $request->phone_number;
                $user->email = $request->email;
                $user->address = $request->address;
                $user->password =  bcrypt($password);
                $user->status = $request->status;
                if ($user->save()){
                    $user->notify(new CompanyCreated($user,$password));
                    $user->assignRole('super-admin-company');
                }
            }
            return redirect()->route('list-companie')->with(['message' => 'User Added Successfully','alert-success' => 'alert-success' ]);
        }
        else {
            return redirect()->route('list-companie')->with(['message' => 'Some Issue.' ]);
        }

    }

    public function companieList(){
        $company = User::with('companie')->whereHas('roles',function($q){
            $q->whereIn('role_id',[4]);
        })->paginate(20);
        return view('companie.list',['company'=>$company]);
    }

    public function companieView($id){
        $company = User::with('companie')->whereHas('roles',function($q){
            $q->whereIn('role_id',[4]);
        })->where('companie_id',$id)->get();

         $users = User::whereHas('roles',function($q){
                $q->whereNotIn('role_id',[4]);
            })->where('companie_id',$id)->paginate(20);

        return view('companie.view',['users' => $users,'company' => $company]);
    }

    public function statusUpadte($id,$statusid){
        //for exception handling use try catch
        try{

            $status = User::where(['id' => $id])->first();
            if ($status) {
                if ($statusid == 1) {
                    $status->status = 0;
                } else {
                    $status->status = 1;
                }
                $data = $status->save();
                if ($data) {
                    return redirect()->route('list-companie')->with(['message' => 'companie status updated Successfully','alert-success'=>'alert-success']);
                } else {
                    return redirect()->route('list-companie')->with(['message' => 'companie status not updated','alert-error'=>'alert-error']);
                }
            } else {
                return redirect()->route('list-companie')->with(['message' => 'companie status not updated','alert-error'=>'alert-error']);
            }

        } catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();
            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
    }
}
