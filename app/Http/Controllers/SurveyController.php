<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Auth;
use Response;
use App\Models\Survey;
use App\Models\AssignWalkbook;
use App\Models\Walkbook;
use App\Models\Answer;
use Illuminate\Http\Request;
use Validator;
use DB;

class SurveyController extends Controller
{

    public function home(Request $request)
    {
        $surveys = Survey::with('assignWalkbook')->paginate();
        //dd($surveys[0]->AssignWalkbook[1]->walkbook);
        return view('survey.list', compact('surveys'));
    }

    # Show page to create new survey
    public function new_survey()
    {
        $videos = Video::where(['status' => 1])->get();
        $walbooks = Walkbook::where('status', 1)->get(['id', 'title'])->toArray();
        return view('survey.new', compact('videos', 'walbooks'));
    }

    public function create(Request $request, Survey $survey)
    {
        $idsToUpdate = $request->update[0];
        $ids = explode(",", $idsToUpdate);

        if ($ids) {
            foreach ($ids as $updateServey) {
                AssignWalkbook::where(['id' => $updateServey])->delete();
            }
        }
        $arr = $request->all();
        $validator = Validator::make($arr, [
            'title' => 'required|unique:survey,title',
            'walkbook_id' => 'required'
        ],
            [
                'walkbook_id.required' => 'The Workbook field is required.'
            ])->validate();
        $arr['user_id'] = Auth::id();
        //dd($arr);
        $surveyItem = $survey->create($arr+['userCreatedSurveyID' =>Auth::user()->id])->id;

        foreach ($request->walkbook_id as $walkbook) {
            AssignWalkbook::insert(['survey_id' => $surveyItem, 'walkbook_id' => $walkbook,'userCreatedAssignWorkbookID' => Auth::user()->id]);
        }

        return redirect()->route('list.survey')->with(['message' => 'Updated Successfully', 'alert-success' => 'alert-success']);
        // return redirect()->route('list.survey',[$surveyItem]);
    }


    # retrieve detail page and add questions here
    public function detail_survey(Survey $survey)
    {
        $survey->load('questions.user');
        //dd("hii");
        return view('survey.detail', compact('survey'));
    }


    public function edit(Survey $survey)
    {
        $videos = Video::where(['status' => 1])->get();
        $walbooks = Walkbook::where('status', 1)->get(['id', 'title'])->toArray();

        return view('survey.edit', compact('survey', 'videos', 'walbooks'));
    }

    # edit survey
    public function update(Request $request, Survey $survey)
    {
         //dd($request->all(),$survey->id);
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:survey,title,' . $survey->id,

        ])->validate();

        //dd($request->all());
        $survey->where('id', $survey->id)
            ->update(['title' => $request->title,'description' => $request->description,'video_id' => $request->video_id,'userCreatedSurveyID' => Auth::user()->id]);

        //$survey->update($request->only(['title', 'description','video_id','walkbook_id']));
        //return redirect()->action('SurveyController@detail_survey', [$survey->id]);
        return redirect()->route('list.survey')->with(['message' => 'Survey Updated Successfully', 'alert-success' => 'alert-success']);
    }

    # view survey publicly and complete survey
    public function view_survey(Survey $survey)
    {
        $survey->option_name = unserialize($survey->option_name);
        return view('survey.view', compact('survey'));
    }

    # view submitted answers from current logged in user
    public function view_survey_answers(Survey $survey)
    {
        $survey->load('user.questions.answers');
        // return view('survey.detail', compact('survey'));
        // return $survey;
        return view('answer.view', compact('survey'));
    }

    // TODO: Make sure user deleting survey
    // has authority to
    public function delete_survey(Request $request, Survey $survey)
    {
        $survey->delete();
        return redirect()->route('list.survey')->with(['message' => 'Survey Deleted Successfully', 'alert-success' => 'alert-success']);
    }

    public function deactivated(Request $request)
    {
        $data = AssignWalkbook::where(['walkbook_id' => $request->id])->first();
        if ($data) {
            return $data;
        }
        return redirect()->route('list.survey')->with(['message' => 'Survey Updated Successfully', 'alert-success' => 'alert-success']);
    }

    public function assignWalkbook($id)
    {
        $data = AssignWalkbook::where(['survey_id' => $id])->paginate();
        return view('survey.assign-walkbook', compact('data', 'id'));
    }

    public function addWalkbook($id)
    {
        $data = AssignWalkbook::where(['survey_id' => $id])->get();
        if ($data) {
            $walkbook = [];
            foreach ($data as $key => $walk) {
                $walkbook[$key] = $walk->walkbook_id;

            }
            $walk = Walkbook::whereNotIn('id', $walkbook)->get();
            return view('survey.add-walkbook', compact('walk', 'id'));
        }
        else{
            return view('survey.list');
        }
    }

    public function addNewWalkbook($id)
    {
        $walk = Walkbook::where('status', 1)->get(['id', 'title'])->toArray();
        return view('survey.add-walkbook', compact('walk', 'id'));
    }

    public function addserveyList(Request $request){
        $idsToUpdate = $request->update[0];
        $ids = explode(",", $idsToUpdate);

        if ($ids) {
            foreach ($ids as $updateServey) {
                AssignWalkbook::where(['id' => $updateServey])->delete();
            }
        }
        $arr = $request->all();
        $validator = Validator::make($arr, [
            'walkbook_id' => 'required'
        ],
            [
                'walkbook_id.required' => 'The Workbook field is required.'
            ])->validate();

        foreach ($request->walkbook_id as $walkbook) {
            AssignWalkbook::insert(['survey_id' => $request->surveyId, 'walkbook_id' => $walkbook]);
        }
        return redirect()->route('list.survey')->with(['message' => 'Updated Successfully', 'alert-success' => 'alert-success']);
    }

    public function deleteWorkbook($id){
        AssignWalkbook::where(['id' => $id])->delete();
        return redirect()->back()->with(['message' => 'Successfully delete', 'alert-success' => 'alert-success']);
        //return view('survey.assign-walkbook')->with(['message' => 'Successfully delete', 'alert-success' => 'alert-success',$id]);
    }

    public function surveyHistory(Request $request){
          //dd($request->date);
        $users = DB::table('users')->where('name', array($request->canvasser_name))->pluck('id');
        $other_impediments = DB::table('answer')->where('other_impediments', array($request->impediments))->pluck('canvasser_id');
       
        $query=Answer::select('survey_users.voter_id','answer.canvasser_id','survey_users.name as prospect','survey_user_walkbook.status as survey_status','answer.id as surveyID',DB::raw('DATE_FORMAT(answer.created_at, "%d-%b-%Y") as CreateOn_date'),'answer.*')
            ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
            ->join('survey_user_walkbook', 'answer.survey_user_id', '=', 'survey_user_walkbook.survey_user_id')
            ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
            ->orderBy('answer.id','DESC');



            if ($request->voter_ID) {
                $query->where('survey_users.voter_id','like','%'.$request->voter_ID.'%');
            }
            if ($request->canvasser_name) {
                $query->whereIn('answer.canvasser_id', $users);
            }
            if ($request->impediments) {
                $query->whereIn('answer.canvasser_id',$other_impediments);
                $query->where('answer.other_impediments',$request->impediments);

            }
            
            if ($request->date) {
                //dd(date('Y-m-d',strtotime($request->date)));
                 $query->whereRaw('answer.created_at like "%'.date('Y-m-d',strtotime($request->date)).'%"');
            }

            if ($request->status) {
               if ($request->status == 2) {
                   $request->status = 0;
               }
             $query->where(['survey_user_walkbook.status' => $request->status]);
            }



        $data = $query->paginate(10);
        // $data = $query->toSql();
        // dd($data);

        $data->appends(request()->all())->render();

        return view('survey.history', compact('data'));
    }

    public function surveyHistoryDetails($survey){
        $data=Answer::select('*')->where('id',$survey)->first();
        $ques=Answer::select('id','question_id','answer')->where(['survey_user_id'=>$data->survey_user_id,
            'walkbooks_id'=>$data->walkbooks_id,
            'survey_id'=>$data->survey_id
        ])->get();

        $arr = [];
        foreach ($ques as $qu) {

            $arr[]=Answer::select('answer.id as surveyID','question.id as questionID','question.title',
                'answer.answer','answer.notes','answer.additional_observation','question.question_type')
                ->join('question', 'answer.question_id', '=', 'question.id')
                ->where(['question_id' => $qu['question_id']])
                ->where(['answer.id' => $qu['id']])
                ->first();
        }

        return view('survey.historyView', compact('arr'));
    }

public function surveyHistoryDetailsDownload(){



    $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=file.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    );

    //$reviews =\DB::table('answer')->get();

    $reviews =Answer::select('survey_users.voter_id','survey_users.name as prospect','survey_user_walkbook.status as survey_status','answer.id as surveyID',DB::raw('DATE_FORMAT(answer.created_at, "%d-%b-%Y") as CreateOn_date'),'answer.*')
            ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
            ->join('survey_user_walkbook', 'answer.survey_user_id', '=', 'survey_user_walkbook.survey_user_id')
            ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
            ->orderBy('answer.id','DESC')
            ->get();
        //dd($reviews->toArray()); 
    $columns = array('S.no',  'Review ID', 'Prospect Name', 'Voter ID ', 'Impediments','Observations','Date','Status');

    $callback = function() use ($reviews, $columns)
    {
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);
        $i = 1;
        foreach($reviews as $review) {
            $satus = ($review->survey_status == 1 ) ? 'Completed' : 'Pending';
             if ( !empty( $review->additional_observation ) ){
                 $impediments = implode(',',  $review->additional_observation);
             }
            else {
              $impediments = 'N/A';
            }

            fputcsv($file, array($i++, $review->id, $review->prospect, $review->voter_id, $review->other_impediments, $impediments, $review->created_at, $satus));
        }
        fclose($file);
    };
    return Response::stream($callback, 200, $headers);







    
}





}
