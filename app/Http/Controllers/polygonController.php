<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PolygonSection;
use App\Models\CsvData;
use App\Models\SurveyUser;
use App\Models\User;
use App\Models\AssignPolygon;
class polygonController extends Controller
{

    public function polygonList(){
        $PolygonSection = PolygonSection::paginate(10);
       // return view('walkbook.polygon-list',compact('PolygonSection'));
    }

     public function index(){
        $csvData = CsvData::orderBy('id', "DESC")->paginate(20);
        return view('walkbook.polygon', ['csvData' => $csvData]);
    }

    public function shapeCreate(Request $request){
        // dd($request->all());
         $CsvData = explode(",",$request->csvID);
         $polyid = explode(",",$request->polyid);
         $data = SurveyUser::select('id')->whereIn('id', $polyid)->whereIn('csv_data_id',$CsvData)->get()->toArray();



        if (isset($data)) {
            $urveyUser_id = [];
            foreach ($data as $key => $id) {
                array_push($urveyUser_id, $id['id']);
            }
        }
        try {
            $secure = implode(",",$urveyUser_id);
            foreach ($CsvData as $key => $value) {
            	$polygonSection = new PolygonSection();
                $polygonSection->surveyUser_id = $urveyUser_id;
                $polygonSection->surveyUser_id_secure = md5($secure);
                $polygonSection->household = $request->polycount;
                $polygonSection->Csv_id = $value;
                $polygonSection->save();
            }
        }catch (\Exception $e) {
                 $error = $e->getMessage();
                 return "Error";
        }
        $excludeSave = PolygonSection::whereIn('Csv_id',$CsvData)->get();

        return response()->json([
       'view' => view('walkbook.polygon-nextStep', compact('excludeSave','household','CsvData'))->render()]);

    }
    public function polygonView($id){
        $polygon = PolygonSection::where(['id' => $id])->first();
        $ids=$polygon->surveyUser_id;
        $data = surveyUser::select('lat','lng')->whereIn('id', $ids)->get()->toArray();
        if ($data) {
                return view('walkbook.view-Polygon',['data' => $data]);
            } else {
                return redirect()->route('polygon-list')->with(['message' => 'Some issue occour']);
            }
    }

     public function deletepolygon($id)
    {
        $deleted = PolygonSection::where(['id' => $id])->delete();
        if($deleted){
            return redirect()->route('polygon-list')->with(['message' => 'Polygon shape Deleted Successfully','alert-success' => 'alert-success' ]);
        }

    }

    public function polygonSaveStep(Request $request){
        try {
        $data = PolygonSection::where('id', $request->rawId)
          ->update(['title' => $request->walkbookName]);
        return $request->walkbookName ;
        } catch (\Exception $e) {
                 $error = $e->getMessage();
                 return "Error";
        }
    }

    public function polyNextStep(Request $request){
       if ($request->cid) {
           $csvID = explode(",",$request->cid);
           $canvassers = User::role('canvasser')->where('status',1)->get();
           $data= PolygonSection::with('AssignPolygon','AssignPolygon.CanvessarUser')->whereIn('Csv_id',$csvID)->get();
           return view('walkbook.poly-nextStep',compact('canvassers', 'data','csvID'));
       }
       if ($request->csvId) {
           $csvID = explode(",",$request->cid);
           $canvassers = User::role('canvasser')->where('status',1)->get();
           $data= PolygonSection::with('AssignPolygon','AssignPolygon.CanvessarUser')->whereIn('Csv_id',$request->csvId)->get();

           return view('walkbook.poly-nextStep',compact('canvassers', 'data','csvID'));
       }

       return redirect('polygons/polygon')->with(['message' => 'Select the any checkbox', 'alert-danger' => 'alert-danger']);
   }

    public function polygonfinalStep(Request $request){
        $id = $request->csvID;
        foreach ($request->surveyUserIds as $excludeDataId) {
            foreach ($request->canvessar_id as $key => $canvessar_id) {
                $assign = new AssignPolygon();
                $assign->canvessar_id = $canvessar_id;
                $assign->save_id = $excludeDataId;
                $assign->save();
            }
        }

        return redirect('polygons/polygon')->with(['message' => 'Canvessar successfully assign', 'alert-success' => 'alert-success']);
     }
}
