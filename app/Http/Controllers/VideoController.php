<?php

namespace App\Http\Controllers;

use App\Http\Requests\VideRequest;
use App\Models\Video;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests\VideoRequest;
use Auth;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $videos = Video::paginate(20);
        return view('manage-videos.list', compact('videos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('manage-videos.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {
        //
        if($request->hasFile('file')){
            $file =$request->file('file');
            $file_name = time().'-'.$file->getClientOriginalName();
            $path = public_path().'/uploads/videos';
            $uploaded = $file->move($path, time().'-'.$file->getClientOriginalName());
            if($uploaded) {
                $video = new Video();
                $video->title = $request->title;
                $video->file_name = $file_name;
                $video->status = $request->status;
                $video->userCreatedVideoID = Auth::user()->id;
                if ($video->save()) {
                    return redirect()->route('list.video')->with(['message' => 'Video Uploaded Successfully','alert-success'=>'alert-success']);
                }
            }
            return redirect()->route('list.video')->with(['message' => 'Some issue']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
        return view('manage-videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:videos,title,'.$video->id,
            'file'=>'mimes:mp4,flv|max:200000',
        ])->validate();
        
        $video = Video::find($video->id);
        $video->title = $request->title;
        $video->userCreatedVideoID = Auth::user()->id;
        if($request->has('file')){
            $file =$request->file('file');
            $file_name = time().'-'.$file->getClientOriginalName();
            $uploaded = $file->move('uploads/videos', time().'-'.$file->getClientOriginalName());
            if($uploaded) {
                unlink(public_path('uploads/videos/' . $video->file_name));
                $video->file_name = $file_name;
            }
        }
        $video->status = $request->status;
        if($video->save()) {
            return redirect()->route('list.video')->with(['message' => 'Video Edited Successfully', 'alert-success' => 'alert-success']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
       $file_name = $video->file_name;
       $deleted = $video->delete();
       if($deleted){
           if(file_exists(public_path('uploads/videos/'.$file_name))) {
               unlink(public_path('uploads/videos/' . $file_name));
           }
            return redirect()->route('list.video')->with(['message' => 'Video Deleted Successfully','alert-success' => 'alert-success' ]);
       }
    }
}
