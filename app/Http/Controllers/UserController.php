<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Survey;
use Auth;
use Validator;
use App\models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Notifications\UserCreated;
use Illuminate\Notifications\Notifiable;


class UserController extends Controller
{
    //

    use HasRoles;
    use Notifiable;

    public function passwordView(){
        return view('account-settings.change-password');
    }

    //password update
    public function passwordUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'current_password'=>['required', function ($attribute, $value, $fail) {
                if (!Hash::check($value, Auth::user()->password)) {
                            $fail('Current Password is invalid.');
                        }
                }],
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
        ])->validate();
        if($validator) {
            $user = Auth::user();
            $user->password = Hash::make($request->password);
            $user->save();
            return Redirect::back()->with(['message' => 'Password Changed Successfully','alert-success' => 'alert-success' ]);
        }

    }


    public function emailView() {
        return view('account-settings.change-email');
    }

    //email update
    public function emailUpdate(Request $request) {
        $user_id = Auth::user()->id;
        $rules = ['email.exists'=>'Invalid Current Email'];
        $validator = Validator::make($request->all(), [
            'email' =>['required',Rule::exists('users')->where(function ($query) use ($user_id) {
                $query->where('id',$user_id );
            }),
             ],
            'new_email' => 'required|unique:users,email,'.$user_id,
        ],$rules)->validate();

        if($validator) {
            $user = Auth::user();
            $user->email = $request->new_email;
            $user->save();
            return Redirect::back()->with(['message' => 'Email Changed Successfully','alert-success' => 'alert-success' ]);
        }

    }

    public function addUserView() {
        $permissions = Permission::get();
        return view('manage-users.add-user',["permissions" => $permissions]);
    }
    //adding canvasser/admin user
    public function addUser(Request $request) {
        $rules = ['phone_number.regex'=>'Format should be +123456789 or 123456789'];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'phone_number' => 'regex:/^\+?\d+$/',
            'rights' =>  Rule::requiredIf($request->user_type == 3),
        ],$rules)->validate();
        if($validator) {
            $password = randomPassword(8);
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number  = $request->phone_number;
            $user->companie_id  = Auth::user()->companie_id ? Auth::user()->companie_id : null;
            $user->address  = $request->address;
            $user->status  = $request->status;
            $user->password = bcrypt($password);
            if($user->save()) {
                $role = Role::find($request->user_type);
                $user->assignRole($role->name);

                if($request->user_type == 3) {
                    foreach ($request->rights as $right){
                        $permission = Permission::find($right);
                        $user->givePermissionTo($permission->name);
                    }
                }
                $user->notify(new UserCreated($user,$password));
                return redirect()->route('list-user')->with(['message' => 'User Added Successfully','alert-success' => 'alert-success' ]);
                //return Redirect::back()->with(['message' => 'User Added Successfully','alert-success' => 'alert-success' ]);
            }else {
                return redirect()->route('list-user')->with(['message' => 'Some Issue.']);
            }
        }
    }

    public function editUserView($id) {

        $user = User::findOrFail($id);
        $user_permissions = $user->getAllPermissions()->map(function($user){
            return $user->id;
        })->toArray();
        $user_role = $user->getRoleNames();
        $user_role = Role::where(['name'=>$user_role])->get()->map(function($role){
                return $role->id;
        })->toArray();

        $permissions = Permission::get();
        return view('manage-users.edit-user',["permissions" => $permissions,"user" => $user ,"user_permissions"=>$user_permissions,"user_role"=>$user_role]);
    }
    //adding canvasser/admin user
    public function editUser(Request $request,$id) {
        $rules = ['phone_number.regex'=>'Format should be +123456789 or 123456789'];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id.'||regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'phone_number' => 'regex:/^\+?\d+$/',
            'rights' => Rule::requiredIf($request->user_type == 3),
        ],$rules)->validate();
        if($validator) {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number  = $request->phone_number;
            $user->address  = $request->address;
            $user->status  = $request->status;
            if($user->save()) {
                $user->syncRoles();
                $role = Role::find($request->user_type);
                $user->assignRole($role->name);
                $user->syncPermissions();
                if($request->user_type == 3) {
                    foreach ($request->rights as $right) {
                        $permission = Permission::find($right);
                        $user->givePermissionTo($permission->name);
                    }
                }
                return redirect()->route('list-user')->with(['message' => 'User Updated Successfully','alert-success' => 'alert-success' ]);
            }else {
                return redirect()->route('list-user')->with(['message' => 'Some Issue.']);
            }
        }
    }

    public function listUser() {
        $auth_user = Auth::user()->companie_id;
        if($auth_user){
            $users = User::whereHas('roles',function($q){
                $q->whereNotIn('role_id',[4]);
            })->where('companie_id',$auth_user)->paginate(20);
        }
        else{
             $users = User::whereHas('roles',function($q){
             $q->whereNotIn('role_id',[1]);
           })->paginate(20);
        }

        return view('manage-users.list-user',['users'=>$users]);
    }
}
