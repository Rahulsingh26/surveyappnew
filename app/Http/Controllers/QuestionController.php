<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use App\Models\Question;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

class QuestionController extends Controller
{
  
    public function store(Request $request, Survey $survey) 
    {
        $rules = ['option_name.0.required_if'=>'Option field is required'];
        $validator = Validator::make($request->all(), [
            'question_type'=>'required',
            'title' => 'required',
            'option_name.0' =>'required_if:question_type,3|required_if:question_type,4'
        ],$rules)->validate();
      $arr = $request->all();
      $arr['user_id'] = Auth::id();
      $arr['userCreatedQuestionID'] = Auth::user()->id;
      if(isset($arr['option_name'])) {
          $arr['option_name'] = array_filter($arr['option_name']);
      }
      $survey->questions()->create($arr);
      return back();
    }

    public function edit(Question $question) 
    {
      return view('question.edit', compact('question'));
    }

    public function update(Request $request, Question $question) 
    {
        $rules = ['option_name.0.required_if'=>'Option field is required'];
        $validator = Validator::make($request->all(), [
            'question_type'=>'required',
            'title' => 'required',
            'option_name.0' =>'required_if:question_type,3|required_if:question_type,4'
        ],$rules)->validate();
        $arr = $request->all();
        $arr['userCreatedQuestionID'] = Auth::user()->id;
        if($arr['question_type'] ==1 || $arr['question_type'] ==2) {
            $arr['option_name'] = null;
        }

      $question->update($arr);
      return redirect()->action('SurveyController@detail_survey', [$question->survey_id]);
    }

    public function delete_question(Request $request, Question $question)
    {
        $question->delete();
        return redirect()->back()->with(['message' => 'Question Deleted Successfully','alert-success' => 'alert-success' ]);
    }

}
