<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\Answer;
use App\Models\Event;
use App\Models\Attende;
use App\Models\EventAttende;
use App\Http\Requests;
use Validator;
use DB;


class EventAttendeController extends Controller
{
	/**
	**@param $token,$userID,$status
	**@return 
	**/
	public static function changeAttendeStatus($token,$userID,$status,$eventID){
		//dd($token,$userID,$status);
		$eventAttendeObj = EventAttende::where(['attendeID' => $userID])->where(['attendeToken' => $token])->where(['eventID' => $eventID])->first();
		//dd($eventAttendeObj);
		if ($eventAttendeObj) {
			if($status == 1) {
				$eventAttendeObj->attendeToken = $token.'-accepted';
				$eventAttendeObj->attendeEventAcceptRejectStatus	= 1;
				$eventAttendeObj->attendeEventAccepetedTiming = date('Y-m-d h:i:s');
				$eventAttendeObj->save();
				return "success";
			} else {
				$eventAttendeObj->attendeToken = $token.'-reject';
				$eventAttendeObj->attendeEventAcceptRejectStatus	= 0;
				$eventAttendeObj->attendeEventAccepetedTiming = date('Y-m-d h:i:s');
				$eventAttendeObj->save();
				return "success";
			}
		} else {
			return "error";
		}
	}


	/**
	**@param $eventID,$attendeID
	**@return success or error message
	**/
	public static function deleteAttendeEvent($eventID,$attendeID){
		$eventAttende = EventAttende::where(['eventID' => $eventID])->where(['attendeID' => $attendeID])->first();
		if ($eventAttende) {
			$eventAttende = EventAttende::where(['eventID' => $eventID])->where(['attendeID' => $attendeID])->first()->delete();
			return "success";
		} else {
			return "error";
		}
	}
}