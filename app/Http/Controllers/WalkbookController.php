<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Helpers\Address;
use App\Models\SurveyUser;
use App\Helper\AppHelper;
use App\Models\AssignWalkbook;
use App\Models\Survey;
use App\Models\SurveyUserWalkbook;
use App\Models\Walkbook;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use DB;
use Auth;

class WalkbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $pages;
    public function __construct(){
         $this->pages = Walkbook::RECORDSPERPAGE;
    }
    public function index()
    {

        $walkbooks = Walkbook::with('assignWalkbookDetails')->paginate($this->pages);

        $surveys = Survey::get();
        return view('walkbook.list',compact('walkbooks', 'surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $canvassers = User::role('canvasser')->where('status',1)->get();
        return view('walkbook.add',compact('canvassers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = ['user_id.required'=>'Canvasser is required'];
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:walkbooks,title',
            'user_id'=>'required|array',
        ],$rules)->validate();

        $walkbook = new Walkbook();
        $walkbook->title = $request->title;
        $walkbook->status = $request->status;
        $walkbook->user_id = $request->user_id;
        $walkbook->userCreatedWorkbookID = Auth::user()->id;
        if($walkbook->save()) {
            return redirect()->route('list.walkbook')->with(['message' => 'Walkbook added Successfully', 'alert-success' => 'alert-success']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Walkbook  $walkbook
     * @return \Illuminate\Http\Response
     */
    public function show(Walkbook $walkbook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Walkbook  $walkbook
     * @return \Illuminate\Http\Response
     */
    public function edit(Walkbook $walkbook)
    {
        //
        $canvassers = User::role('canvasser')->where('status',1)->get();
        $associatedSurveyUsers = $walkbook->surveyUser()->paginate($this->pages );
        return view('walkbook.edit', compact('walkbook','associatedSurveyUsers','canvassers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Walkbook  $walkbook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Walkbook $walkbook)
    {
        //
        $rules = ['user_id.required'=>'Canvasser is required'];
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:walkbooks,title,'.$walkbook->id,
            'user_id'=>'required|array',
        ],$rules)->validate();

        $walkbook->title = $request->title;
        $walkbook->status = $request->status;
        $walkbook->user_id = $request->user_id;
        $walkbook->userCreatedWorkbookID = Auth::user()->id;
        if($walkbook->save()) {
            return redirect()->route('list.walkbook')->with(['message' => 'Walkbook Edited Successfully', 'alert-success' => 'alert-success']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Walkbook  $walkbook
     * @return \Illuminate\Http\Response
     */
    public function destroy(Walkbook $walkbook)
    {
        //
        $deleted = $walkbook->delete();
        if($deleted){
            return redirect()->route('list.walkbook')->with(['message' => 'Walkbook Deleted Successfully','alert-success' => 'alert-success' ]);
        }
    }

    public function getWalkbooksFromAjax(){
        $walbooks = Walkbook::where('status',1)->get(['id','title'])->toArray();
        $dropDown = "<div class='walkBook-container'><select name='walkbook' id ='walkbook' style='margin-right:5px'><option value='0'>New</option>";
        foreach ($walbooks as $walbook){
            $dropDown.= "<option value=".$walbook['id'].">".$walbook['title']."</option>";
        }
        $dropDown.="</select><input type='text' name='title' id='walkbook_name' style='margin-right:5px;padding-left:2px;' placeholder='Walkbook Name'><button id='add' class='btn btn-info'>Add</button></div>";

        return $dropDown;
    }
    public function ajaxAddSurveyUserToWalkbook(Request $request){
        $id  = $request->id;
        $title  = $request->title;
        if($request->select_all == 'on'){
            $SurveyUser = SurveyUser::select('id')->where($request->selectf , '=', $request->textserach)->pluck('id');
            $request->suveyUsersId = $SurveyUser;
        }
        if($id == 0){
            if($title == ''){
                return response()->json([
                    'status' => 404,
                    'message' => 'Please Add Walkbook Name.'
                ]);
            }
           if(!$this->checkWalkbook($title)) {
               $walkbook = new Walkbook();
               $walkbook->title = $title;
               $walkbook->save();
               $id = $walkbook->id;
           }else{
               return response()->json([
                   'status' => 404,
                   'message' => 'WalkBook Already Exists.'
               ]);
           }
       }else{
           $walkbook = Walkbook::find($id);
       }
       foreach ($request->suveyUsersId as $surveyUserId){
            $exists = $this->checkSurveyUserAssociatedWithWalkbook($id,$surveyUserId);
            if($exists){
                continue 1;
            }
           $walkbook->surveyUser()->attach($surveyUserId);
       }

           return response()->json([
               'status' => 200,
               'message' => 'Added to Walkbook.'
           ]);

    }

    public function checkSurveyUserAssociatedWithWalkbook($id,$surveyUserId){
        $walkbook = Walkbook::find($id);
        return $walkbook->surveyUser->contains($surveyUserId);
    }

    public function checkWalkbook($title){
       return Walkbook::where('title',$title)->count();
    }


    public function tracking()
    {
        $radius = 30;
        $sur_user = SurveyUserWalkbook::with('surveyUser')
//            ->groupBY('walkbook_id')
            ->get();
        $record = [];

        foreach ($sur_user as $key => $user) {

            $current_lat = isset($user->surveyUser->lat) ? $user->surveyUser->lat : 0;
            $current_lng = isset($user->surveyUser->lng) ? $user->surveyUser->lng : 0;

            if ($current_lat != null && $current_lng != null) {
                $record[] = Answer::with('survey')->select('survey_users.lat as users_latitude', 'survey_users.lng as users_longitute', 'survey_users.name as prospect', 'answer.lat', 'answer.lng', 'answer.*')
                    ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
                    ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
                    ->where('walkbooks_id', $user['walkbook_id'])
                    ->selectRaw("6371 * acos(cos(radians($current_lat))
                            * cos(radians(survey_users.lat))
                            * cos(radians(survey_users.lng)
                            - radians($current_lng))
                            + sin(radians($current_lat))
                            * sin(radians(survey_users.lat))) AS distance")
                    ->orderBy('distance', 'ASC')
                    ->havingRaw("distance < ?", [$radius])
                    ->where('status', 1)
                    ->get();
            }
        }


        $surveysId = [];
        foreach ($record as $sur_user_id) {
            foreach ($sur_user_id as $sur_usersID) {
                $surveysId[] = $sur_usersID->survey_user_id;
            }
        }
        $surveysId = array_unique($surveysId);
        $survey = [];
        foreach ($record as $qu) {
            foreach ($qu as $quData) {

                $surveyUserWalkBook = SurveyUserWalkbook::with('walkbook', 'walkbook.walkbookRelatedToSurvey.survey','surveyUser')->select('*', DB::raw('count(*) as completed', 'survey_user_id'))
                    ->where(['walkbook_id' => $quData->walkbooks_id])
                    ->where(['canvasser_id' => $quData->canvasser_id])
                    ->whereIn('survey_user_id', $surveysId)
                    ->groupBy('walkbook_id', 'canvasser_id')
                    ->first();

                if($surveyUserWalkBook){
                    $survey[$surveyUserWalkBook->canvasser_id.$quData->walkbooks_id] = $surveyUserWalkBook->toArray();
                    $surveyUserIds = SurveyUserWalkbook::where(['walkbook_id' => $quData->walkbooks_id])
                        ->where(['canvasser_id' => $quData->canvasser_id])->pluck('survey_user_id')->toArray();
                    $survey[$surveyUserWalkBook->canvasser_id.$quData->walkbooks_id]['survey_user_ids'] = implode(",", $surveyUserIds);
                }

            }

        }
        $data = array_values($survey);
        return view('walkbook.tracking', compact('data'));
    }

    public function perSquareMile(Request $request){
        $miles=$request->id;

        $radius = $miles * 1.60934;
        $sur_user= SurveyUserWalkbook::with('surveyUser')
//            ->groupBY('walkbook_id')
            ->get();
        $record = [];
        foreach ($sur_user as  $key =>$user) {
            $current_lat= isset($user->surveyUser->lat) ? $user->surveyUser->lat:0;
            $current_lng= isset($user->surveyUser->lng) ? $user->surveyUser->lng:0;

            if ($current_lat != null && $current_lng != null){
                $record[] =  Answer::with('survey')->select('survey_users.lat as users_latitude','survey_users.lng as users_longitute','survey_users.name as prospect','answer.lat','answer.lng','answer.*')
                    ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
                    ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
                    ->where('walkbooks_id',$user['walkbook_id'])
                    ->selectRaw("6371 * acos(cos(radians($current_lat))
                            * cos(radians(survey_users.lat))
                            * cos(radians(survey_users.lng)
                            - radians($current_lng))
                            + sin(radians($current_lat))
                            * sin(radians(survey_users.lat))) AS distance")
                    ->orderBy('distance', 'ASC')
                    ->havingRaw("distance < ?", [$radius])
                    ->where('status',1)
                    ->get();
            }
        }

        $surveysId=[];
        foreach ($record as $sur_user_id){
            foreach ($sur_user_id as $sur_usersID){

                $surveysId[]=$sur_usersID->survey_user_id;
            }
        }
        $surveysId = array_unique($surveysId);
        $survey=[];
        foreach ($record as $qu) {
            foreach ($qu as $quData){

                $surveyUserWalkBook = SurveyUserWalkbook::with('walkbook', 'walkbook.walkbookRelatedToSurvey.survey','surveyUser')->select('*', DB::raw('count(*) as completed', 'survey_user_id'))
                    ->where(['walkbook_id' => $quData->walkbooks_id])
                    ->where(['canvasser_id' => $quData->canvasser_id])
                    ->whereIn('survey_user_id', $surveysId)
                    ->groupBy('walkbook_id', 'canvasser_id')
                    ->first();

                if($surveyUserWalkBook){
                    $survey[$surveyUserWalkBook->canvasser_id.$quData->walkbooks_id] = $surveyUserWalkBook->toArray();
                    $surveyUserIds = SurveyUserWalkbook::where(['walkbook_id' => $quData->walkbooks_id])
                        ->where(['canvasser_id' => $quData->canvasser_id])->pluck('survey_user_id')->toArray();
                    $survey[$surveyUserWalkBook->canvasser_id.$quData->walkbooks_id]['survey_user_ids'] = implode(",", $surveyUserIds);
                }
            }

        }

        $data=array_values($survey);


        return view('walkbook.ajax', compact('data'))->render();
    }

    public function houseHoldView(Request $request){
        $ar[] = explode(",",$request->suvalue);
        $rec = array_unique($ar[0]);
        $record = [];
        foreach ($rec as $a) {
            $record[] = SurveyUserWalkbook::with('surveyUser')
                ->where(['survey_user_id'=>$a,
                    'walkbook_id'=>$request->id,
                ])->first();
        }
        return view('walkbook.houseHold', compact('record'));
    }

    public function fraudDetection(Request $request){
        $getWalkList = [];
        $canvassers = User::role('canvasser')->where('status',1)->get();
        $walkbookData = walkbook::get();

        $query=Answer::with('walkbook','users')->select('answer.canvasser_id','survey_users.lat as survey_lat','survey_users.lng as survey_lng','answer.lat as current_lat','answer.lng as current_lng','survey_users.name as prospect','answer.id as surveyID','answer.survey_user_id','answer.walkbooks_id')
            ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
            ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
            ->where('status',1)
            ->where('answer.data_mode',"Online");
            if($request->canvessar_id) {
                $query->where('answer.canvasser_id',$request->canvessar_id);
            }
            if($request->walkbooks_id) {
            $query->where('answer.walkbooks_id',$request->walkbooks_id);
        }

        $data = $query->paginate();
        $data->appends(request()->all())->render();

        if (count($data)){
            foreach ($data as $keys => $distance){
                $lat1 = $distance->survey_lat;
                $lon1= $distance->survey_lng;
                $lat2= $distance->current_lat;
                $lon2= $distance->current_lng;
                $unit="M";
                // echo "<pre>", print_r($distance->walkbook['title']), "</pre>";
                $distanceCal[]=$this->distance($lat1, $lon1, $lat2, $lon2, $unit);
                // $getWalkList[$distance->walkbooks_id] = $this->getWalkList($distance->canvasser_id);
                $getWalkList[$distance->walkbook['title']] = $this->getWalkList($distance->walkbook['title']);

            }
             // print_r($getWalkList);
            // dd($getWalkList);
            // dd($distance->walkbook->title);

            $records['distance']= $distanceCal;
        }
        return view('walkbook.fraudDetection', compact('data','records','canvassers', 'getWalkList','walkbookData'));
    }

    public function networkStatus(Request $request){

        $canvassers = User::role('canvasser')->where('status',1)->get();
        $walkbookData = walkbook::get();


        $query = Answer::with('walkbook','users')->select('answer.canvasser_id','survey_users.lat as survey_lat','survey_users.lng as survey_lng','answer.lat as current_lat','answer.lng as current_lng','survey_users.name as prospect','answer.id as surveyID','answer.survey_user_id','answer.walkbooks_id')
            ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
            ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
            ->where('status',1)
            ->where('answer.data_mode',"Offline");

        if($request->canvessar_id) {
            $query->where('answer.canvasser_id',$request->canvessar_id);
        }
         if($request->walkbooks_id) {
            $query->where('answer.walkbooks_id',$request->walkbooks_id);
        }

        $data = $query->paginate();
        $data->appends(request()->all())->render();

        if (count($data)) {
            foreach ($data as $keys => $distance) {
                $lat1 = $distance->survey_lat;
                $lon1 = $distance->survey_lng;
                $lat2 = $distance->current_lat;
                $lon2 = $distance->current_lng;
                $unit = "M";
                $distanceCal[] = $this->distance($lat1, $lon1, $lat2, $lon2, $unit);
                $getWalkList[$distance->walkbook['title']] = $this->getWalkList($distance->walkbook['title']);
            }
            $records['distance'] = $distanceCal;
        }
        return view('walkbook.dataMode',  compact('data','records','canvassers','getWalkList','walkbookData'));
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "M") {
                $dataRound=round($miles,1);
                if ($dataRound == 0.1 || $dataRound == 0.0){
                    return round($miles);
                }
                else{
                    return $miles;
                }
            } else {
                return ($miles * 0.8684);
            }
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function changeSurvey(Request $request){

        $isUpdated = AssignWalkbook::updateOrCreate(['walkbook_id' => $request->walkbook], [
            'survey_id' => $request->survey_id
        ]);

        $isUpdated = AssignWalkbook::updateOrCreate(['walkbook_id' => $request->walkbook], [
            'survey_id' => $request->survey_id
        ]);
        if($isUpdated){
            return response()->json([
                'status' => 202
            ]);
        }else{
            return response()->json([
                'status' => 404
            ]);
        }
    }

    public function getWalkList($id = false){

       $walkbooks = Walkbook::with('surveyUser')->where('status',1)->get();
       $res =[];
       $results =[];


       if($walkbooks){
           // $results = [];
        // dd($walkbooks->toArray());
           foreach ($walkbooks as $walkKey => $walk) {
               // if (!empty($walk) && !empty($walk['user_id']) && in_array(Auth()->user()->id,$walk['user_id'])) {
               // if (!empty($walk) && !empty($walk['user_id']) && in_array(25,$walk['user_id'])) {
               if (!empty($walk) && !empty($walk['title']) && $id == $walk['title']) {

                   if(isset($walk['surveyUser'])) {
                      $walkbooks[$walkKey]['totalwalkbook'] = count(AppHelper::getHouseHold($walk['surveyUser']));


                       $walkbooks[$walkKey]['walkadd'] =Null;
                       $houseHoldArr = [];

                      /*(03/01/20) Begin*/
                      $pendingHouseHold =  \DB::table('survey_user_walkbook')
                      ->where('survey_user_walkbook.status', 0)
                      ->where('survey_user_walkbook.walkbook_id', $walk->id)
                      ->join('survey_users', 'survey_users.id', 'survey_user_walkbook.survey_user_id')
                      ->groupBy('survey_users.house_hold_id')
                      ->get();

                      $walkbooks[$walkKey]['Household_left'] =  $pendingHouseHold->count();
                      /*(03/01/20) End*/
                      $res = ['totalwalkbook' => $walkbooks[$walkKey]['totalwalkbook'],  'Household_left' => $walkbooks[$walkKey]['Household_left']];
                   }

                   $results[$walkKey] = $walkbooks[$walkKey];

                    return  $res;
               }


           }
           // return $this->sendResponse( 'List of WalkBook.',['lists' => AppHelper::paginate(collect($results), 'walkbook_lists')]);
       }
       else{
           return $this->sendErrorMessage('WalkBook list empty.', 401);
       }
   }


}
