<?php

namespace App\Http\Controllers;

use App\Exports\WalkbookExport;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Survey;
use App\Models\SurveyUser;
use App\Models\SurveyUserWalkbook;
use App\Models\Walkbook;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;


class ReportController extends Controller
{
    use HasRoles;
    use Notifiable;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportManage(Request $request){
        $surveyList = Survey::whereHas('questions', function($q){
            $q->whereIn('question_type', [3, 4]);
        })->get();
        $walkbookList = Walkbook::has('surveyUser')->get();
        return view('reportsManage', compact('surveyList', 'walkbookList'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function surveyQuestion(Request $request){
        if($request->surveylist) {
            return Question::where('survey_id', $request->surveylist)->whereIn('question_type', [3, 4])->get();
        }
        else{
            return "error";
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function questionReport(Request $request){
        $question = Question::find($request->quesId);

        $questionOptions = $question->option_name;
        $data = [];
        foreach ($questionOptions as $value){
            if($question->question_type == 3){
                $data[] = Answer::where([
                    'status' => 1,
                    'question_id' => $request->quesId,
                    'survey_id' => $request->surveylist,
                    'answer' => '"'.$value.'"'
                ])->count();
            }elseif ($question->question_type == 4){
                $answers = Answer::where([
                    'status' => 1,
                    'question_id' => $request->quesId,
                    'survey_id' => $request->surveylist,
                ])->get();
                $counter = 0;
                foreach ($answers as $answer){
                    if(in_array($value, $answer->answer)){
                        $counter++;
                    }
                }
                $data[] = $counter;
            }

        }

        return [
            'labels' => $questionOptions,
            'data' => $data
        ];

    }

    /**
     * @param Request $request
     * @param $walkbookId
     * @return mixed
     */
    public function downloadWalkBook(Request $request, $walkbookId){

        $surveyUsers = SurveyUserWalkbook::userListByWalkbook($walkbookId);

        return \Excel::create('walkbook-' . time(), function ($excel) use ($surveyUsers) {
            $excel->sheet('sheet name', function ($sheet) use ($surveyUsers) {
                $sheet->fromArray($surveyUsers);
            });
        })->download('csv');
    }
}
