<?php

namespace App\Http\Controllers;

use App\Models\HouseHold;
use App\Models\SurveyUser;
use App\Models\Tag;
use App\Models\User;
use App\Models\AssignPolygonWalkbook;
use App\Models\SaveData;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\Types\Null_;
use Validator;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use App\Models\CsvData;
use App\Http\Requests\CsvImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Contracts\Session\Session;
use App\Http\Requests\AddhouseholdsRequest;
use App\Helpers\Address;
use Auth;


class SurveyUserController extends Controller
{
    use HasRoles;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $csvDataId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, CsvData $csvData)
    {

        $pages = 20;
        if (isset($request->type) && isset($request->value) && $request->value != '') {
            $value = $request->value;

            if ($request->type == 'house_hold') {
                $surveyUsers = SurveyUser::whereHas('houseHold', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            } elseif ($request->type == 'region') {
                $surveyUsers = SurveyUser::where($request->type, 'LIKE', "%$request->value%");
            } elseif ($request->type == 'Tag') {
                $surveyUsers = SurveyUser::where($request->type, 'LIKE', "%$request->value%");
                $surve = Tag::select('survey_user_id')->where('title', 'LIKE', "%$request->value%")->get();

                foreach ($surve as $data) {

                    if (count($surve)) {
                        $surveyUsers = SurveyUser::where('id', $data->survey_user_id);

                    }
                }
            } else {
                $surveyUsers = SurveyUser::where($request->type, 'LIKE', "%$request->value%");
            }
        } else {
            $surveyUsers = new SurveyUser();
        }

        $surveyUsers = $surveyUsers->where(['csv_data_id' => $csvData->id])->paginate($pages);
        return view('manage-households.list', ['surveyUsers' => $surveyUsers, 'csvData' => $csvData]);
    }


    public function addedUserManually(Request $request)
    {
        $pages = 20;
        if (isset($request->type) && isset($request->value) && $request->value != '') {
            $value = $request->value;

            if ($request->type == 'house_hold') {
                $surveyUsers = SurveyUser::whereHas('houseHold', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            } elseif ($request->type == 'region') {
                $surveyUsers = SurveyUser::where($request->type, 'LIKE', "%$request->value%");
            } elseif ($request->type == 'Tag') {
                $surveyUsers = SurveyUser::where($request->type, 'LIKE', "%$request->value%");
                $surve = Tag::select('survey_user_id')->where('title', 'LIKE', "%$request->value%")->get();

                foreach ($surve as $data) {

                    if (count($surve)) {
                        $surveyUsers = SurveyUser::where('id', $data->survey_user_id);

                    }
                }
            } else {
                $surveyUsers = SurveyUser::where($request->type, 'LIKE', "%$request->value%");
            }
        } else {
            $surveyUsers = new SurveyUser();
        }

        $surveyUsers = $surveyUsers->whereNull('csv_data_id')->paginate($pages);
        return view('manage-households.list', ['surveyUsers' => $surveyUsers]);
    }

    public function finance(Request $request)
    {
        if ($request->type == "Financial Contributor") {
            $surveyUsers = SurveyUser::where('financial_contributor', 'Yes')->get();

        } else {
            $surveyUsers = SurveyUser::all();
        }
        $data = view('manage-households.ajax', ['surveyUsers' => $surveyUsers])->render();

        return response()->json(["data" => $data, "status" => "200", "message" => "SUccessfully"]);
        // return redirect()->route('surveyUser.finance')->with(['message' => 'Added Successfully', 'alert-success' => 'alert-success']);
        // return view('manage-households.finance', ['surveyUsers1' => $surveyUsers1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('manage-households.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //array_merge($request->all(),['userCreatedID' => Auth::user()->id]);
        //dd($request->all());

        $rules = ['phone.regex' => 'Format should be +123456789 or 123456789'];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            //  'voter_id' => 'required'|'unique:survey_users,voter_id,' . $SurveyUser->id,

            'zipcode' => 'required',
            'country' => 'required',
            'street' => 'required',
            'state' => 'required',
            'city' => 'required',
        ], $rules)->validate();

        if ($validator) {

            $SurveyUser = SurveyUser::create($request->except('_token', 'tags', 'notes'), ['userCreatedSurveyUserID' => Auth::user()->id]);

            if ($SurveyUser) {

                foreach ($request->notes as $note1) {

                    if (!empty($note1)) {
                        $note = new Note();

                        $note->survey_user_id = $SurveyUser->id;
                        $note->text = $note1;
                        $note->userCreatedNoteID = Auth::user()->id;
                        $note->save();

                    }

                }
                if ($request->tags != Null) {


                    foreach ($request->tags as $single) {

                        if (!empty($single)) {
                            // $surveyUsers = new SurveyUser();
                            $tag = new Tag();
                            $tag->survey_user_id = $SurveyUser->id;
                            $tag->title = $single;
                            $tag->userCreatedTagID = Auth::user()->id;
                            $tag->save();
                        }

                    }
                }
                if ($houseHoldId = $this->checkAddressExist($request->street)) {

                    $this->updateHouseHold($SurveyUser->id, $houseHoldId);
                } else {
                    if ($houseHoldId = $this->createdHouseHold($request->street)) {
                        $this->updateHouseHold($SurveyUser->id, $houseHoldId);
                    }
                }
            }
            return redirect("/manage-households/list")->with(['message' => 'Added Successfully', 'alert-success' => 'alert-success']);

        }
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Models\SurveyUser $SurveyUser
     * @return \Illuminate\Http\Response
     */
    public function show(SurveyUser $SurveyUser)
    {

        return view('manage-households.edit', ['surveyUser' => $SurveyUser]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\SurveyUser $SurveyUser
     * @return \Illuminate\Http\Response
     */
    public function edit(SurveyUser $SurveyUser)
    {

        $tagsRrecord = Tag::where(['survey_user_id' => $SurveyUser->id])->get();
        $titleRrecord = Note::where(['survey_user_id' => $SurveyUser->id])->get();

        return view('manage-households.edit', ['surveyUser' => $SurveyUser, 'tagsRecord' => $tagsRrecord, 'titleRrecord' => $titleRrecord]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SurveyUser $SurveyUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SurveyUser $SurveyUser)
    {

        $rules = ['phone.regex' => 'Format should be +123456789 or 123456789'];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'tags' => 'required',
            'voter_id' => 'required',  //|unique:survey_users,voter_id,' . $SurveyUser->id
            'precinct' => 'required',
            'identification' => 'required',
            'phone' => 'nullable|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/',

        ], $rules)->validate();
        if ($validator) {
            $updated = SurveyUser::where(['id' => $SurveyUser->id])->update($request->except('_token', 'tags', 'notes'));
            if ($updated) {
                $tagsIds = [];
                $noteid = [];
                foreach ($request->notes as $note1) {
                    if (!empty($note1)) {
                        $note = new Note();
                        $note->survey_user_id = $SurveyUser->id;
                        $note->text = $note1;
                        $note->userCreatedNoteID = Auth::user()->id;
                        $note->save();
                        $noteid[] = $note->id;

                    }

                }

                foreach ($request->tags as $single) {
                    $record = Tag::where(['title' => $single])->where(['survey_user_id' => $SurveyUser->id])->first();
                    if ((!$record) && (!empty($single))) {
                        $tag = new Tag();
                        $tag->survey_user_id = $SurveyUser->id;
                        $tag->title = $single;
                        $tag->userCreatedTagID = Auth::user()->id;
                        $tag->save();
                        $tagsIds[] = $tag->id;
                    }
                }
                // Tag::whereNotIn('id', $tagsIds)->delete();
                Note::whereNotIn('id', $noteid)->delete();
                if ($houseHoldId = $this->checkAddressExist($request->precinct)) {
                    $this->updateHouseHold($SurveyUser->id, $houseHoldId);
                } else {
                    if ($houseHoldId = $this->createdHouseHold($request->precinct)) {
                        $this->updateHouseHold($SurveyUser->id, $houseHoldId);
                    }
                }
            }
            return redirect("/manage-households/list/$SurveyUser->csv_data_id")->with(['message' => 'Updated Successfully', 'alert-success' => 'alert-success']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\SurveyUser $SurveyUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(SurveyUser $surveyUser)
    {
        //
    }

    public function checkVoterid($voter_id)
    {
        $v_id = SurveyUser::where([['voter_id', '=', $voter_id]])->first();

        if ($v_id) {

            return $v_id->voter_id;
        } else {
            return false;
        }
    }


    public function checkAddressExist($address)
    {

        $houseHold = HouseHold::where([['address', '=', $address]])->first();
        if ($houseHold) {
            return $houseHold->id;
        }
        return false;
    }


    public function updateHouseHold($surveyUserId, $houseHoldId)
    {
        SurveyUser::where('id', $surveyUserId)
            ->update(['house_hold_id' => $houseHoldId, 'userCreatedSurveyUserID' => Auth::user()->id]);


    }

    /*public function updateSurveymapping($voter)
    {
        $updated = SurveyUser::where(['voter_id' =>$voter])
            ->update(['precinct' =>$row[$address]]);
        // dd($updated);


    }*/

    public function createdHouseHold($address)
    {
        $record = HouseHold::orderBy('id', 'DESC')->first(['id']);
        if (!$record) {
            $lastid = 1;
        } else {
            $lastid = $record->id + 1;
        }
        $houseHold = new HouseHold();
        $intial = $houseHold->houseHoldIntials;
        $name = $intial . '-' . $lastid;
        $houseHold->name = $name;
        $houseHold->address = $address;
        $houseHold->userCreatedHouseHoldID = Auth::user()->id;
        if ($houseHold->save()) {
            return $houseHold->id;
        }
        return false;
    }

    public function getImport()
    {

        $csvList = CsvData::all();
        return view('manage-households.import', ['csvList' => $csvList]);
    }

    public function parseImport(CsvImportRequest $request)
    {
        $file = $request->file('csv_file');
        $path = $file->getRealPath();
        if ($request->has('header')) {
            $data = Excel::load($path, function ($reader) {
            })->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        if (count($data) > 0) {

            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {

                    $csv_header_fields[] = $key;

                }
            }
            $csv_data = array_slice($data, 0, 2);
            $extension = $file->getClientOriginalExtension();
            $savedPath = time() . rand() . "file." . $extension;
            $file->move('public/csv-upload/', $savedPath);

            if ($request->csvSelectList === "new") {
                $csv_data_file = CsvData::create([
                    'csv_filename' => $request->csv_name,
                    'csv_original_filename' => $request->file('csv_file')->getClientOriginalName(),
                    'csv_saved_filename' => $savedPath,
                    'csv_header' => $request->has('header'),
                    'csv_data' => json_encode($data)
                ]);
            } else {
                CsvData::where(['id' => $request->csvSelectList])->update([
                    'csv_original_filename' => $request->file('csv_file')->getClientOriginalName(),
                    'csv_saved_filename' => $savedPath,
                    'csv_data' => json_encode($data)
                ]);
                $csv_data_file = CsvData::where(['id' => $request->csvSelectList])->first();
            }

        } else {
            return redirect()->back();
        }

        return Redirect::route('surveyUser.mapping')->with(['csv_header_fields' => $csv_header_fields, 'csv_data' => $csv_data, 'csv_data_file' => $csv_data_file]);
    }

    public function mapping(Request $request)
    {

        $csv_header_fields = $request->session()->get('csv_header_fields');
        $csv_data = $request->session()->get('csv_data');
        $csv_data_file = $request->session()->get('csv_data_file');
        if (!$request->session()->get('csv_header_fields')) {
            abort(404);
        }
        return view('manage-households.import_fields', compact('csv_header_fields', 'csv_data', 'csv_data_file'));
    }

    public function processImport(Request $request)
    {
        $data = CsvData::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        $individualCount = count($csv_data);
        $houseHoldArray = [];
        /*  $start=time();*/

        foreach ($csv_data as $row) {
            $count = [];
            $surveyUser = new SurveyUser();

            foreach ($request->fields as $key => $field) {

                if (!empty($field)) {
                    if ($key == 'voter_id') {
                        $checksurveyUser = SurveyUser::where(['voter_id' => $row[$field], 'csv_data_id' => $request->csv_data_file_id])->first();
                        if ($checksurveyUser) {
                            $surveyUser = SurveyUser::find($checksurveyUser->id);
                        }
                    }
                    if ($key == 'street') {
                        $houseHoldArray[$row[$field]] = $row[$field];
                    }

                    $surveyUser->$key = $row[$field];
                    $count [] = $field;
                }
            }

            if (count($count)) {
                $surveyUser->csv_data_id = $request->csv_data_file_id;
                $dataSaved = $surveyUser->save();
                $address = $request->fields['street'];
                if ($dataSaved) {

                    if ($row[$address] == '') {

                        $row[$address] = 'N/A';
                    }

                    if ($houseHoldId = $this->checkAddressExist($row[$address])) {
                        $this->updateHouseHold($surveyUser->id, $houseHoldId);
                    } else if ($houseHoldId = $this->createdHouseHold($row[$address])) {
                        $this->updateHouseHold($surveyUser->id, $houseHoldId);
                    }
                }

            } else {
                return redirect()->route('surveyUser.mapping')->with(['message' => 'Please Select atleast One Field', 'alert-success' => 'alert-success']);
            }
        }
        /*$end = time();

        $taken = $end - $start;
        echo $taken;
        echo $end;
        echo $start;
        exit;*/

        CsvData::where(['id' => $request->csv_data_file_id])->update(['house_holds_number' => count($houseHoldArray), 'individuals_number' => $individualCount]);
        return redirect()->route('surveyUser.import')->with(['message' => 'Uploaded Successfully', 'alert-success' => 'alert-success']);
    }


    public function export(Request $request)
    {
        if (isset($request->type) && isset($request->value) && $request->value != '') {

            if ($request->type == 'house_hold_id') {

                $houseHoldId = HouseHold::where([['name', '=', $request->value]])->get(['id']);
                $surveyUsers = SurveyUser::select('id', 'name as First Name', 'last_name as Last Name', 'street as Street', 'voter_id as Voter ID', 'gender as Gender', 'age as Age', 'precinct as Precinct', 'phone as Phone', 'vote_history_1 as Vote History 1', 'vote_history_2 as Vote History 2', 'vote_history_3 as Vote History 3', 'vote_history_4 as Vote History 4', 'contact_history_1 as Contact History 1', 'contact_history_2 as Contact History 2', 'contact_history_3 as Contact History 3', 'early_voting_location as Early Voting Location', 'election_day_location as Election Day Location', 'financial_contributor as Financial Contributer', 'voted as Voted', 'extra_1 as Append 1', 'extra_2 as Append 2', 'note as Note')->whereIn('house_hold_id', $houseHoldId);

            } else {
                $surveyUsers = SurveyUser::select('id', 'name as First Name', 'last_name as Last Name', 'voter_id as Voter ID', 'gender as Gender', 'age as Age', 'precinct as Precinct', 'phone as Phone', 'vote_history_1 as Vote History 1', 'vote_history_2 as Vote History 2', 'vote_history_3 as Vote History 3', 'vote_history_4 as Vote History 4', 'contact_history_1 as Contact History 1', 'contact_history_2 as Contact History 2', 'contact_history_3 as Contact History 3', 'early_voting_location as Early Voting Location', 'election_day_location as Election Day Location', 'financial_contributor as Financial Contributer', 'voted as Voted', 'extra_1 as Append 1', 'extra_2 as Append 2', 'note as Note')->where($request->type, 'LIKE', "%$request->value%");
            }
        } else {
            $surveyUsers = SurveyUser::select('id', 'name as First Name', 'last_name as Last Name', 'voter_id as Voter ID', 'street as Street', 'city as City', 'state as State', 'gender as Gender', 'zipcode as Zipcode', 'age as Age', 'precinct as Precinct', 'phone as Phone', 'vote_history_1 as Vote History 1', 'vote_history_2 as Vote History 2', 'vote_history_3 as Vote History 3', 'vote_history_4 as Vote History 4', 'contact_history_1 as Contact History 1', 'contact_history_2 as Contact History 2', 'contact_history_3 as Contact History 3', 'early_voting_location as Early Voting Location', 'election_day_location as Election Day Location', 'financial_contributor as Financial Contributer', 'voted as Voted', 'extra_1 as Append 1', 'extra_2 as Append 2', 'note as Note');
        }
        if (isset($request->csv_data_id) && $request->csv_data_id != "") {
            $surveyUsers->where(['csv_data_id' => $request->csv_id]);
        } else {
            $surveyUsers->whereNull('csv_data_id');
        }

        $surveyUsers = $surveyUsers->get()->toArray();


        for ($i = 0; $i < count($surveyUsers); $i++) {
            $surveyUsers[$i]["Tag"] = implode(",", Tag::where(['survey_user_id' => $surveyUsers[$i]['id']])->get()->pluck('title')->toArray());
        }

        return \Excel::create('household-' . time(), function ($excel) use ($surveyUsers) {
            $excel->sheet('sheet name', function ($sheet) use ($surveyUsers) {
                $sheet->fromArray($surveyUsers);

            });

        })->download('csv');
    }

    public function FinancialContributor(Request $request)
    {
        $id = $request->id;
        $data = $request->data;
        $rec = SurveyUser::where('id', $request->id)->update([
            'financial_contributor' => $data,
        ]);

    }

    public function FinancialDelete(Request $request)
    {
        $id = $request->id;
        $data = $request->data;
        $delete = SurveyUser::where('id', $request->id)->update(['financial_contributor' => $data,]);
    }

    public function Support(Request $request)
    {
        SurveyUser::where('id', $request->id)->update([
            'Identification' => "Support",

        ]);
    }

    public function supportDelete(Request $request)
    {
        $supportDelete = SurveyUser::where('id', $request->id)->update([
            'Identification' => '',
        ]);
    }

    public function notesDelete(Request $request)
    {

        $supportDelete = Note::where('id', $request->id)->delete();
        if ($supportDelete) {
            return "success";
        } else {
            return "error";
        }
    }

    public function FinancialContributorGet(Request $request)
    {

        $data = $request->data;

        $role = SurveyUser::select('financial_contributor')->where('financial_contributor', $data)->get();

    }

    /**
     **@param tagTitle
     **@param surveyUserID
     **@return success error message
     **/
    public function deleteTag(Request $request)
    {

        $record = Tag::where(['title' => $request->tagTitle])->where(['survey_user_id' => $request->surveyUserID])->first()->delete();
        if ($record) {
            return "success";
        } else {
            return "error";
        }
    }

    public function csvList()
    {

        $csvData = CsvData::orderBy('id', "DESC")->paginate(20);

        return view('manage-households.csv-list', ['csvData' => $csvData]);
    }

    public function csvListnew()
    {

        $csvData = CsvData::orderBy('id', "DESC")->paginate(20);

        return view('manage-households.csvnew-list', ['csvData' => $csvData]);
    }


    public function excludeDataGet(Request $request)
    {
        try {
            if ($request->csvID) {
                $excludeData = SurveyUser::whereIn('csv_data_id', $request->csvID)->get();
                $zipcode = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('zipcode')->get();
                $city = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('city')->get();
                $country = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('country')->get();
                $gender = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('gender')->get();
                $age = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('age')->get();
                $ethencity = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('ethencity')->get();
                $precinct = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('precinct')->get();
                $tag = SurveyUser::whereIn('csv_data_id', $request->csvID)->groupBy('tag')->get();
                $data = array(
                    'excludeData' => $excludeData,
                    'zipcode' => $zipcode,
                    'gender' => $gender,
                    'country' => $country,
                    'city' => $city,
                    'age' => $age,
                    'ethencity' => $ethencity,
                    'precinct' => $precinct,
                    'tag' => $tag,
                );
                return $data;
            } else {
                return "Error";
            }

        } catch (\Exception $e) {
            $error = $e->getMessage();
            return "please select checkbox";
        }

    }

    public function excludeData(Request $request)
    {
        $csv_id = explode(",", $request->csv_id);
        if ($request->type == "zipcode") {
            if ($request->zipcodeRemove) {

                $zipcode = SurveyUser::whereIn('zipcode', $request->zipcodeRemove)->whereIn('csv_data_id', $csv_id)->get();
                $individual = $zipcode->count();
                $data = [];
                foreach ($zipcode as $key => $householde) {
                    if (!in_array($householde['street'], $data)) {
                        array_push($data, $householde['street']);
                    }

                }
                $households = count($data);
                $live = array('households' => $households,
                    'individual' => $individual,
                );

            }
        }
        if ($request->type == "city") {
            if ($request->city) {

                $city = SurveyUser::whereIn('city', $request->city)->whereIn('csv_data_id', $csv_id)->get();
                $individual = $city->count();
                $data = [];
                foreach ($city as $key => $householde) {
                    if (!in_array($householde['street'], $data)) {
                        array_push($data, $householde['street']);
                    }

                }
                $households = count($data);
                $live = array('households' => $households,
                    'individual' => $individual,
                );

            }
        }
        if (isset($live)) {

            return $live;
        } else {

            $live = array('households' => 0,
                'individual' => 0,
            );

            return $live;
        }


    }

    public function createcsvList()
    {
        $csvData = CsvData::orderBy('id', "DESC")->paginate(20);

        return view('manage-households.create-walkbook-list', ['csvData' => $csvData]);
    }

    public function excludeDataMap(Request $request)
    {
        $csv_id = explode(",", $request->csv_id);
        if ($csv_id) {
            $data1 = SurveyUser::whereIn('id', $csv_id)->get();
        }
        if ($request->type == "zipcode") {
            if ($request->zipcodeData) {
                $zipcode = SurveyUser::whereIn('zipcode', $request->zipcodeData)->whereIn('csv_data_id', $csv_id)->get();

                $individual = $zipcode->count();
                $live = [];
                foreach ($zipcode as $key => $householde) {
                    if (!in_array($householde['street'], $live)) {
                        array_push($live, $householde['street']);
                    }

                }
                $households = count($live);
                $data = array('households' => $households,
                    'individual' => $individual,
                    'data' => $zipcode
                );
            }
        }

        if (isset($data)) {
            return $data;
        } else {

            $data = array('households' => 0,
                'individual' => 0,
            );

            return $data;
        }

    }

    public function includeDataMap(Request $request)
    {
        $csv_id = explode(",", $request->csv_id);
        if ($request->type == "zipcode") {
            if ($request->zipcodeData) {
                $zipcode = SurveyUser::whereIn('zipcode', $request->zipcodeData)->whereIn('csv_data_id', $csv_id)->get();

                $individual = $zipcode->count();
                $live = [];
                foreach ($zipcode as $key => $householde) {
                    if (!in_array($householde['street'], $live)) {
                        array_push($live, $householde['street']);
                    }

                }
                $households = count($live);
                $data = array('households' => $households,
                    'individual' => $individual,
                    'data' => $zipcode
                );
            }
        }

        if (isset($data)) {
            return $data;
        } else {

            $data = array('households' => 0,
                'individual' => 0,
            );

            return $data;
        }

    }

    public function excludeNext(Request $request)
    {
        $household = $request->household;
        $individual = $request->individual;
        $CsvData = explode(",", $request->rawId);
        //$canvassers = User::role('canvasser')->where('status',1)->get();
        if ($request->type == "zipcode") {
            if ($request->zipcodeData) {
                $data = SurveyUser::select('id')->whereIn('zipcode', $request->zipcodeData)->whereIn('csv_data_id', $CsvData)->get()->toArray();
            }

        }

        if (isset($data)) {
            $urveyUser_id = [];
            foreach ($data as $key => $id) {
                array_push($urveyUser_id, $id['id']);
            }
            $secure = implode(",", $urveyUser_id);
            try {
                foreach ($CsvData as $key => $value) {
                    $saveData = new SaveData();
                    $saveData->surveyUser_id = $urveyUser_id;
                    $saveData->surveyUser_id_secure = md5($secure);
                    $saveData->household = $household;
                    $saveData->individual = $individual;
                    $saveData->Csv_id = $value;
                    $saveData->save();
                }
            } catch (\Exception $e) {
                $error = $e->getMessage();
                return "Error";
            }

            $excludeSave = SaveData::whereIn('Csv_id', $CsvData)->get();
        }
        return response()->json([
            'view' => view('manage-households.csv-nextStep', compact('excludeSave', 'household', 'individual', 'CsvData'))->render()]);

    }

    public function saveStep(Request $request)
    {
        try {
            $data = SaveData::where('id', $request->rawId)
                ->update(['title' => $request->walkbookName]);
            return $request->walkbookName;
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return "Error";
        }
    }

    public function nextStep(Request $request)
    {
        if ($request->cid) {
            $csvID = explode(",", $request->cid);
            $canvassers = User::role('canvasser')->where('status', 1)->get();
            $data = SaveData::with('AssignWalkbook', 'AssignWalkbook.CanvessarUser')->whereIn('Csv_id', $csvID)->get();

            return view('manage-households.nextStep', compact('canvassers', 'data', 'csvID'));
        }
        if ($request->csvId) {
            $csvID = explode(",", $request->cid);
            $canvassers = User::role('canvasser')->where('status', 1)->get();
            $data = SaveData::with('AssignWalkbook', 'AssignWalkbook.CanvessarUser')->whereIn('Csv_id', $request->csvId)->get();

            return view('manage-households.nextStep', compact('canvassers', 'data', 'csvID'));
        }

        return redirect('manage-households/createcsv-list/')->with(['message' => 'Select the any checkbox', 'alert-danger' => 'alert-danger']);
    }

    public function finalStep(Request $request)
    {
        $id = $request->csvID;
        foreach ($request->surveyUserIds as $excludeDataId) {
            foreach ($request->canvessar_id as $key => $canvessar_id) {
                $assign = new AssignPolygonWalkbook();
                $assign->canvessar_id = $canvessar_id;
                $assign->exclude_save_id = $excludeDataId;
                $assign->save();
            }
        }

        return redirect('manage-households/createcsv-list/' . $id)->with(['message' => 'Canvessar successfully assign', 'alert-success' => 'alert-success']);
    }

    public function canvessaList(Request $request)
    {
        $canvessa = AssignPolygonWalkbook::with('CanvessarUser')->groupBy('canvessar_id')->get();

        if ($request->canvessar_id) {
            $SurveyUser = AssignPolygonWalkbook::with('SaveData')->where('canvessar_id', $request->canvessar_id)->groupBy('canvessar_id', 'exclude_save_id')->get();
            $surveyId = [];
            $surveyuserId = [];
            if ($SurveyUser) {
                foreach ($SurveyUser as $key => $User) {
                    foreach ($User->SaveData as $key => $value) {
                        array_push($surveyId, $value->surveyUser_id);
                    }
                }
                foreach ($surveyId as $key2 => $data) {
                    foreach ($data as $key => $value) {
                        if (!in_array($value, $surveyuserId)) {
                            array_push($surveyuserId, $value);
                        }
                    }
                }
                $data = SurveyUser::whereIn('id', $surveyuserId)->get();
            }

        }

        return view('manage-households.assign-canvessa', compact('canvessa', 'data'));
    }

    public function testingmap()
    {
        $zip = 284135;
        $data = Address::getzipcode($zip);
        dd($data);
    }


}
