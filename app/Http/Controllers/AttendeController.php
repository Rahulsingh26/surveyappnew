<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\Answer;
use App\Models\Event;
use App\Models\Attende;
use App\Models\EventAttende;
use App\Http\Requests;
use Validator;
use Mail;
use Excel;
use App\Mail\AttendeEventApprovel;
use DB;


class AttendeController extends Controller
{
	/**
	**
	**@return attende list to the blade file
	**/
	public function index(Request $request) {
		//for exception handling use try catch
		try{

			$record = DB::table('event_attendes')->where(['attendeEventEntryPoint' => 0])->distinct('attendeID')->get()->pluck('attendeID');

			$query = DB::table('attendes')->whereNotIn('id',$record);

	    	//$query = Attende::orderBy('id','desc')->where(['attendeAssiginedEventStatus' => 0]);
	    	if ($request->attendeName) {
	    		$query->where('attendeName','like','%'.$request->attendeName.'%');
	    	}
	    	if ($request->attendeAddress) {
	    		$query->where('attendeAddress','like','%'.$request->eventAddress.'%');
	    	}
	    	if ($request->attendePhoneNo) {
	    		$query->where(['attendePhoneNo' => '%'.$request->attendePhoneNo.'%']);
	    	}
	    	if ($request->attendeEmailAddress) {
	    		$query->where(['attendeEmailAddress' => '%'.$request->attendeEmailAddress.'%']);
	    	}

	    	$attendes = $query->paginate(10);
	    	$attendes->appends(request()->all())->render();
			return view('attende.listAttende',compact("attendes"));

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**
	**@return event whose endDate is greater than yesterday
	**/
	public static function getEventByDate(){
		$todayDate = date("Y-m-d", strtotime("-1 day"));
		$eventObjs = Event::where(['eventStatus' => 1])->where('eventEndDate', '>' ,$todayDate)->get();
		return $eventObjs;
	}

	/**
	**@param attende ID and event ID userEmail
	**@return eventAttende added and attende status updated
	**/
	public static function addEventAttende($eventIDs,$attendeID,$userEmail){
		if ($eventIDs) {

			foreach ($eventIDs as $eventId) {
				$eventAttende = EventAttende::where(['eventID' => $eventId])->where(['attendeID' => $attendeID])->first();

				if (!$eventAttende) {

				$random = rand(pow(10,7), pow(10,8));

				$link = $random;

				$eventAttendeObj = new EventAttende;
				$eventAttendeObj->eventID = $eventId;
				$eventAttendeObj->attendeID = $attendeID;
				$eventAttendeObj->attendeToken = $random;
				$eventAttendeObj->eventAttendeCreatedByUserID = Auth::user()->id;
				$eventAttendeObj->save();

				$event = Event::where(['id' => $eventId])->first();
                 //Email send to attende
               	Mail::to($userEmail)->send(new AttendeEventApprovel($attendeID,$link,$eventId,$event->eventName));

				$attendeObj = Attende::where(['id' => $attendeID])->first();
				$attendeObj->attendeAssiginedEventStatus = 1;

				$attendeObj->save();

				return "created successfully";

				}

			}
		}
	}

	/**
	**@param attendeObject,attendeName,attendeAddress,attendePhoneNo
	**@return attendeId
	**/
	public static function addSingleAttende($attendeObj,$attendeName,$attendeAddress,$attendePhoneNo,$attendeEmailAddress){

		$attendeObj->attendeName = $attendeName;
		$attendeObj->attendeAddress = $attendeAddress;
		$attendeObj->attendePhoneNo = $attendePhoneNo;
		$attendeObj->attendeEmailAddress = $attendeEmailAddress;
		$attendeObj->attendeCreatedByUserID = Auth::user()->id;
		$record = $attendeObj->save();

		return $attendeObj->id;
	}

	/**
	**
	**@return form to insert event
	**/
	public function create() {
		//for exception handling use try catch
		try{

		$eventObjs = self::getEventByDate();
		$todayDate = date("Y-m-d", strtotime("-1 day"));
		$eventObjs = Event::where(['eventStatus' => 1])->where('eventEndDate', '>' ,$todayDate)->get();
		return view('attende.addAttende',compact("eventObjs"));

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param request body of form
	**@return success message if data added successfully else error message
	**/
	public function addAttende(Request $request) {

		//for exception handling use try catch
		try{

			$validator = Validator::make($request->all(), [
	            'attendeName' => 'required',
	            'attendeAddress' => 'required',
	            'attendePhoneNo' => 'required|regex:/^\+?\d+$/|digits_between:8,16|numeric',
	            'attendeEmailAddress' => 'required|email'
	        ],[
	        	'attendeName.required' => 'Attende name is required',
	        	'attendeAddress.required' => 'Attende address is required',
	        	'attendePhoneNo.required' => 'Attende phone number is required',
	        	'attendeEmailAddress.required' => 'Attende email is required',
	        	'attendeEmailAddress.email' => 'Attende email must be valid',
	        	'attendePhoneNo.digits_between' => 'Attende minimum 8 to 16 digit'
	        ]);

	        if ($validator->fails()) {

	            return redirect('attende/add')
	                        ->withErrors($validator)
	                        ->withInput();
	        }

			$attendeObj = new Attende();
			$userEmail = $request->attendeEmailAddress;
			$attendeObjID = self::addSingleAttende($attendeObj,$request->attendeName,$request->attendeAddress,$request->attendePhoneNo,$request->attendeEmailAddress);

			if ($attendeObjID) {

				self::addEventAttende($request->eventID,$attendeObjID,$userEmail);

				return redirect()->route('list.attende')->with(['message' => 'Attende added Successfully','alert-success'=>'alert-success']);
			} else {
				return redirect()->route('list.attende')->with(['message' => 'Some issue occour']);
			}

		} catch (\Exception $e) {

            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}


	/**
	**@param event id,current status
	**@return error or success message
	**/
	public function statusUpadte($id,$statusid){
		//for exception handling use try catch
		try{

			$attendeObj = Attende::where(['id' => $id])->first();
			if ($attendeObj) {
				if ($statusid == 1) {
					$attendeObj->attendeStatus = 0;
				} else {
					$attendeObj->attendeStatus = 1;
				}
				$data = $attendeObj->save();
				if ($data) {
					return redirect()->route('list.attende')->with(['message' => 'Attende status updated Successfully','alert-success'=>'alert-success']);
				} else {
					return redirect()->route('list.attende')->with(['message' => 'Attende status not updated','alert-error'=>'alert-error']);
				}
			} else {
				return redirect()->route('list.attende')->with(['message' => 'Attende status not updated','alert-error'=>'alert-error']);
			}

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}


	/**
	**@param id of attende
	**@return error or success message when attende is deleted
	**/
	public function deleteAttende($id){
		//for exception handling use try catch
		try{

			$attendeObj = Attende::where(['id' => $id])->first();
			if ($attendeObj) {
				$data = Attende::where(['id' => $id])->first()->delete();
				if ($data) {
					return redirect()->route('list.attende')->with(['message' => 'Attende deleted Successfully','alert-success'=>'alert-success']);
				} else {
					return redirect()->route('list.attende')->with(['message' => 'Attende not deleted','alert-error'=>'alert-error']);
				}
			} else {
				return redirect()->route('list.attende')->with(['message' => 'Attende not deleted','alert-error'=>'alert-error']);
			}

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param id of attende
	**@return collection of attende if succes else it shows error message
	**/
	public function editAttende($id){
		//for exception handling use try catch
		try{

			$event = Event::select('event.id')
				->join('event_attendes',function($join) use($id){
					$join->on('event_attendes.eventID','=','event.id');
				})
				->join('attendes', 'attendes.id', '=', 'event_attendes.attendeID')
	            ->where('event_attendes.attendeID',$id)->get()->pluck('id')->toArray();

			$todayDate = date("Y-m-d", strtotime("-1 day"));
			$eventObjs = Event::where(['eventStatus' => 1])->get();
			//dd($eventObjs);
			$attendeObj = Attende::where(['id' => $id])->first();
			//dd($attendeObj);
			if ($attendeObj) {
				return view('attende.editAttende',compact("attendeObj","eventObjs","event"));
			} else {
				return redirect()->route('list.attende')->with(['message' => 'Some issue occour']);
			}

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param event object
	**@return if object updated successfully success message else error message
	**/
	public function updateAttende(Request $request) {

		//for exception handling use try catch
		try{

			$validator = Validator::make($request->all(), [
	            'attendeName' => 'required',
	            'attendeAddress' => 'required',
                'attendePhoneNo' => 'required|regex:/^\+?\d+$/|digits_between:8,16|numeric',
	            'attendeEmailAddress' => 'required|email'
	        ],[
	        	'attendeName.required' => 'Attende name is required',
	        	'attendeAddress.required' => 'Attende address is required',
	        	'attendePhoneNo.required' => 'Attende phone number is required',
                'attendePhoneNo.digits_between' => 'Attende minimum 8 to 16 digit',
	        	'attendeEmailAddress.required' => 'Attende email is required',
	        	'attendeEmailAddress.email' => 'Attende email must be valid'
	        ]);
	        //dd($request->eventID);

	        if ($validator->fails()) {

	            return redirect('attende/attende-edit/'.$request->attendeID)
	                        ->withErrors($validator)
	                        ->withInput();
	        }

			$attendeObj = Attende::where(['id' => $request->attendeID])->first();

			if ($attendeObj) {
				$userEmail = $request->attendeEmailAddress;
				$attendeObjID = self::addSingleAttende($attendeObj,$request->attendeName,$request->attendeAddress,$request->attendePhoneNo,$request->attendeEmailAddress);

				if ($attendeObjID) {

					self::addEventAttende($request->eventID,$attendeObjID,$userEmail);

					return redirect()->route('list.attende')->with(['message' => 'Attende updated Successfully','alert-success'=>'alert-success']);
				} else {
					return redirect()->route('list.attende')->with(['message' => 'Some issue occour']);
				}
			} else {
				return redirect()->route('list.attende')->with(['message' => 'Some issue occour']);
			}


		} catch (\Exception $e) {

            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}


	/**
	**@param csv file
	**@return success or error message based on attende record stored on db
	**/
	public function addAttendeByCSV(Request $request) {
		//for exception handling use try catch
		try{

		return view('attende.addAttendeCSV');

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param attendeName attendeAddress attendePhnoNo
	**@return success or error message
	**/
	public function addAttendeCsvRecord(Request $request) {
		//for exception handling use try catch
		try{

		$validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,txt'
        ]);

        if ($validator->fails()) {

            return redirect('attende/attende-csv-form')
                        ->withErrors($validator)
                        ->withInput();
        }

		if ($_FILES['file']['name']) {
			$filename = explode(".",$_FILES['file']['name']);
			if ($filename['1'] == 'csv') {
				$path = $request->file('file')->getRealPath();
				$data = Excel::load($path)->get();
				if($data->count() > 0) {
					foreach($data->toArray() as $key => $row) {
						//dd($row);
						$attendeObj = new Attende();
						$attendeObjID = self::addSingleAttende($attendeObj,$row['attendename'],$row['attendeaddress'],$row['attendephoneno'],$row['attendeemailaddress']);
					}
					return redirect()->route('list.attende')->with(['message' => 'Attende updated Successfully','alert-success'=>'alert-success']);

				} else {
					return redirect()->route('list.attende')->with(['message' => 'Some issue occour']);
				}

			}
		}

		} catch (\Exception $e) {
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}
}
