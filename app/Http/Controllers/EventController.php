<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\Answer;
use App\Models\Event;
use App\Models\Attende;
use App\Models\EventAttende;
use App\Http\Requests;
use Validator;
use DB;
use App\Helpers\EventAttendHelper;
use App\Http\Controllers\EventAttendeController;


class EventController extends Controller
{

	/**
	**
	**@return events list to the blade file
	**/
	public function index(Request $request) {
		//for exception handling use try catch
		try{

    	$query = Event::orderBy('id','desc');
    	if ($request->eventName) {
    		$query->where('eventName','like','%'.$request->eventName.'%');
    	}
    	if ($request->eventAddress) {
    		$query->where('eventAddress','like','%'.$request->eventAddress.'%');
    	}
    	if ($request->eventStartDate) {
    		$query->where(['eventStartDate' => $request->eventStartDate]);
    	}
    	if ($request->eventEndDate) {
    		$query->where(['eventEndDate' => $request->eventEndDate]);
    	}
    	$events = $query->paginate(10);
    	$events->appends(request()->all())->render();
		
		return view('event.listEvent',compact('events'));

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param eventObject,eventName,eventStartDate,eventEndDate,eventAddress
	**@return success or error
	**/
	public static function addSingleEvent($eventObj,$eventName,$eventStartDate,$eventEndDate,$eventAddress,$eventStartTime,$eventEndTime){
		$eventObj->eventName = $eventName;
		$eventObj->eventStartDate = $eventStartDate;
		$eventObj->eventEndDate = $eventEndDate;
		$eventObj->eventAddress = $eventAddress;
		$eventObj->eventStartTime = $eventStartTime;
		$eventObj->eventEndTime = $eventEndTime;
		$eventObj->eventCreatedByUserID = Auth::user()->id;
		$record = $eventObj->save();
		return $record;
	}

	/**
	**
	**@return form to insert event
	**/
	public function create() {
		return view('event.addEvent');
	}

	/**
	**@param request body of form
	**@return success message if data added successfully else error message
	**/
	public function addEvent(Request $request) {
		//for exception handling use try catch
		try{ 

			$validator = Validator::make($request->all(), [
	            'eventName' => 'required|unique:event',
	            'eventStartDate' => 'required',
	            'eventEndDate' => 'required',
	            'eventAddress' => 'required',
	            'eventStartTime' => 'required',
	            'eventEndTime' => 'required'
	        ],[
	        	'eventName.required' => 'Event name is required',
	        	'eventStartDate.required' => 'Event start date is required',
	        	'eventEndDate.required' => 'Event end date is required',
	        	'eventAddress.required' => 'Event address is required',
	        	'eventName.unique' => 'Event name must be unique',
	        	'eventStartTime.required' => 'Event start time is required',
	        	'eventEndTime.required' => 'Event end time is required'
	        ]);

	        if ($validator->fails()) {

	            return redirect('event/add')
	                        ->withErrors($validator)
	                        ->withInput();
	        }
	        //dd($request->eventStartDate,$request->eventEndDate);
			$eventObj = new Event();
			$record = self::addSingleEvent($eventObj,$request->eventName,$request->eventStartDate,$request->eventEndDate,$request->eventAddress,$request->eventStartTime,$request->eventEndTime);
			
			if ($record) {
				return redirect()->route('list.event')->with(['message' => 'Event added Successfully','alert-success'=>'alert-success']);
			} else {
				return redirect()->route('list.create')->with(['message' => 'Some issue occour']);
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}


	/**
	**@param event id,current status
	**@return error or success message
	**/
	public function statusUpadte($id,$statusid){
		//for exception handling use try catch
		try{

			$eventObj = Event::where(['id' => $id])->first();
			if ($eventObj) {
				if ($statusid == 1) {
					$eventObj->eventStatus = 0;
				} else {
					$eventObj->eventStatus = 1;
				}
				$data = $eventObj->save();
				if ($data) {
					return redirect()->route('list.event')->with(['message' => 'Event status updated Successfully','alert-success'=>'alert-success']);
				} else {
					return redirect()->route('list.event')->with(['message' => 'Event status not updated','alert-error'=>'alert-error']);
				}
			} else {
				return redirect()->route('list.event')->with(['message' => 'Event status not updated','alert-error'=>'alert-error']);
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}


	/**
	**@param id of event
	**@return error or success message when event is deleted
	**/
	public function deleteEvent($id){
		//for exception handling use try catch
		try{

			$date = date('Y-m-d');
			$event = Event::where('eventEndDate','<=',$date)->where(['id' => $id])->first();
			//dd($event);
			if ($event) {
				return redirect()->route('list.event')->with(['message' => 'Event not deleted because cureent date exceeds the end date','alert-error'=>'alert-error']);
			} else{
			$eventObj = Event::where(['id' => $id])->first();
			if ($eventObj) {
				$data = Event::where(['id' => $id])->first()->delete();
				if ($data) {
					return redirect()->route('list.event')->with(['message' => 'Event deleted Successfully','alert-success'=>'alert-success']);
				} else {
					return redirect()->route('list.event')->with(['message' => 'Event not deleted','alert-error'=>'alert-error']);
				}
			} else {
				return redirect()->route('list.event')->with(['message' => 'Event not deleted','alert-error'=>'alert-error']);
			}
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param id of event
	**@return collection of event if succes else it shows error message
	**/
	public function editEvent($id){
		//for exception handling use try catch
		try{
			$eventObj = Event::where(['id' => $id])->first();
			if ($eventObj) {
				return view('event.editEvent',compact("eventObj"));
			} else {
				return redirect()->route('list.event')->with(['message' => 'Some issue occour']);	
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param event object
	**@return if object updated successfully success message else error message
	**/
	public function updateEvent(Request $request) {
		//for exception handling use try catch
		try{

			$validator = Validator::make($request->all(), [
	            'eventName' => 'required|unique:event,eventName,'.$request->eventID,
	            'eventStartDate' => 'required',
	            'eventEndDate' => 'required',
	            'eventAddress' => 'required'
	        ]);
	        //dd($request->eventID);

	        if ($validator->fails()) {

	            return redirect('event/event-edit/'.$request->eventID)
	                        ->withErrors($validator)
	                        ->withInput();
	        }
	        
			$eventObj = Event::where(['id' => $request->eventID])->first();
			
			if ($eventObj) {

				$record = self::addSingleEvent($eventObj,$request->eventName,$request->eventStartDate,$request->eventEndDate,$request->eventAddress,$request->eventStartTime,$request->eventEndTime);

				if ($record) {
					return redirect()->route('list.event')->with(['message' => 'Event updated Successfully','alert-success'=>'alert-success']);
				} else {
					return redirect()->route('list.event')->with(['message' => 'Some issue occour']);
				}
			} else {
				return redirect()->route('list.event')->with(['message' => 'Some issue occour']);
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}

	/**
	**@param eventID
	**@return list of attende
	**/
	public function eventAttendeList(Request $request,$eventID){
		//for exception handling use try catch
		try{
	
		$query = Attende::select('attendes.*','event_attendes.*' ,'event.eventName')
				->join('event_attendes',function($join) use($eventID){
					$join->on('event_attendes.attendeID','=','attendes.id');
				})		
				->join('event', 'event.id', '=', 'event_attendes.eventID')
	            ->where('event_attendes.eventID',$eventID);

	            if ($request->attendeName) {
	            	$query->where('attendes.attendeName','like','%'.$request->attendeName.'%');
	            }

	            if ($request->attendeEmailAddress) {
	            	$query->where('attendes.attendeEmailAddress','like','%'.$request->attendeEmailAddress.'%');
	            }

	            if ($request->attendeAddress) {
	            	$query->where('attendes.attendeAddress','like','%'.$request->attendeAddress.'%');
	            }
	            if ($request->attendePhoneNo) {
	            	$query->where('attendes.attendePhoneNo','like','%'.$request->attendePhoneNo.'%');
	            }
	          
	        
	        $attendesRecords = $query->paginate(10);
	    	$attendesRecords->appends(request()->all())->render();

	    	return view('event.listEventAttende',compact("attendesRecords"));

    	} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }

	}

	/**
	**@param eventID,attendeID
	**@return error or success message
	**/
	public function deleteAttendeEventConnection(Request $request){
		//for exception handling use try catch
		try{
			$eventID = $request->eventID;
			$attendeID = $request->attendeID;
			$result = EventAttendeController::deleteAttendeEvent($eventID,$attendeID);
			if ($result == "success") {
				return "success";
			} else {
				return "error";
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}


	/**
	**@param $token $userID $status $eventID 
	**@return error or success message
	**/
	public function changeAttendeEventStatus($token,$userID,$status,$eventID){
		$record = EventAttendeController::changeAttendeStatus($token,$userID,$status,$eventID);
		//for exception handling use try catch
		try{ 

			if ($record == "success") {
				return "success";
			} else {
				return "error";
			}

		} catch (\Exception $e) { 
            //$e defines exception object
            $error = $e->getMessage();

            //return error to the blade file
            return view('exceptionHandling.error',compact('error'));
        }
	}
}