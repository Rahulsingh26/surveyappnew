<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\AssignWalkbook;
use App\Models\Survey;
use DB;
use Auth;
use Carbon\Carbon;
use DateTime;
use App\Models\SurveyUser;
use App\Models\SurveyUserWalkbook;
use App\Models\Walkbook;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $surveyUsers = SurveyUser::all();
        $canvasserid = Answer::groupBy('canvasser_id')
            ->get();
        return view('dashboard', compact('surveyUsers','canvasserid'));
    }

    public function selectePrecinct(Request $request){
        $surveyUsers = SurveyUserWalkbook::with('surveyUser')->where('survey_user_id',$request->precinctlistId)->first();
        if (isset($surveyUsers)){
            $surveyList = AssignWalkbook::with('survey')->whereHas('survey.questions', function($q){
                $q->whereIn('question_type', [3, 4]);
            })->where('walkbook_id',$surveyUsers['walkbook_id'])->first();
            $data= array(
                "survey"=>$surveyList->toArray(),
                "surveyUser"=> $surveyUsers->toArray());
           return $data;
        }
        else{
            return "list not found";
        }
    }

    public function getGraphData(Request $request){

//        $surveyUsers = SurveyUser::with('survey');
        $returnData = [];
        $answer = Answer::where(['status' => 1, 'survey_id' => $request->surveyid, 'question_id' => $request->quesId])->get();

        $data=[];
        foreach ($answer as $key=> $result){
            if($request->observations){
                foreach($request->observations as $key2=> $re) {
                    if (in_array($re, $result->additional_observation)) {

                          if(!array_key_exists($re,$data)) {
                              $data[$re] = 1;
                          }
                          else{
                              $data[$re]=++$data[$re];
                          }
                    }
                }
            }
        }

        return [
            'labels' => array_keys($data),
            'data' => array_values($data)
        ];
    }

    public function filterCanvasser(Request $request)
    {
        // dd($request->canvasser_id);
        
        /*$query = Answer::select('id',DB::raw('DATE(created_at) as Date'),DB::raw('YEAR(created_at) year, MONTH(created_at) month'));
        
            if($request->DMY == 1) {
                $query->whereRaw('DATE(created_at) > DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                $query->whereYear('created_at', '=', Carbon::now()->year);

            }
            elseif ($request->DMY == 2){

                $query->whereYear('created_at', '=', Carbon::now()->year);
                //$query->whereMonth('created_at', Carbon::now()->month);
            }
            elseif ($request->DMY == 3){
                //$query->whereYear('created_at', '=', Carbon::now()->year);
            }
            $ids = $request->canvasser_id;
            $query->whereIn('canvasser_id',$ids);
            $query->where('status', 1);
            $query->groupBy('canvasser_id','survey_user_id');
            $query->orderBY('created_at');
            $data= $query->get();


         // dd($data->toArray());
        if($request->DMY == 1) {
            $array_data = [];
            if ($data) {
                foreach ($data as $key => $countDate) {
                    if (!array_key_exists($countDate->Date, $array_data)) {
                        $array_data[$countDate->Date] = 1;
                    } else {
                        $array_data[$countDate->Date] = ++$array_data[$countDate->Date];
                    }
                }
            }
            $weekName = [];
            foreach (array_keys($array_data) as $weekget) {
                $date = new DateTime($weekget);
                $week = $date->format("D");
                if (!in_array($week, $weekName)) {
                    $weekName[] = $week;
                }

            }
            return [
                'labels' => $weekName,
                'data' => array_values($array_data)
            ];
        }

        elseif ($request->DMY == 2){
            $array_data = [];
            if ($data) {
                foreach ($data as $key => $countDate) {
                    if (!array_key_exists($countDate->month, $array_data)) {
                        $array_data[$countDate->month] = 1;
                    } else {
                        $array_data[$countDate->month] = ++$array_data[$countDate->month];
                    }
                }
            }
            //dd($array_data);
            $weekName = [];
            foreach (array_keys($array_data) as $weekget) {
                $dateObj   = DateTime::createFromFormat('!m', $weekget);
                $monthName = $dateObj->format('F');
                if (!in_array($monthName, $weekName)) {
                    $weekName[] = $monthName;
                }

            }
            return [
                'labels' => $weekName,
                'data' => array_values($array_data)
            ];

        }
        elseif ($request->DMY == 3){
            $array_data = [];
            if ($data) {
                foreach ($data as $key => $countDate) {
                    if (!array_key_exists($countDate->year, $array_data)) {
                        $array_data[$countDate->year] = 1;
                    } else {
                        $array_data[$countDate->year] = ++$array_data[$countDate->year];
                    }
                }
            }
            //dd($array_data);
            $weekName = [];
            foreach (array_keys($array_data) as $weekget) {

                if (!in_array($weekget, $weekName)) {
                    $weekName[] = $weekget;
                }

            }
            return [
                'labels' => $weekName,
                'data' => array_values($array_data)
            ];
        }*/

        $dateRange = explode("-", $request->dateRange);
        $startDate = date('Y-m-d', strtotime($dateRange[0]));
        // $startDate = '2019-12-03';
        $endDateDate = date('Y-m-d', strtotime($dateRange[1]));
        // $endDateDate = '2019-12-04';
        $ids = implode(",", $request->canvasser_id);
        $sql =  "select users.id, users.name from users LEFT JOIN answer ON users.id = answer.canvasser_id WHERE `answer`.`status` = 1 AND users.id IN ({$ids}) AND answer.created_at >= '{$startDate}' AND answer.created_at <= '{$endDateDate}' group by `answer`.`canvasser_id`, `answer`.`survey_user_id` ORDER BY `answer`.`canvasser_id` ASC";
        $answers = DB::select($sql);
        // $ids = implode(",", $request->canvasser_id);
        // $answers = DB::select("select users.id, users.name from users LEFT JOIN answer ON users.id = answer.canvasser_id WHERE `answer`.`status` = 1 AND users.id IN (".$ids.") AND answer.created_at >= '2019-12-03' AND answer.created_at <= '2019-12-04' group by `answer`.`canvasser_id`, `answer`.`survey_user_id` ORDER BY `answer`.`canvasser_id` ASC");
        $array_data = [];
        $canvasser_id_arr = [];
        $data = array_count_values(array_column($answers, 'name'));

        $data_array = [];
        // for($i=0; $i < count($data); $i++){
        //     $data_array['labels'] = 'Rahul';
        //     $data_array['data'] = 10;
        // }
        foreach ($data as $key => $d) {
            // $data_array['labels'][] = $d;
            $data_array['labels'][] = $key;
            $data_array['data'][] = $d;
            // print_r($d);
        }
        // dd($data_array);
        $data = [
                'labels' => ['Rahul', 'Mayur'],
                'data' => [10,20]
            ];
        return $data_array;
        // dd($data);

    }

    public function filterCanvasserIdentification(){
        $data = SurveyUser::select('Identification')->get();
        $array_data = [];
        if ($data) {
            foreach ($data as $key => $identification) {
                if (!array_key_exists($identification->Identification, $array_data)) {
                    $array_data[$identification->Identification] = 1;
                }
                else {
                    $array_data[$identification->Identification] = ++$array_data[$identification->Identification];
                }
            }
            //dd($array_data);
        }
        return [
            'labels' => array_keys($array_data),
            'data' => array_values($array_data)
        ];
    }

    public function getSurveyResultData(){
        //$answer = Answer::groupBy('canvasser_id','survey_user_id')->get();
        $answer = Answer::select(DB::raw('count(*) as total,answer,status'))
            ->from(DB::raw('(SELECT * FROM answer ORDER BY id DESC) a'))
            ->groupBy('a.canvasser_id','a.survey_user_id')
            ->get();
        $array_data = [];
        foreach ($answer as $key => $result) {
            if(trim($result->answer!="")) {
                if($result->status) {
                    $survey_option = "Complete";
                } else {
                   $survey_option = $result->answer;
                }
                if (!array_key_exists($survey_option, $array_data)) {
                    $array_data[$survey_option] = 1;
                }
                else {
                    $array_data[$survey_option] = ++$array_data[$survey_option];
                }
            }
        }

       $per = [];
        foreach (array_values($array_data) as  $value) {
            $per[] = round($value / array_sum($array_data) * 100 );
           
        }
        
        
        //dd(array_sum($array_data));
        
        return [
            'labels' => array_keys($array_data),
            'data' => array_values($per)
        ];
    }

}
