<?php

namespace App\Http\Controllers\API\Auth\Survey;

use App\Helper\AppHelper;
use App\Helpers\Address;
use App\Models\Answer;
use App\Models\AssignWalkbook;
use App\Models\HouseHold;
use App\Models\Preference;
use App\Models\SurveyUser;
use App\Models\SurveyUserWalkbook;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SurveyDashboadController extends Controller
{
    public function surveyList(Request $request){
        // $request['assignId'] = 7;
        // $this->serverStart($request);
        /*
         *SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY','')); */
       $data=HouseHold::select('survey_user_walkbook.status','survey_users.name as survey_name','survey_users.voter_id',
           'survey_users.gender','survey_users.age','survey_users.precinct','survey_users.street','survey_users.city',
           'survey_users.state','survey_users.zipcode','survey_users.country','survey_users.phone','survey_users.email',
           'survey_users.house_hold_id','house_holds.name','survey_users.id')
            ->rightJoin('survey_users', 'house_holds.id', '=', 'survey_users.house_hold_id')
            ->Join('survey_user_walkbook', 'survey_users.id', '=', 'survey_user_walkbook.survey_user_id')
            ->where('survey_user_walkbook.walkbook_id',$request->walkbook_id)
            ->groupBy('house_holds.name')
           ->orderBy('survey_users.precinct','asc')
            ->get();


       if($data){
           foreach ($data as $key => $loc){
               $addressStr = $loc['precinct'] . ' ' . $loc['street'] . ' ' . $loc['city'] . ' ' . $loc['state'] . ' ' . $loc['zipcode'] . ' ' . $loc['country'];
               $loc['address']=$addressStr;
               $loc['location'] = Address::getLetAndLon($addressStr);
               $data[$key]['walkbook_id']=$request->walkbook_id;
               $request['assignId'] = $loc->house_hold_id;
                
               $loc['userDetails']=$this->serverStart($request);
               $loc['serveyQuestionDetails']=$this->serverDetails($request);

           }
           // dd($data);
       }
      
       else{
           return $this->sendErrorMessage('Survey list list empty.', 401);
       }


        //return $this->sendResponse( 'List of Survey.',['lists' => $data]);
       return $this->sendResponse( 'List of Survey.',['lists' => AppHelper::paginate(collect($data), 'Survey_lists')]);

    }

    public function serverStart(Request $request){
        //dd($request->all());
        $userPreference = Preference::where('canv_id',Auth::user()->id)->first();
        $dataQuery = SurveyUser::with('walkBook','houseHold', 'surveyUserWalkbook','walkBook.walkbookRelatedToSurvey.survey')
            ->where('house_hold_id', $request->assignId);
        if($userPreference && $userPreference->additional_HH_members) {
            $data = $dataQuery->get();
        } elseif($userPreference && $userPreference->additional_HH_members == 0){
            $data[] = $dataQuery->first();
        } else{
            $data = $dataQuery->get();
        }
        $results=[];
        if($data){
            foreach ($data as $key => $details){
                if (isset($details->walkBook) && $details->walkBook != null){
                    if (isset($details->walkBook)){
                        $results[$key]['walkbook_id']=$details->walkBook[0]->id;
                    }
                  else{
                      return $this->sendErrorMessage('walkbook not assign.', 401);
                  }
                    if (isset($details->walkBook[0]['walkbookRelatedToSurvey'])){
                        $results[$key]['survey_id']=$details->walkBook[0]['walkbookRelatedToSurvey']->survey_id;
                    }
                    else{
                        $results[$key]['survey_id']=null;
                    }
                    $results[$key]['assign_id']=$details->houseHold->id;
                    $results[$key]['house_name']=$details->houseHold->name;
                    $results[$key]['server_name']=$details->name;
                    $results[$key]['users_id']=$details->id;
                    $results[$key]['voter_id']=$details->voter_id;
                    $results[$key]['gender']=$details->gender;
                    $results[$key]['age']=$details->age;
                    $results[$key]['address']=$details->precinct . ' ' . $details->street . ' ' . $details->city . ' ' . $details->state . ' ' . $details->zipcode . ' ' . $details->country;
                    $results[$key]['status'] = $details->surveyUserWalkbook->status;
                    $preference = [];

                    if($userPreference){

                        if($userPreference && $userPreference->vote_history) {
                            $preference[] = [
                                "name" => "vote_history",
                                "value" => $details->vote_history_1 ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/vote_history_1x.png"
                            ];
                        }
                        if($userPreference && $userPreference->contact_history) {
                            $preference[] = [
                                "name" => "contact_history",
                                "value" => $details->contact_history_1 ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/contact_history_1_1x.png"
                            ];
                        }
                        if($userPreference && $userPreference->phone_number) {
                            $preference[] = [
                                "name" => "phone_number",
                                "value" => $details->phone ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/phone_number_1x.png"
                            ];
                        }
                        if($userPreference && $userPreference->early_voting_location) {
                            $preference[] = [
                                "name" => "early_voting_location",
                                "value" => $details->early_voting_location ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/early_voting_location_1x.png"
                            ];
                        }
                        if($userPreference && $userPreference->election_day_location) {
                            $preference[] = [
                                "name" => "election_day_location",
                                "value" => $details->election_day_location ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/election_day_location_1x.png"
                            ];

                        }
                        if($userPreference && $userPreference->voted) {
                            $preference[] = [
                                "name" => "voted",
                                "value" => $details->voted ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/voted_1x.png"
                            ];
                        }
                        if($userPreference && $userPreference->previous_contact_history) {
                            $preference[] = [
                                "name" => "previous_contact_history",
                                "value" => $details->contact_history_3 ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/previous_contact_history_1x.png"
                            ];
                        }
                        if($userPreference && $userPreference->financial_contributor) {
                            $preference[] = [
                                "name" => "financial_contributor",
                                "value" => $details->financial_contributor ?? "NA",
                                "icon" => env('FRONTEND_IMG_URL')."public/preference_icon/financial_1x.png"
                            ];
                        }
                    }

                    $results[$key]['preference']= $preference;
                    $results[$key]['location']= Address::getLetAndLon($details->precinct . ' ' . $details->street . ' ' . $details->city . ' ' . $details->state . ' ' . $details->zipcode . ' ' . $details->country);
                }
                else{
                    return $this->sendErrorMessage('WalkBook list empty.', 401);
                }
            }
        }
        else{
            return $this->sendErrorMessage('surver list empty.', 401);
        }
        return $this->sendResponse( 'Get All Details Successfully.',['lists' => $results]);
    }

    public function serverDetails(Request $request){
       

        $data=AssignWalkbook::select('assign_walkbook.walkbook_id as walkbook_id','videos.id as videosID',
            'assign_walkbook.survey_id as survey_id','question.id as question_id','question.user_id','question.title',
            'question.question_type','question.option_name','question.created_at',DB::raw("CONCAT('" . asset('uploads/videos/') . "/'" . ",file_name) AS videoURL"))
            ->Join('survey', 'assign_walkbook.survey_id', '=', 'survey.id')
            ->Join('question', 'assign_walkbook.survey_id', '=', 'question.survey_id')
            ->leftJoin('videos', 'survey.video_id', '=', 'videos.id')
            ->where('walkbook_id',$request->walkbook_id)
            ->where('assign_walkbook.survey_id', '!=', 0)->get();

        //$data=AssignWalkbook::with('survey.video','survey.questions')->where('walkbook_id',$request->walkbook_id)->get();
        return $this->sendResponse( 'Get All Survey Questions.',['lists' => $data]);
    }

    public function serverDetailsSubmit(Request $request){

        foreach($request->submitUser as $key => $rec) {
            if (!$rec['users_list']){
                return $this->sendErrorMessage('users list empty.', 401);
            }

            foreach ($rec['users_list'] as $key => $usersList) {

                $validation = $this->validateRequest(collect($usersList), [
                    'users_id' => 'required',
                ]);
                if ($validation !== true) {
                    return $validation;
                }
                if (isset($rec['answer'])){
                    foreach ($rec['answer'] as $ans){
                        $answerObj =  new Answer;
                        $answerObj->survey_user_id = $usersList['users_id'];
                        $answerObj->walkbooks_id = $rec['walkbooks_id'];
                        $answerObj->survey_id = $rec['survey_id'];
                        $answerObj->notes = $rec['notes'];
                        $answerObj->notes = $rec['notes'];
                        if (isset($rec['network_status'])){
                            $answerObj->data_mode = $rec['network_status'];
                        }
                        $answerObj->survey_type = $rec['survey_type'];
                        $answerObj->lat = $rec['latitude'];
                        $answerObj->lng = $rec['longitude'];
                        $answerObj->additional_observation = $rec['additional_observation'];
                        $answerObj->canvasser_id = Auth::user()->id;
                        $answerObj->status = 1;

                        $arr = [];
                        foreach($rec['additional_observation'] as $re) {

                            if ($re['checked'] == "true") {
                                $arr[] = $re['value'];

                            }
                        }
                        $answerObj->additional_observation = $arr;

                        if ($ans['question_type'] == 4) {
                            $checked=[];
                            //dd($ans['answer']);
                            foreach ($ans['answer'] as $checkbox)
                            {
                                if ($checkbox['checked'] == "true") {
                                    $checked[] = $checkbox['value'];
                                }

                            }
                            $answerObj->answer = $checked;
                        }
                        if ($ans['question_type'] == 1 || $ans['question_type'] == 2 || $ans['question_type'] == 3) {
                            $answerObj->answer = $ans['answer'];


                        }
                        $answerObj->question_id = $ans['id'];
                        $answerObj->save();
                    }
                }
                else{
                    $answercancel =  new Answer;
                    $arr = [];
                    foreach($rec['additional_observation'] as $re) {

                        if ($re['checked'] == "true") {
                            $arr[] = $re['value'];

                        }
                    }
                    if (isset($rec['network_status'])){
                        $answercancel->data_mode = $rec['network_status'];
                    }
                    $answercancel->additional_observation = $arr;
                    $answercancel->survey_user_id = $usersList['users_id'];
                    $answercancel->walkbooks_id = $rec['walkbooks_id'];
                    $answercancel->survey_id = $usersList['survey_id'];
                    $answercancel->answer = NULL;
                    $answercancel->status = 0;
                    $answercancel->canvasser_id = Auth::user()->id;
                    $answercancel->save();
                    return $this->sendResponse('Data Submit.');
                }


                if($answerObj->save()){
                    SurveyUserWalkbook::where('survey_user_id',$usersList['users_id'])->update(['status'=>1 , 'canvasser_id'=> Auth::user()->id]);
                }
                else{
                    return $this->sendErrorMessage('users list empty.', 401);
                }
            }

        }
        return $this->sendResponse('Data Submit.');
    }


    public function serverCancel(Request $request){
        // dd($request->toArray());
        foreach($request->cancelUser as $key => $rec) {
            $validation = $this->validateRequest(collect($rec), [
                'walkbooks_id' => 'required'
            ]);

            // check validation
            if ($validation !== true) {
                return $validation;
            }

            foreach ($rec['users_list'] as $usersList) {
                $answerObj =  new Answer;
                $answerObj->survey_user_id = $usersList['users_id'];
                $answerObj->walkbooks_id = $rec['walkbooks_id'];
                $answerObj->answer = $rec['answer'];
                if (isset($rec['network_status'])){
                    $answerObj->data_mode = $rec['network_status'];
                }
                $answerObj->additional_observation = $rec['additional_observation'];
                $answerObj->status = 0;
                $answerObj->canvasser_id = Auth::user()->id;
                $arr = [];
                foreach($rec['additional_observation'] as $re) {

                    if ($re['checked'] == "true") {
                        $arr[] = $re['value'];

                    }
                }
                $answerObj->additional_observation = $arr;
                $answerObj->other_impediments = $rec['other_impediments'];
                $answerObj->save();
            }
            
        }

        return $this->sendResponse('Data Submit.');
    }

    public function filterAddress(Request $request){

    //$radius = 1000;
           
       $data=SurveyUser::select('survey_users.id','survey_users.house_hold_id',DB::raw('CONCAT(survey_users.precinct," ",survey_users.street," ",survey_users.city," ",
            survey_users.zipcode," ",survey_users.country) AS address'),
           'survey_users.name','survey_users.voter_id','survey_users.gender','survey_users.age','survey_users.precinct',
           'survey_users.street','survey_users.city','survey_users.zipcode','survey_users.country','survey_users.lat','survey_users.lng',
           'survey_user_walkbook.walkbook_id','survey_user_walkbook.status')
           ->groupBY('survey_users.house_hold_id')

           ->join('survey_user_walkbook', 'survey_users.id', '=', 'survey_user_walkbook.survey_user_id')
          /* ->selectRaw("6371 * acos(cos(radians($request->latitude))
                     * cos(radians(survey_users.lat))
                     * cos(radians(survey_users.lng)
                     - radians($request->longitude))
                     + sin(radians($request->latitude))
                     * sin(radians(survey_users.lat))) AS distance")

            ->orderBy('distance', 'ASC')
            ->havingRaw("distance < ?", [$radius])*/
           ->where('walkbook_id',$request->walkbook_id)
            ->get();
             $walkbooks = [];
            foreach ($data as $key => $direction) {
                $walkbooks[$key]['direction'] = Address::getDirection($request->latitude,$request->longitude,$direction['address']);
                $walkbooks[$key]['id'] = $direction['id'];
                $walkbooks[$key]['house_hold_id'] = $direction['house_hold_id'];
                $walkbooks[$key]['address'] = $direction['address'];
                $walkbooks[$key]['name'] = $direction['name'];
                $walkbooks[$key]['voter_id'] = $direction['voter_id'];
                $walkbooks[$key]['gender'] = $direction['gender'];
                $walkbooks[$key]['age'] = $direction['age'];
                $walkbooks[$key]['precinct'] = $direction['precinct'];
                $walkbooks[$key]['street'] = $direction['street'];
                $walkbooks[$key]['city'] = $direction['city'];
                $walkbooks[$key]['zipcode'] = $direction['zipcode'];
                $walkbooks[$key]['country'] = $direction['country'];
                $walkbooks[$key]['lat'] = $direction['lat'];
                $walkbooks[$key]['lng'] = $direction['lng'];
                $walkbooks[$key]['walkbook_id'] = $direction['walkbook_id'];
                $walkbooks[$key]['status'] = $direction['status'];
                $request['assignId'] = $direction['house_hold_id'];
                $walkbooks[$key]['userDetails']=$this->serverStart($request);              
                $walkbooks[$key]['serveyQuestionDetails']=$this->serverDetails($request);
                
                
            }
            sort($walkbooks);
         

          
return $this->sendResponse( 'Get All Details Successfully.',['lists' => AppHelper::paginate(collect($walkbooks), 'Survey_lists')]);
     
       //return $this->sendResponse( 'Get All Details Successfully.',['lists' => $walkbooks]);

    }

    public function filterMap(Request $request){
        //$radius = 30 * 0.62137;
        $data=SurveyUser::select('survey_users.id',DB::raw('CONCAT(survey_users.precinct," ",survey_users.street," ",survey_users.city," ",
            survey_users.zipcode," ",survey_users.country) AS address'),
            'survey_users.name','survey_users.voter_id','survey_users.gender','survey_users.age','survey_users.precinct',
            'survey_users.street','survey_users.city','survey_users.zipcode','survey_users.country','survey_users.lat','survey_users.lng',
            'survey_user_walkbook.walkbook_id','survey_user_walkbook.status','survey_users.house_hold_id')
            ->groupBY('survey_users.house_hold_id')

            ->join('survey_user_walkbook', 'survey_users.id', '=', 'survey_user_walkbook.survey_user_id')
           /* ->selectRaw("6371 * acos(cos(radians($request->latitude))
                     * cos(radians(survey_users.lat))
                     * cos(radians(survey_users.lng)
                     - radians($request->longitude))
                     + sin(radians($request->latitude))
                     * sin(radians(survey_users.lat))) AS distance")

            ->orderBy('distance', 'ASC')
            ->havingRaw("distance < ?", [$radius])*/
            ->where('walkbook_id',$request->walkbook_id)
            ->get();

                 $walkbooks = [];
            foreach ($data as $key => $direction) {
                $walkbooks[$key]['direction'] = Address::getDirection($request->latitude,$request->longitude,$direction['address']);
                $walkbooks[$key]['id'] = $direction['id'];
                $walkbooks[$key]['house_hold_id'] = $direction['house_hold_id'];
                $walkbooks[$key]['address'] = $direction['address'];
                $walkbooks[$key]['name'] = $direction['name'];
                $walkbooks[$key]['voter_id'] = $direction['voter_id'];
                $walkbooks[$key]['gender'] = $direction['gender'];
                $walkbooks[$key]['age'] = $direction['age'];
                $walkbooks[$key]['precinct'] = $direction['precinct'];
                $walkbooks[$key]['street'] = $direction['street'];
                $walkbooks[$key]['city'] = $direction['city'];
                $walkbooks[$key]['zipcode'] = $direction['zipcode'];
                $walkbooks[$key]['country'] = $direction['country'];
                $walkbooks[$key]['lat'] = $direction['lat'];
                $walkbooks[$key]['lng'] = $direction['lng'];
                $walkbooks[$key]['walkbook_id'] = $direction['walkbook_id'];
                $walkbooks[$key]['status'] = $direction['status'];
                $request['assignId'] = $direction['house_hold_id'];
                $walkbooks[$key]['userDetails']=$this->serverStart($request);              
                $walkbooks[$key]['serveyQuestionDetails']=$this->serverDetails($request);
            }
            sort($walkbooks);
                
        return $this->sendResponse( 'Get All Details Successfully.',['lists' => $walkbooks, 'Survey_lists']);

    }

    public function surveyHistory(Request $request){
        $data=Answer::select('survey_users.voter_id','survey_users.name as prospect','answer.id as surveyID',DB::raw('DATE_FORMAT(answer.created_at, "%d-%b-%Y") as CreateOn_date'),'answer.*')
            ->join('survey_users', 'answer.survey_user_id', '=', 'survey_users.id')
            ->groupBY('answer.survey_user_id','answer.survey_id','answer.walkbooks_id')
            ->orderBy('answer.id','DESC')
            ->where('status',1)
            ->get();

        return $this->sendResponse( 'Get All Details Successfully Survey History.',['lists' => AppHelper::paginate($data)]);
    }

    public function historyDetails(Request $request){
        $data=SurveyUserWalkbook::select('survey_user_walkbook.status','survey_users.voter_id')
            ->join('survey_users', 'survey_user_walkbook.survey_user_id', '=', 'survey_users.id')
            ->where(['survey_user_id'=>$request->survey_user_id,
                     'walkbook_id'=>$request->walkbook_id,
                ])->first();

         $ques=Answer::select('id','question_id','answer','survey_type',DB::raw('DATE_FORMAT(created_at, "%d-%b-%Y") as CreateOn_date'))->where(['survey_user_id'=>$request->survey_user_id,
             'walkbooks_id'=>$request->walkbook_id,
             'survey_id'=>$request->survey_id
         ])->get();
         if(count($ques)){
             $result= array([
                 "canvassar_name"=>Auth::user()->name,
                 "status"=>isset($data->status) ? $data->status : null,
                 "voter_id"=>isset($data->voter_id) ? $data->voter_id : null,
                 "survey_id"=>isset($ques)? $ques[0]->id : null,
                 "survey_type"=>isset($ques) ? $ques[0]->survey_type : null,
                 "CreateOn_date"=>isset($ques) ? $ques[0]->CreateOn_date : null,
             ]);

             $arr = [];
             foreach ($ques as $qu) {

                 $arr[]=Answer::select('answer.id as surveyID','question.id as questionID','question.title',
                     'answer.answer','answer.notes','answer.additional_observation','question.question_type',DB::raw('DATE_FORMAT(answer.created_at, "%d-%b-%Y") as CreateOn_date'))
                     ->join('question', 'answer.question_id', '=', 'question.id')
                     ->where(['question_id' => $qu['question_id']])->first();
             }

             return $this->sendResponse( 'Get All Details Successfully.',['profileDetails' => $result, 'question'=>$arr]);
         }

        else{
                return $this->sendErrorMessage('Question list empty.', 401);
            }
    }

}
