<?php

namespace App\Http\Controllers\API\Auth\Event;

use App\Helper\AppHelper;
use App\Helpers\Address;
use App\Models\AssignWalkbook;
use App\Models\Event;
use App\Models\HouseHold;
use App\Models\EventAttende;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\EventAttendeHelper;

class EventApiController extends Controller
{

	/**
	**@param Request
	**@return list of event
	**/
	public function listEvent(Request $request){

		$query = Event::orderBy('id','desc')->where(['eventStatus' => 1])->get();
		$events = [];
		foreach ($query as $key => $result) {
			$events[$key]['eventID'] = $result->id;
			$events[$key]['eventName'] = $result->eventName;
			$events[$key]['eventStartDate'] = $result->eventStartDate;
			$events[$key]['eventEndDate'] = $result->eventEndDate;
			$events[$key]['eventAddress'] = $result->eventAddress;
			$events[$key]['eventStatus'] = $result->eventStatus;
			$events[$key]['eventStartTime'] = $result->eventStartTime;
			$events[$key]['eventEndTime'] = $result->eventEndTime;
			$events[$key]['rsvpCount'] = count(EventAttendeHelper::getEventAttende($result->id));
		}
		if ($events) {
		return $this->sendResponse( 'List of Events.',['eventList' => AppHelper::paginate(collect($events))]);
		} else {
			return $this->sendErrorMessage('List of Events is empty.', 401);
		}

	}

	/**
	**@param eventID
	**@return list of attendee at a particular event
	**/
	public function listOfEventAttendee(Request $request){
		$eventID = $request->eventID;
		//dd($request->eventID);
		$event = Event::where(['id' => $eventID])->first();
		//dd($event);
		$eventRecord = [];

		$eventRecord['eventID'] = $event->id;
		$eventRecord['eventName'] = $event->eventName;
		$eventRecord['eventStartDate'] = $event->eventStartDate;
		$eventRecord['eventEndDate'] = $event->eventEndDate;
		$eventRecord['eventAddress'] = $event->eventAddress;
		$eventRecord['eventStatus'] = $event->eventStatus;
		$eventRecord['eventStartTime'] = $event->eventStartTime;
		$eventRecord['eventEndTime'] = $event->eventEndTime;
		$eventRecord['rsvpCount'] = count(EventAttendeHelper::getEventAttende($event->id));

		$eventAttendeeLists = EventAttendeHelper::getEventAttende($eventID);
		$eventAttende = [];
		foreach ($eventAttendeeLists as $key => $attendeeEvent) {
			$eventAttende[$key]['eventAttendeID'] = $attendeeEvent->id;
			$eventAttende[$key]['attendeeStatus'] = $attendeeEvent->attendeEventCheckedInStatus;
			$eventAttende[$key]['attendeeID'] = $attendeeEvent->attendeID;
			$eventAttende[$key]['attendeeName'] = $attendeeEvent->attendeName;
			$eventAttende[$key]['eventID'] = $attendeeEvent->eventID;
			$eventAttende[$key]['attendeeAddress'] = $attendeeEvent->attendeAddress;
			$eventAttende[$key]['attendeeEmailAddress'] = $attendeeEvent->attendeEmailAddress;
			$eventAttende[$key]['attendeePhoneNumber'] = $attendeeEvent->attendePhoneNo;

		}
		if ($eventRecord) {
		return $this->sendResponse('List of Events Attendes.',['eventList' => $eventRecord,'attendeesList' => AppHelper::paginate(collect($eventAttende))]);
		} else {
			return $this->sendErrorMessage('List of Events is empty.', 401);
		}

	}

	/**
	**@param eventAttendeId
	**@return success and error message
	**/
	public function eventAttendeCheckinStatus(Request $request){

		$eventAttendeID = $request->eventAttendeID;
		$eventAttende = EventAttende::where(['id' => $eventAttendeID])->first();

		if ($eventAttende->attendeEventCheckedInStatus == 0){
		$eventAttende->attendeEventCheckedInStatus = 1;
		$record = $eventAttende->save();

		if ($record) {
			return $this->sendResponse('Attendee Checked in successfully.');
		} else {
			return $this->sendErrorMessage('Something went wrong.', 401);
		}
		} else {
			$eventAttende->attendeEventCheckedInStatus = 0;
			$record = $eventAttende->save();
			if ($record) {
				return $this->sendResponse('Attendee Checked in status changes to signin successfully.');
			} else {
			return $this->sendErrorMessage('Attende already checked in.', 401);
			}
		}
	}


}
