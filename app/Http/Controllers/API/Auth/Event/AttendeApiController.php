<?php

namespace App\Http\Controllers\API\Auth\Event;

use App\Helper\AppHelper;
use App\Helpers\Address;
use App\Models\AssignWalkbook;
use App\Models\Event;
use App\Models\Attende;
use App\Models\EventAttende;
use App\Models\HouseHold;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\EventAttendeHelper;
use Mail;
use App\Mail\AttendeEventApprovel;
use Illuminate\Support\Facades\Validator;
use Auth;

class AttendeApiController extends Controller
{

	/**
	**@param $attendeeEventID
	**@return attendee and attendeeEvent
	**/
	public function editAttendee(Request $request){
		$attendeeEventID = $request->attendeeEventID;
		$eventAttendeeRec = EventAttende::where(['id' => $attendeeEventID])->first();

		$attendeRec = Attende::where(['id' => $eventAttendeeRec->attendeID])->first();
		$attendeeObject = [];
		$attendeeObject['attendeeID'] = $attendeRec->id;
		$attendeeObject['attendeeEmailAddress'] = $attendeRec->attendeEmailAddress;
		$attendeeObject['attendeeName'] = $attendeRec->attendeName;
		$attendeeObject['attendeeAddress'] = $attendeRec->attendeAddress;
		$attendeeObject['attendeePhoneNumber'] = $attendeRec->attendePhoneNo;
		$attendeeObject['eventAttendeeID'] = $eventAttendeeRec->id;
		$attendeeObject['attendeeStatus'] = $eventAttendeeRec->attendeEventSupportStatus;
		$attendeeObject['eventID'] = $eventAttendeeRec->eventID;
		if($attendeRec){
			return $this->sendResponse('List of Event Attendee.',['attendeesList' => $attendeeObject]);
		} else {
			return $this->sendErrorMessage('List of Events is empty.', 401);
		}
	}

	/**
	**@param attendeObject,attendeName,attendeAddress,attendePhoneNo
	**@return attendeId
	**/
	public static function addSingleAttende($attendeObj,$attendeName,$attendeAddress,$attendePhoneNo,$attendeEmailAddress){

		$attendeObj->attendeName = $attendeName;
		$attendeObj->attendeAddress = $attendeAddress;
		$attendeObj->attendePhoneNo = $attendePhoneNo;
		$attendeObj->attendeEmailAddress = $attendeEmailAddress;
		$record = $attendeObj->save();

		return $attendeObj->id;
	}


	/**
	**@param $eventID,$attendeObjID,$attendeeEventSupportStatus,$userEmail
	**@return success then object return else error
	**/
	public static function addEventAttendeSingle($eventID,$attendeObjID,$attendeeEventSupportStatus,$userEmail){

		$eventAttendeObj = EventAttende::where(['eventID' => $eventID])->where(['attendeID' => $attendeObjID])->first();

				if (!$eventAttendeObj){
					$random = rand(pow(10,7), pow(10,8));

					$link = $random;

					$eventAttendeObj = new EventAttende;
					$eventAttendeObj->eventID = $eventID;
					$eventAttendeObj->attendeID = $attendeObjID;
					$eventAttendeObj->attendeToken = $random;
					$eventAttendeObj->attendeEventEntryPoint = 0;
					$eventAttendeObj->attendeEventSupportStatus = $attendeeEventSupportStatus;
					$eventAttendeObj->eventAttendeCreatedByUserID = Auth::user()->id;
					$eventAttendeObj->save();

					$event = Event::where(['id' => $eventID])->first();
                 //Email send to attende
               	Mail::to($userEmail)->send(new AttendeEventApprovel($attendeObjID,$link,$eventID,$event->eventName));

				$attendeObj = Attende::where(['id' => $attendeObjID])->first();
				$attendeObj->attendeAssiginedEventStatus = 1;

				$rec = $attendeObj->save();

					if ($rec) {
						return $eventAttendeObj;
					} else {
						return "error";
					}

				} else {
					$eventAttendeObj->attendeEventSupportStatus = $attendeeEventSupportStatus;
					$res = $eventAttendeObj->save();

					if ($res) {
						return $eventAttendeObj;
					} else {
						return "error";
					}
				}

	}


	/**
	**@param request data
	**@return object of attende
	**/
	public function addAttendeeEvent(Request $request){
		//dd($request->all());
		$validator = Validator::make($request->all(), [
            'attendeeName' => 'required',
            'attendeeAddress' => 'required',
            'attendeePhoneNumber' => 'required|regex:/^\+?\d+$/',
            'attendeeEmailAddress' => 'required|email'
        ],[
        	'attendeeName.required' => 'Attende name is required',
        	'attendeeAddress.required' => 'Attende address is required',
        	'attendeePhoneNo.required' => 'Attende phone number is required',
        	'attendeeEmailAddress.required' => 'Attende email is required',
        	'attendeeEmailAddress.email' => 'Attende email must be valid'
        ]);
        //dd($request->eventID);

        if ($validator->fails()) {

            return response()->json(['errors'=>$validator->errors()]);
        }

		if($request->attendeeID){
			$attendeObj = Attende::where(['id' => $request->attendeeID])->first();
		} else {
			$attendeObj = new Attende;
		}

			$userEmail = $request->attendeeEmailAddress;
			$attendeObjID = self::addSingleAttende($attendeObj,$request->attendeeName,$request->attendeeAddress,$request->attendeePhoneNumber,$request->attendeeEmailAddress);

			if ($attendeObjID) {
				$arr = [];
				foreach($request->attendeeEventSupportStatus as $rec) {
					if ($rec['checked'] == true){
						$arr[] = $rec['value'];
					}
				}

				$result = self::addEventAttendeSingle($request->eventID,$attendeObjID,$arr,$userEmail);
				if ($result == "error") {
					return $this->sendErrorMessage('Something went wrong.', 401);
				} else {
					$attende = Attende::where(['id' => $result->attendeID])->first();
					$record = [];
					$record['attendeeID'] = $attende['id'];
					$record['attendeeEmailAddress'] = $attende['attendeEmailAddress'];
					$record['attendeeName'] = $attende['attendeName'];
					$record['attendeeAddress'] = $attende['attendeAddress'];
					$record['attendeePhoneNumber'] = $attende['attendePhoneNo'];
					$record['attendeeEventSupportStatus'] = $result['attendeEventSupportStatus'];
					return $this->sendResponse('Attendee record',['attendeesList' => collect($record)]);
				}

		} else {
			return $this->sendErrorMessage('Something went wrong.', 401);
		}


	}

}
