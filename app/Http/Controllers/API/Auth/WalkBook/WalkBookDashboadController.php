<?php

namespace App\Http\Controllers\API\Auth\WalkBook;

use App\Helpers\Address;
use App\Models\SurveyUser;
use App\Models\SurveyUserWalkbook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Walkbook;
use App\Helper\AppHelper;
use Illuminate\Support\Facades\Auth;
use DB;

class WalkBookDashboadController extends Controller
{
   public function walkList(Request $request){
       $walkbooks = Walkbook::with('surveyUser')->where('status',1)->get();
       

       if($walkbooks){
           $results = [];
           foreach ($walkbooks as $walkKey => $walk) {
               if (!empty($walk) && !empty($walk['user_id']) && in_array(Auth()->user()->id,$walk['user_id'])) {

                   if(isset($walk['surveyUser'])) {
                      $walkbooks[$walkKey]['totalwalkbook'] = count(\App\Helper\AppHelper::getHouseHold($walk['surveyUser']));

                    
                       $walkbooks[$walkKey]['walkadd'] =Null;
                       $houseHoldArr = [];
                       foreach ($walk['surveyUser'] as $key => $walkadd) {

                           $isPending = SurveyUserWalkbook::where(['survey_user_id' => $walkadd->pivot['walkbook_id'], 'status' => 0])->first();
                            if($isPending){
                                $houseHoldArr[] =  $walkadd['house_hold_id'];
                            }

                         /*  $addressStr = $walkadd['precinct'] . ' ' . $walkadd['street'] . ' ' . $walkadd['city'] . ' ' . $walkadd['state'] . ' ' . $walkadd['zipcode'] . ' ' . $walkadd['country'];*/
                         $addressStr = $walkadd['street'] . ' ' . $walkadd['city'] . ' ' . $walkadd['state'];
                           $walkbookadd =$walkadd['city'];
                           $walkbooks[$walkKey]['surveyUser'][$key]['location'] = Address::getLetAndLon($addressStr);
//                           $walkbooks[$walkKey]['Household_left'] = $left;

                           $walkbooks[$walkKey]['walkadd'] = Address::getLetAndLon($walkbookadd);

                           $add=Address::getLetAndLon($addressStr);
                           SurveyUser::where('id',$walkadd->id)->update(['lat' => $add['lat'],
                               'lng' =>$add['lon']
                           ]);
                       }
                       // $walkbooks[$walkKey]['Household_left'] = count($houseHoldArr);
                       DB::enableQueryLog();
                        $Household_left = SurveyUser::has('pendingWalkBook')/*whereHas('comments', function (Builder $query) {
                          $query->where('content', 'like', 'foo%');
                        })*/
                        ->groupBy('survey_users.house_hold_id')
                        ->get()
                        ->count();

                         //$walkbooks[$walkKey]['Household_left'] =  $Household_left;
 

                      // $walkbooks[$walkKey]['Household_left'] = count(\App\Helper\AppHelper::getHouseHoldCount($walk['surveyUser']));
                      
                      /*(03/01/20) Begin*/
                      $pendingHouseHold =  \DB::table('survey_user_walkbook')
                      ->where('survey_user_walkbook.status', 0)
                      ->where('survey_user_walkbook.walkbook_id', $walk->id)
                      ->join('survey_users', 'survey_users.id', 'survey_user_walkbook.survey_user_id')
                      ->groupBy('survey_users.house_hold_id')
                      //->groupBy('survey_user_walkbook.survey_user_id')
                      //->count();
                      ->get();

                      $walkbooks[$walkKey]['Household_left'] =  $pendingHouseHold->count();
                      /*(03/01/20) End*/

                   }

                   $results[$walkKey] = $walkbooks[$walkKey];

               }
               
               // echo count(\App\Helper\AppHelper::getHouseHoldCount($walk['surveyUser'])); die;
           }
           return $this->sendResponse( 'List of WalkBook.',['lists' => AppHelper::paginate(collect($results), 'walkbook_lists')]);
       }
       else{
           return $this->sendErrorMessage('WalkBook list empty.', 401);
       }
   }
}
