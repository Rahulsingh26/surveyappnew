<?php
namespace App\Http\Controllers\API\Auth;
use App\Helper\AppHelper;
use App\Models\Answer;
use App\Models\Preference;
use App\Models\SurveyUser;
use App\Models\SurveyUserWalkbook;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;
use Validator;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{

public $successStatus = 200;
/**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        $validation = $this->validateRequest($request, [
            "email" => 'required|email',
            'password' => 'required',
        ]);

        // check validation
        if ($validation !== true) {
            return $validation;
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            if($user->status != 0){
                $data = [
                    'token' => $user->createToken('MyApp')->accessToken,
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone_number' => $user->phone_number,
                    'status' => $user->status,
                    'address' => $user->address,
                ];
                return $this->sendResponse('Successful login.', $data);
            }
            else{
                return $this->sendErrorMessage('Inactive User.', 404);
            }

        }
        else{
            return $this->sendErrorMessage('Your password is invalid,Please try again.', 401);
        }

    }

    public function passwordUpdate(Request $request){

        $validator = Validator::make($request->all(), [
            'old_Password'=>['required', function ($attribute, $value, $fail) {
                if (!Hash::check($value, Auth::user()->password)) {
                            $fail('old Password is invalid.');
                        }
                }],
            'password' => 'required|min:6',

        ]);
        if ($validator->fails()) {
            return $this->sendErrorMessage($validator->errors(), 401);
        }

            if (Auth::check()) {
            $user = Auth::user();
            $user->password = Hash::make($request->password);
            $rec = $user->save();

                if ($rec) {
                    return $this->sendResponse('your password has been reset successfully.');
                } else {
                    return $this->sendErrorMessage('Something went wrong.', 401);
                }
            }
            else {
                return $this->sendErrorMessage('Something went wrong.', 401);
            }


    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            return $this->sendResponse('Thanks for using this application, logout successfully.');
        } else {
            return $this->sendErrorMessage('You are not logout correctly.', 404);
        }

    }

    public function userProfileImg(Request $request){
        $files = $request->file('images');

        if ($request->hasFile('images')) {

            $fileName = uniqid();
            //get just ext
            $extension = $files->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $fileName . '.' . $extension;
            //Upload Image
            $files->move(storage_path('app/public/users/images/'), $fileNameToStore);
            $images = 'storage/app/public/users/images/' . $fileNameToStore;

            $img = User::where('id',Auth::id())->first();
            $img->image = $images;
            $img->save();
        }
        else{
            return $this->sendErrorMessage('Something went wrong.', 404);
        }
        return $this->sendResponse('successfully.');
    }

    public function preferences(Request $request){
        $oldData = Preference::where('canv_id',Auth::user()->id)->first();

        $data=Preference::where('canv_id',Auth::user()->id)
        ->update([
            "additional_HH_members"=>isset($request->additional_HH_members) ? $request->additional_HH_members:$oldData['additional_HH_members'],
            "contact_history"=>isset($request->contact_history) ? $request->contact_history:$oldData['contact_history'],
            "vote_history"=>isset($request->vote_history) ? $request->vote_history:$oldData['vote_history'],
            "make_me_discoverable"=>isset($request->make_me_discoverable) ? $request->make_me_discoverable:$oldData['make_me_discoverable'],
            "phone_number"=>isset($request->phone_number) ? $request->phone_number:$oldData['phone_number'],
            "early_voting_location"=>isset($request->early_voting_location) ? $request->early_voting_location:$oldData['early_voting_location'],
            "election_day_location"=>isset($request->election_day_location) ? $request->election_day_location:$oldData['election_day_location'],
            "voted"=>isset($request->voted) ? $request->voted:$oldData['voted'],
            "previous_contact_history"=>isset($request->previous_contact_history) ? $request->previous_contact_history:$oldData['previous_contact_history'],
            "financial_contributor"=>isset($request->financial_contributor) ? $request->financial_contributor:$oldData['financial_contributor'],
        ]);
        if(!$data){
            try{
                $preferences =  new Preference();
                $preferences->canv_id = Auth::user()->id;
                $preferences->additional_HH_members = isset($request->additional_HH_members) ? $request->additional_HH_members :false;
                $preferences->contact_history = isset($request->contact_history) ? $request->contact_history :false;
                $preferences->vote_history = isset($request->vote_history) ? $request->vote_history :false;
                $preferences->make_me_discoverable = isset($request->make_me_discoverable) ? $request->make_me_discoverable :false;
                $preferences->phone_number = isset($request->phone_number) ? $request->phone_number :false;
                $preferences->early_voting_location = isset($request->early_voting_location) ? $request->early_voting_location :false;
                $preferences->election_day_location = isset($request->election_day_location) ? $request->election_day_location :false;
                $preferences->voted = isset($request->voted) ? $request->voted :false;
                $preferences->previous_contact_history = isset($request->previous_contact_history) ? $request->previous_contact_history :false;
                $preferences->financial_contributor = isset($request->financial_contributor) ? $request->financial_contributor :false;
                $preferences->save();
            }
            catch (\Exception $exception) {
                return $this->sendErrorMessage('Something went wrong.', 404);
            }

        }

        return $this->sendResponse('successfully.');
    }

    public function getPreference(Request $request){
        $data= Preference::where('canv_id',Auth::user()->id)->get();
        if (count($data)){
            return $this->sendResponse( 'Get All Details Successfully.',['profileDetails' => AppHelper::paginate(collect($data))]);
        }
        else{
            $preferences =  new Preference();
            $preferences->canv_id = Auth::user()->id;
            $preferences->additional_HH_members = "0";
            $preferences->contact_history = "0";
            $preferences->vote_history = "0";
            $preferences->make_me_discoverable = "0";
            $preferences->phone_number = "0";
            $preferences->early_voting_location = "0";
            $preferences->election_day_location = "0";
            $preferences->voted = "0";
            $preferences->previous_contact_history = "0";
            $preferences->financial_contributor = "0";
            $preferences->save();
            $preferencesDetails= Preference::where('canv_id',Auth::user()->id)->get();
            return $this->sendResponse( 'Get All Details Successfully.',['profileDetails' => AppHelper::paginate(collect($preferencesDetails))]);
        }
    }

    public function profileDetails(Request $request){

        $surveyUserIds = SurveyUserWalkbook::where(['canvasser_id' => auth()->user()->id, 'status' => 1])->get()->pluck('survey_user_id');
        $totalAssignedUser = count($surveyUserIds);

        $totalDoorKnocked = Answer::where(['canvasser_id' => Auth::user()->id])
            ->groupBy('survey_user_id')
            ->get();

        $totalDoorKnockedToday = Answer::where(['canvasser_id' => Auth::user()->id])
            ->whereDate('created_at', Carbon::today())
            ->groupBy('survey_user_id')
            ->get();

        // connect rate today
        $connectRateToday = $this->connectRate(0);
        $averageConnectRate = ($totalAssignedUser == 0) ? 0 : $this->connectRate(0)/$totalAssignedUser;

        $data=array([
            "name"=> Auth::user()->name,
            "profile"=> env('FRONTEND_IMG_URL').Auth::user()->image,
            "Doors_knocked_to_date"=> count($totalDoorKnocked),
            "Doors_knocked_to_today"=> count($totalDoorKnockedToday),
            "Average_connect_rate"=> $averageConnectRate,
            "Today_connect_rate"=> $connectRateToday
        ]);
        return $this->sendResponse( 'Get All Details Successfully.',['profileDetails' => AppHelper::paginate(collect($data))]);
    }

    /**
     * Calculate connect rate
     * @param bool $isToday
     * @return float|int
     */
    private function connectRate($isToday = false){

        $totalHouseholdTodayKnockedQuery = Answer::where(['canvasser_id' => Auth::user()->id, 'survey_type' => 'Household'])->groupBy('survey_user_id');

        $surveyTodayUserIdsQuery = Answer::where(['canvasser_id' => Auth::user()->id])->groupBy('survey_user_id');

        $noAnswerTodaysQuery = Answer::where(['status' => 1, 'canvasser_id' => Auth::user()->id])->groupBy('survey_user_id');
        if($isToday){
            $totalHouseholdTodayKnockedQuery->whereDate('created_at', Carbon::today());
            $surveyTodayUserIdsQuery->whereDate('created_at', Carbon::today());
            $noAnswerTodaysQuery->whereDate('created_at', Carbon::today());
        }
        $totalHouseholdTodayKnocked = count($totalHouseholdTodayKnockedQuery->get());
        if($totalHouseholdTodayKnocked == 0){
            return 0;
        }
        $surveyTodayUserIds = $surveyTodayUserIdsQuery->get()->pluck('survey_user_id');
        $surveyTodayUsers = SurveyUser::whereIn('id', $surveyTodayUserIds)->get();
        $noAnswerTodays = $noAnswerTodaysQuery->get();

        $supportCount = 0;
        $undecidedCount = 0;
        $opposedCount = 0;
        foreach ($surveyTodayUsers as $surveyUser){
            switch ($surveyUser->Identification){
                case "Support":
                    $supportCount++;
                    break;
                case "Undecided":
                    $undecidedCount++;
                    break;
                case "Opposed":
                    $opposedCount++;
                    break;
            }
        }

        $noAnswerTodayCount = 0;
        $contactImpedimentsToday = 0;
        foreach ($noAnswerTodays as $noAnswerToday){

            switch ($noAnswerToday->answer){
                case "Not Home":
                    $noAnswerTodayCount++;
                    break;
                case "Refused Survey":
                case "Deceased":
                case "Not Listed Voter":
                case "Address Not Found":
                case "No Soliciting":
                    $contactImpedimentsToday++;
                    break;
            }
        }

         $calculate=$supportCount + $undecidedCount + $opposedCount + $noAnswerTodayCount;


        return $calculate / $totalHouseholdTodayKnocked-$contactImpedimentsToday;

        //return $supportCount+$undecidedCount+$opposedCount+$noAnswerTodayCount/$totalHouseholdTodayKnocked-$contactImpedimentsToday;

    }
}
