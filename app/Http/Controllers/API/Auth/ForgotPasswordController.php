<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Models\User; 
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class ForgotPasswordController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset emails and
	| includes a trait which assists in sending these notifications from
	| your application to your users. Feel free to explore this trait.
	|
	 */
	use SendsPasswordResetEmails;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

    /**
     * Send a reset link to the given user.
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
	public function getResetToken(Request $request)
	{
        
		$validation = $this->validateRequest($request, [
			'email' => 'required|email',
		], [
			'email.required' => 'Email field is required.',
			'email.email' => 'Email must be a valid email address.',
		]);
		
		// check validation
		if ($validation !== true) {
			return $validation;
		}

		$user = User::where(['email' => $request->input('email'), 'status' => 1])->first();
		
		if (!$user) {
			return $this->sendErrorMessage([
				0 => trans('passwords.user')
			]);
		}
		
		$token = $this->broker()->createToken($user);
		//dd($user->email);
		$user->token = $token;
		$user->save();
		Mail::to($user->email)->send(new ForgotPassword($user));

		return $this->sendResponse("An email sent to your email, please confirm!", [
			'token' => $token
		]);

	}

	public function ChangePasswordForm(Request $request) {
		$token = $request->token;
        return view('mails/reset',compact("token"));
        
	}
	
	public function getChangedPassword(Request $request) {
		

		$validator = Validator::make($request->all(), [
             'password'  => 'required|min:2',
            'password_confirmation' => 'required|same:password'
        ]);
		$path = 'password/forget?token='.$request->user_token;
        if ($validator->fails()) {
			
            return redirect($path)
                        ->withErrors($validator)
                        ->withInput();
        }
		$user = User::where(['token' => $request->user_token])->first();
		if($user) {
            $user->password = bcrypt($request->password);
            $user->token = $request->password.'_used';
            $user->save();
        } else {
            request()->session()->flash('error','you have already updated the password by this link');
       		 return redirect($path);
		}
		request()->session()->flash('success','Password updated successfully');
        return redirect($path);
        
	}
}
