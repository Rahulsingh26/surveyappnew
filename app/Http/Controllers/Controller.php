<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    /**
     * @param $request
     * @param $rules
     * @param array $customErrorMessages
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function validateRequest($request, $rules, $customErrorMessages = [])
    {

        $validator = Validator::make($request->all(), $rules, $customErrorMessages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return $this->sendErrorMessage($errors->all());
        }
        return true;
    }

    /**
     * @param $message
     * @param string $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResponse($message, $data = '')
    {

        // if (!is_array($data)) {
        //     $data = json_decode("{}");
        // }
        $response = [
            'message' => $message,
            'data' => $data,
            'code' => 200
        ];

        return response()->json($response, 200);
    }

    /**
     * @param array $message
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendErrorMessage($message = [], $status = 400)
    {
        $single = null;
        $message = is_object($message) ? json_decode(json_encode($message), true) : $message;
        $message = is_array($message) ? $message : [$message];
        $newMessage = [];
        if (count(array_filter(array_keys($message))) > 0) {
            foreach ($message as $key => $errors) {
                $newMessage[$key] = count($errors) > 0 ? $errors[0]: $errors;
            }

            $message = $newMessage;
        }

        $tempErrors = array_values($message);
         $single = count($tempErrors) > 0 ? $tempErrors[0] : null;

        $response = [
            'message' => $message,
            'data' => $single,
            'code' => $status
        ];

        return response()->json($response, $status);
    }

}

function ___($text, $locale = null)
{
    return \App\Models\TranslationStatic::get($text, $locale);
}