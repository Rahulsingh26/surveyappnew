<?php

namespace App;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Illuminate\Support\Facades\Auth;

class CustomFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
            if ($this->isRole() == 'super-admin') {
                return $item;
            } else {

                if(isset($item['role']) && $item['role'] != $this->isRole()){
                    return false;
                }
                if (isset($item['permission'])) {
                    $istrue = $this->isVisible($item['permission']);
                    if ($istrue) {
                        return $item;
                    }
                    return false;
                }
                return $item;
            }
    }

    public function isRole(){
            $role = Auth::user()->getRoleNames();
            return $role[0];
    }
    public function isVisible($permission = null){
        $permissions = Auth::user()->getAllPermissions()->map(function ($permission){
                return $permission->name;
        })->toArray();
        if(in_array($permission,$permissions)){
            return true;
        }
        return false;
    }
}
