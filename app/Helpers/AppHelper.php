<?php
namespace App\Helper;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\SurveyUser;
use App\Models\SurveyUserWalkbook;

class AppHelper {

    public static function getPermissionsName($allPermission){
        $permission =[];
        foreach ($allPermission as $single) {
                $permission[] = ucfirst($single->name);
        }

        if(count($permission)) {
            return implode(',', $permission);
        }
        return 'N/A';
    }
    public static function getUserDetails($user_id){
       return User::find($user_id);
    }

    public static function paginate($collection, $collectionName = 'data', $pageNumber = null, $perPage = 10)
    {
        if (!$pageNumber)
            $pageNumber = request()->get('page', 1);

        $query_per_page = request()->get('per_page');

        if (!empty($query_per_page)) {
            $perPage = $query_per_page;
        } elseif (!$perPage) {
            $perPage = 10;
        }

        $startIndex = ($pageNumber-1)*$perPage;
        //dd($startIndex);
        return [
            'page_count' => ceil($collection->count() / $perPage),
            'current_page' => (int)$pageNumber,
            $collectionName => $collection->slice($startIndex, $perPage, true)->values(),
        ];
    }

    /**
    **@param userID
    **@return userName who edit record
    **/
    public static function getUserCreateEditName($userID){
        if ($userID) {
        $user = User::where(['id' => $userID])->first();
        return $user->name;
        } else {
            return "Deleted User";
        }
    }

    /**
    **@param userID
    **@return userName who edit record
    **/
    public static function getUserCreateEditNameForEventAttendee($userID){
        if ($userID) {
        $user = User::where(['id' => $userID])->first();
        return $user->name;
        } else {
            return "Deleted User";
        }
    }

    public static function getHouseHold($surveyUserArr){
        $userID=[];
        foreach ($surveyUserArr as $key=> $result){
            $userID[] = $result->id;
        }
        $survey_users = SurveyUser::whereIn('id', $userID)->groupBy('house_hold_id')->get();
        return $survey_users;
    }

     public static function getHouseHoldCount($surveyUserArr){
       $data=$surveyUserArr->toArray();
        $userID=[];
        foreach ($data as $result){

                $userID[] =  SurveyUserWalkbook::where(['survey_user_id'=> $result, "status"=>0])->get()->toArray();

        }

       return array_filter($userID);


    }
    public static function validateSubdomai(){
        if (request()->getHttpHost() == 'surveyappnew.n1.iworklab.com') {
            return request()->getHttpHost();
        }
        return request()->getHttpHost();
    }


}
