<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Event;
use App\Models\Attende;
use App\Models\EventAttende;

class EventAttendeHelper {

	/**
	**@param $eventID 
	**@return attendes in a particular event
	**/
	public static function getEventAttende($eventID){
		
	$eventAttende = Attende::select('attendes.*','event_attendes.*' ,'event.eventName')
				->join('event_attendes',function($join) use($eventID){
					$join->on('event_attendes.attendeID','=','attendes.id');
				})		
				->join('event', 'event.id', '=', 'event_attendes.eventID')
            	->where('event_attendes.eventID',$eventID)->get();
            
            	return $eventAttende;
	}


}
