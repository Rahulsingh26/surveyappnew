<?php
function randomPassword($len) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $len; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
/* * * * * * * * * * * * * Guzzle Client functions * * * * * * * * * * * */
if (!function_exists('getGuzzleRequest')) {
    function getGuzzleRequest($client, $url)
    {
        //dd($url);
        $request = $client->request('GET', $url);
        $response = json_decode($request->getBody());
        return $response;
    }
}

?>


