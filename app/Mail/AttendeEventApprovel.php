<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AttendeEventApprovel extends Mailable
{
    use Queueable, SerializesModels;
	public $attendeID,$link,$eventId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($attendeID,$link,$eventId,$eventName)
    {

        $this->attendeID = $attendeID;
        $this->link = $link;
        $this->eventId = $eventId;
        $this->eventName = $eventName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $attendeID = $this->attendeID;
        $link = $this->link;
        $eventId = $this->eventId;
        $eventName = $this->eventName;
        return $this->view('mails/attendeEventApprovel',compact("attendeID","link","eventId","eventName"));
    }
}
