<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaveData extends Model
{
	protected $table = 'exclude_save_data';
    protected $casts = [
        'surveyUser_id' => 'array',
    ];

      public function AssignWalkbook()
    {
    	 return $this->hasMany(AssignPolygonWalkbook::class,'exclude_save_id','id')->groupBy('canvessar_id','exclude_save_id');
    }

     
}
