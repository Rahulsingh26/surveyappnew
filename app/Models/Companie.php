<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companie extends Model
{
    protected $table = 'companies';

    public function user(){
        return $this->belongsTo('App\Models\User','id','companie_id');
    }


}
