<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['answer'];
    protected $table = 'answer';

    protected $casts = [
        'additional_observation' => 'array',
        'answer' => 'array',
    ];

    public function survey() {
      return $this->belongsTo(Survey::class);
    }

    public function question() {
      return $this->belongsTo(Question::class);
    }

    public function walkbook() {
        return $this->belongsTo(Walkbook::class,'walkbooks_id','id');
    }
    public function users() {
        return $this->belongsTo(User::class,'canvasser_id','id');
    }
}
