<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HouseHold extends Model
{
    //
    public $houseHoldIntials = 'hh';
    public function surveyUser()
    {
        return $this->belongsTo('App\Models\SurveyUser','house_hold_id');
    }

    public function surverHouse() {
        return $this->belongsTo(SurveyUser::class,'id','house_hold_id');
    }
}
