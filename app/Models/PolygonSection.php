<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PolygonSection extends Model
{
    protected $table = 'survey_polygon_section';
    protected $casts = [
        'surveyUser_id' => 'array',
    ];

     public function AssignPolygon()
    {
    	return $this->hasMany(AssignPolygon::class,'save_id','id')->groupBy('canvessar_id','save_id');
    }

}
