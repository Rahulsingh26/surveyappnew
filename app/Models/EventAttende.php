<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventAttende extends Model
{
	protected $table = "event_attendes";

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'attendeEventSupportStatus' => 'array',
	];

	public function event(){
        return $this->hasMany(Event::class,'eventID','id');
	}

	public function attende(){
        return $this->hasMany(Attende::class,'attendeID','id');
	}
}