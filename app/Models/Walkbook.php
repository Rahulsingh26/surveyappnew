<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Walkbook extends Model
{
    //
    protected $casts = [
        'user_id' => 'array',
    ];
    const RECORDSPERPAGE = 20;
    protected $fillable = ['title', 'status','user_id'];

    public function surveyUser()
    {
        return $this->belongsToMany('App\Models\SurveyUser');
    }

    public function assignWalkbook() {
        return $this->belongsTo(AssignWalkbook::class);
    }

    public function walkbookRelatedToSurvey() {
        return $this->belongsTo(AssignWalkbook::class,'id','walkbook_id');
    }

    public function surveyUserWalkbook(){
        return $this->belongsTo(SurveyUserWalkbook::class);
    }

    public function assignWalkbookDetails() {
        return $this->hasOne(AssignWalkbook::class, 'walkbook_id');
    }

}
