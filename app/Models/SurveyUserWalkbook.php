<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyUserWalkbook extends Model
{
    protected $table = 'survey_user_walkbook';

    public function surveyUser() {
        return $this->hasOne(SurveyUser::class,'id','survey_user_id');
    }
    public function walkbook(){
        return $this->hasOne(Walkbook::class,'id','walkbook_id');
    }

    /**
     * get all users by walkbook id
     * @param $walkbookId
     * @return mixed
     */
    public static function userListByWalkbook($walkbookId){
        $surveyUserIds = SurveyUserWalkbook::where(['walkbook_id' => $walkbookId])->get()->pluck('survey_user_id');
        $surveyUsers = SurveyUser::select('id', 'name as Name', 'voter_id as Voter ID', 'street as Street', 'city as City', 'state as State', 'gender as Gender', 'zipcode as Zipcode', 'age as Age', 'precinct as Precinct', 'phone as Phone', 'vote_history_1 as Vote History 1', 'vote_history_2 as Vote History 2', 'vote_history_3 as Vote History 3', 'vote_history_4 as Vote History 4', 'contact_history_1 as Contact History 1', 'contact_history_2 as Contact History 2', 'contact_history_3 as Contact History 3', 'early_voting_location as Early Voting Location', 'election_day_location as Election Day Location', 'financial_contributor as Financial Contributer', 'voted as Voted', 'extra_1 as Append 1', 'extra_2 as Append 2', 'note as Note')
        ->whereIn('id', $surveyUserIds)->get()->toArray();

        for ($i=0; $i < count($surveyUsers); $i++){
            $surveyUsers[$i]["Tag"] = implode(",", Tag::where(['survey_user_id' => $surveyUsers[$i]['id']])->get()->pluck('title')->toArray());
        }
        return $surveyUsers;
    }
}
