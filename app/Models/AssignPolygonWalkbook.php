<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignPolygonWalkbook extends Model
{
    protected $table = "assign_polygon_walkbook";
   /* protected $casts = [
    	'canvessar_id'=>'array',
    ];*/
  

     public function CanvessarUser()
    {
    	 return $this->hasMany(User::class,'id','canvessar_id');
    }

    public function SaveData()
    {
        return $this->hasMany(SaveData::class,'id','exclude_save_id');
    }
   
}
