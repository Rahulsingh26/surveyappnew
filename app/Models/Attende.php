<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attende extends Model
{
	protected $table = "attendes";

	public function eventAttende(){
        return $this->belongsTo(EventAttende::class);
	}
}