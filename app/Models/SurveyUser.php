<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyUser extends Model
{
    //
    protected $guarded = [];

    public function tags()
    {
        return $this->hasMany('App\Models\Tag','survey_user_id');
    }

    public function houseHold() {
        return $this->belongsTo(HouseHold::class);
    }

    public function houseNumber(){
        return $this->hasMany(HouseHold::class,'id','house_hold_id');
    }

    public function walkBook()
    {
        return $this->belongsToMany('App\Models\Walkbook');
    }

    public function pendingWalkBook()
    {
        return $this->walkBook()->wherePivot('status', 0);
    }


    public function surveyUserWalkbook(){
        return $this->hasOne(SurveyUserWalkbook::class);
    }

}
