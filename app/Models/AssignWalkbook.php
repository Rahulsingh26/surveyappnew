<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignWalkbook extends Model
{
    protected $table = 'assign_walkbook';
    protected $guarded = [];
    public $timestamps = false;

    public function survey() {
        return $this->hasOne(Survey::class,'id','survey_id');
    }
    public function walkbook(){
        return $this->hasOne(Walkbook::class,'id','walkbook_id');
    }


}
