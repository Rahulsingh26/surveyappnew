<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignPolygon extends Model
{
    protected $table = 'assign_polygon_section';

    public function CanvessarUser()
    {
    	 return $this->hasMany(User::class,'id','canvessar_id');
    }

    public function PolygonSection()
    {
        return $this->hasMany(PolygonSection::class,'id','save_id');
    }
}
