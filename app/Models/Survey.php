<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Survey extends Model
{
  protected $fillable = ['title', 'description', 'user_id','video_id','walkbooks_id','userCreatedSurveyID'];
  protected $dates = ['deleted_at'];
  protected $table = 'survey';

  public function questions() {
    return $this->hasMany(Question::class);
  }

  public function available_mcq_questions() {
    return $this->questions()->where('question_type','=', 3);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function answers() {
    return $this->hasMany(Answer::class);
  }

    public function video() {
        return $this->belongsTo(Video::class)->select('*',DB::raw("CONCAT('" . asset('uploads/videos/') . "/'" . ",file_name) AS videoURL"));
    }

    public function assignWalkbook(){
        return $this->hasMany(AssignWalkbook::class);
    }
}
