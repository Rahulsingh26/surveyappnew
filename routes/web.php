<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('attend-event-acceptance/{token}/{userID}/{statusID}/{eventID}','EventAttendeController@changeAttendeStatus');
Route::namespace('Auth')->group(function () {
    Route::get('/', 'LoginController@showLoginForm')->name('login');
});

Auth::routes();

Route::domain(\App\Helper\AppHelper::validateSubdomai())->middleware(['auth','check_user_role'])->group(function () {


    Route::prefix('dashboard')->group(function () {
        Route::get('/', 'DashboardController@index')->name('dasboard');
        Route::post('filter-dashboard-graph-data', 'DashboardController@getGraphData')->name('dashboard-graph-data');
        Route::post('filter-selected-precinct', 'DashboardController@selectePrecinct')->name('selected.precinct');
        Route::post('filter-selected-canvasser', 'DashboardController@filterCanvasser')->name('selected.canvasser');
        Route::get('filter-canvasser-identification', 'DashboardController@filterCanvasserIdentification')->name('canvasser.identification');
        Route::get('filter-survey-result', 'DashboardController@getSurveyResultData')->name('survey.result');
    });
    Route::prefix('account-settings')->group(function () {
        Route::get('change-password', 'UserController@passwordView')->name('change-password');
        Route::post('change-password', 'UserController@passwordUpdate')->name('change-password');
        Route::get('change-email', 'UserController@emailView')->name('change-email');
        Route::post('change-email', 'UserController@emailUpdate')->name('change-email');
    });
    Route::prefix('manage-users')->group(function () {
        Route::group(['middleware' => 'role_or_permission:super-admin|super-admin-company|write'], function () {
            Route::get('/add-user', 'UserController@addUserView')->name('add-user');
            Route::post('/add-user', 'UserController@addUser')->name('add-user');
            Route::get('/edit-user/{userId}', 'UserController@editUserView')->name('edit-user');
            Route::post('/edit-user/{userId}', 'UserController@editUser')->name('edit-user');
        });
        Route::get('/listUser/', 'UserController@listUser')->name('list-user');
    });
    Route::prefix('manage-reports')->group(function () {
        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('reports-manage', 'ReportController@reportManage')->name('report-user');
            Route::post('survey-question', 'ReportController@surveyQuestion')->name('survey.question');
            Route::post('survey-question-report', 'ReportController@questionReport')->name('question.report');
            Route::get('download-walkbook/{walkbookId}', 'ReportController@downloadWalkBook')->name('download-walkbook');
        });
    });
    Route::prefix('manage-households')->group(function () {
        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('/add', 'SurveyUserController@create')->name('surveyUser.add');
            Route::post('/add', 'SurveyUserController@store')->name('surveyUser.add');
            Route::get('/edit/{SurveyUser}', 'SurveyUserController@edit')->name('surveyUser.edit');
            Route::post('/edit/{SurveyUser}', 'SurveyUserController@update')->name('surveyUser.edit');
        });
        Route::group(['middleware' => 'role_or_permission:super-admin|upload/download'], function () {
            Route::post('/delete-tag','SurveyUserController@deleteTag');
            Route::get('/import', 'SurveyUserController@getImport')->name('surveyUser.import');
            Route::get('/mapping', 'SurveyUserController@mapping')->name('surveyUser.mapping');
            Route::post('/finacial', 'SurveyUserController@FinancialContributor')->name('surveyUser.finacial');
            Route::post('/support', 'SurveyUserController@Support')->name('surveyUser.support');
            Route::post('/delete', 'SurveyUserController@FinancialDelete')->name('surveyUser.delete');
            Route::post('/deletesupport', 'SurveyUserController@supportDelete')->name('surveyUser.deletesupport');
            Route::post('/deletenotes', 'SurveyUserController@notesDelete')->name('surveyUser.deletenotes');
            Route::post('/getfinacial', 'SurveyUserController@FinancialContributorGet')->name('surveyUser.getfinacial');
            //Route::get('/mapping/{path}', 'SurveyUserController@mapping')->name('surveyUser.mapping');
            Route::post('/import', 'SurveyUserController@parseImport')->name('surveyUser.import');
            Route::post('/import_process', 'SurveyUserController@processImport')->name('surveyUser.import_process');
        });

        Route::get('/view/{SurveyUser}', 'SurveyUserController@show')->name('surveyUser.show');
        Route::get('/list/{csvData}', 'SurveyUserController@index')->name('surveyUser.list');
        Route::get('/list', 'SurveyUserController@addedUserManually')->name('surveyUser.list');
        Route::get('/createcsv-list/{id?}', 'SurveyUserController@createcsvList')->name('createcsv.list');

        Route::get('/csv-list', 'SurveyUserController@csvList')->name('csv.list');
        Route::get('/csv-listnew', 'SurveyUserController@csvListnew')->name('csv.listnew');
        Route::post('/exclude-get', 'SurveyUserController@excludeDataGet')->name('exclude-data-get');
        Route::post('/exclude', 'SurveyUserController@excludeData')->name('exclude-data');

        //ajax on dynamic set in pin on map
        Route::post('/exclude-get-map', 'SurveyUserController@excludeDataMap')->name('exclude-data-map');
        Route::post('/include-get-map', 'SurveyUserController@includeDataMap')->name('include-data-map');
        Route::post('/exclude-get-next','SurveyUserController@excludeNext')->name('exclude-next');
        Route::post('/exclude-next-step','SurveyUserController@nextStep')->name('nextStep');
        Route::post('/exclude-save-step','SurveyUserController@saveStep')->name('saveStep');
        Route::post('/exclude-final-step','SurveyUserController@finalStep')->name('finalStep');
        Route::get('/exclude-assign-canvessa','SurveyUserController@canvessaList')->name('assign-canvessa');

        Route::post('/finance/', 'SurveyUserController@finance')->name('surveyUser.finance');
        Route::get('/export', 'SurveyUserController@export')->name('surveyUser.export');
    });

    Route::prefix('walkbook')->group(function () {
        //Route::get('/import', 'SurveyUserController@getImport')->name('surveyUser.import');
        Route::get('list-ajax', 'WalkbookController@getWalkbooksFromAjax')->name('ajax.walkbook');
        Route::get('list', 'WalkbookController@index')->name('list.walkbook');
        Route::post('ajax-add', 'WalkbookController@ajaxAddSurveyUserToWalkbook')->name('addSurveyUser.walkbook');
        Route::get('create', 'WalkbookController@create')->name('create.walkbook');
        Route::post('create', 'WalkbookController@store')->name('store.walkbook');
        Route::get('{walkbook}/edit', 'WalkbookController@edit')->name('edit.walkbook');
        Route::post('{walkbook}/update', 'WalkbookController@update')->name('update.walkbook');
        Route::get('/delete/{walkbook}', 'WalkbookController@destroy')->name('delete.walkbook');
        Route::get('tracking-walkbook','WalkbookController@tracking')->name('track.walkbook');
        Route::post('miles-change','WalkbookController@perSquareMile')->name('miles.walkbook');
        Route::post('house-hold-view','WalkbookController@houseHoldView')->name('houseHoldView.walkbook');
        Route::get('house-hold-details','WalkbookController@houseHoldDetails')->name('houseHoldDetails.walkbook');
        Route::get('fraud-detection','WalkbookController@fraudDetection')->name('fraud.walkbook');
        Route::get('network-status','WalkbookController@networkStatus')->name('dataMode.walkbook');
        Route::post('change-servey', 'WalkbookController@changeSurvey')->name('change.survey');
    });
    Route::prefix('manage-videos')->group(function () {
        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('/add', 'VideoController@create')->name('create.video');
            Route::post('/add', 'VideoController@store')->name('store.video');
            Route::get('/edit/{video}', 'VideoController@edit')->name('edit.video');
            Route::post('/update/{video}', 'VideoController@update')->name('update.video');
            Route::get('/delete/{video}', 'VideoController@destroy')->name('delete.video');
        });
        Route::get('/list', 'VideoController@index')->name('list.video');

    });

    Route::prefix('event')->group(function () {
        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('/add', 'EventController@create')->name('create.event');
            Route::post('/event-add', 'EventController@addEvent')->name('add.event');
            Route::get('/status-update/{id}/{statusid}','EventController@statusUpadte')->name('statusUpdate.event');
            Route::get('/event-delete/{id}','EventController@deleteEvent')->name('deleteEvent.event');
            Route::get('/event-edit/{id}','EventController@editEvent')->name('editEvent.event');
             Route::post('/event-update', 'EventController@updateEvent')->name('update.event');
             Route::post('/delete-event-attende', 'EventController@deleteAttendeEventConnection')->name('deleteEventAtende.event');
             Route::get('attend-event-acceptance/{token}/{userID}/{statusID}/{eventID}','EventController@changeAttendeEventStatus');
        });
        Route::get('/list-attende/{eventID}','EventController@eventAttendeList')->name('list.eventAttende');
        Route::get('/list', 'EventController@index')->name('list.event');

    });

    Route::prefix('attende')->group(function () {
        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('/add', 'AttendeController@create')->name('create.attende');
            Route::post('/attende-add', 'AttendeController@addAttende')->name('add.attende');
            Route::get('/status-update/{id}/{statusid}','AttendeController@statusUpadte')->name('statusUpdate.attende');
            Route::get('/attende-delete/{id}','AttendeController@deleteAttende')->name('deleteAttende.attende');
            Route::get('/attende-edit/{id}','AttendeController@editAttende')->name('editAttende.attende');
             Route::post('/attende-update', 'AttendeController@updateAttende')->name('update.attende');
             Route::post('/attende-event-delete', 'AttendeController@updateAttende')->name('update.attende');
             Route::get('/attende-csv-form','AttendeController@addAttendeByCSV')->name('csv-form.attende');
             Route::post('/attende-csv-add','AttendeController@addAttendeCsvRecord')->name('csv.attende');
        });
        Route::get('/list', 'AttendeController@index')->name('list.attende');

    });


    Route::prefix('survey')->group(function () {
        Route::get('/list', 'SurveyController@home')->name('list.survey');
        Route::get('/view/{survey}', 'SurveyController@view_survey')->name('view.survey');
        Route::get('/answers/{survey}', 'SurveyController@view_survey_answers')->name('view.survey.answers');
        Route::post('/view/{survey}/completed', 'AnswerController@store')->name('complete.survey');
        Route::post('/create', 'SurveyController@create')->name('create.survey');
        Route::get('/survey-history', 'SurveyController@surveyHistory')->name('histroy.survey');
        Route::get('/survey-history-details/{survey}', 'SurveyController@surveyHistoryDetails')->name('histroyDetails.survey');

        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('/new', 'SurveyController@new_survey')->name('new.survey');
            Route::get('/{survey}', 'SurveyController@detail_survey')->name('detail.survey');
            Route::get('/{survey}/delete', 'SurveyController@delete_survey')->name('delete.survey');
            Route::get('/{survey}/edit', 'SurveyController@edit')->name('edit.survey');
            Route::post('/{survey}/update', 'SurveyController@update')->name('update.survey');

            Route::post('/deactivated-survey', 'SurveyController@deactivated')->name('deactivated-survey');

            Route::get('/walkbook-list/{survey}', 'SurveyController@assignWalkbook')->name('walkbook.survey');
            Route::get('walkbook-add/{survey}', 'SurveyController@addWalkbook')->name('add.walkbook');
            Route::get('walkbook-new/{survey}', 'SurveyController@addNewWalkbook')->name('add.NewWalkbook');
            Route::post('add-survey-list', 'SurveyController@addserveyList')->name('add.serveyList');
            Route::get('delete-walkbook/{id}', 'SurveyController@deleteWorkbook')->name('walkbook.delete');


            // Questions related
            Route::post('/{survey}/questions', 'QuestionController@store')->name('store.question');
            Route::get('/question/{question}/edit', 'QuestionController@edit')->name('edit.question');
            Route::patch('/question/{question}/update', 'QuestionController@update')->name('update.question');
            Route::get('/question/{question}/delete', 'QuestionController@delete_question')->name('delete.question');

        });



    });

    Route::prefix('companies')->group(function (){
        Route::group(['middleware' => 'role_or_permission:super-admin|write'], function () {
            Route::get('companie', 'CompanieController@index')->name('companie');
            Route::post('add-companie', 'CompanieController@addCompanie')->name('add-companie');
            Route::get('companie-list', 'CompanieController@companieList')->name('list-companie');
            Route::get('companie-view/{id}', 'CompanieController@companieView')->name('view-companie');
            Route::get('/status-update/{id}/{statusid}','CompanieController@statusUpadte')->name('statusUpdate-companies');

        });
    });
    Route::prefix('polygons')->group(function (){
            Route::get('polygon-list', 'polygonController@polygonList')->name('polygon-list');
            Route::get('polygon', 'polygonController@index')->name('polygon');
            Route::post('polygon-create', 'polygonController@shapeCreate')->name('polygon-create');
            Route::get('polygon-view/{id}', 'polygonController@polygonView')->name('view-polygon');
            Route::get('delete-polygon/{id}', 'polygonController@deletepolygon')->name('polygon.delete');
            Route::post('/polygon-save-step','polygonController@polygonSaveStep')->name('polygonSaveStep');
            Route::post('/polygon-next-step','polygonController@polyNextStep')->name('polyNextStep');
            Route::post('/polygon-final-step','polygonController@polygonfinalStep')->name('polygonfinalStep');
    });



});

 Route::get('history-download', 'SurveyController@surveyHistoryDetailsDownload')->name('surveyHistoryDetails.Download');

Route::get('password/forget', 'API\Auth\ForgotPasswordController@ChangePasswordForm');
Route::post('password/store', 'API\Auth\ForgotPasswordController@getChangedPassword')->name('password.forget');

Route::get('testing', 'SurveyUserController@testingmap');
Route::view('/testing2', 'testmap2');

