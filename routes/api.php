<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'API'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('login', 'UserController@login');
        Route::post('password-email', 'ForgotPasswordController@getResetToken');

       // Route::post('register', 'UserController@register');
        Route::group(['middleware' => ['auth:api']], function(){
                Route::post('/change-password', 'UserController@passwordUpdate');
                Route::post('logout', 'UserController@logout');
                Route::post('user-img', 'UserController@userProfileImg');
                Route::post('preferences', 'UserController@preferences');
                Route::post('get-preferences', 'UserController@getPreference');
                Route::post('profile-details', 'UserController@profileDetails');


                Route::group(['namespace' => 'WalkBook'], function(){
                    Route::post('walkbook-list','WalkBookDashboadController@walkList');
                });

                Route::group(['namespace' => 'Survey'], function(){
                    Route::post('survey-list','SurveyDashboadController@surveyList');
                    Route::post('survey-start','SurveyDashboadController@serverStart');
                    Route::post('survey-details','SurveyDashboadController@serverDetails');
                    Route::post('survey-details-submit','SurveyDashboadController@serverDetailsSubmit');
                    Route::post('survey-cancel','SurveyDashboadController@serverCancel');
                    Route::post('filter-address','SurveyDashboadController@filterAddress');
                    Route::post('filter-map','SurveyDashboadController@filterMap');

                    /*History for survey*/
                    Route::post('survey-history','SurveyDashboadController@surveyHistory');
                    Route::post('history-details','SurveyDashboadController@historyDetails');
                });

                Route::group(['namespace' => 'Event'],function(){
                    Route::post('event-list','EventApiController@listEvent');
                    Route::post('event-attende-list','EventApiController@listOfEventAttendee');
                    Route::post('event-attende-edit','AttendeApiController@editAttendee');
                    Route::post('event-attende-add','AttendeApiController@addAttendeeEvent');
                    Route::post('event-attende-checkIn','EventApiController@eventAttendeCheckinStatus');
                });
         });
    });


});

